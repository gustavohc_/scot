#!/bin/sh

NODES="100 150 200"
TRIALS="32"

echo ELECTRON Experiments

pCheck=`which sqlite3`
if [ -z "$pCheck" ]
then
  echo "ERROR: This script requires sqlite3 (wifi-example-sim does not)."
  exit 255
fi

pCheck=`which gnuplot`
if [ -z "$pCheck" ]
then
  echo "ERROR: This script requires gnuplot (wifi-example-sim does not)."
  exit 255
fi

pCheck=`which sed`
if [ -z "$pCheck" ]
then
  echo "ERROR: This script requires sed (wifi-example-sim does not)."
  exit 255
fi

export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:bin/

if [ -e db/data.db ]
then
  echo "Kill data.db? (y/n)"
  read ANS
  if [ "$ANS" = "yes" -o "$ANS" = "y" ]
  then
    echo Deleting database
    rm db/data.db
    if [ -e db/electron-detection.data ]
	then
		rm db/electron-detection.data
	fi
  fi
fi

for trial in `seq 1 $TRIALS`
do
  for nodes in $NODES
  do
    echo Trial-$trial Nodes-$nodes
    pAttackers=$(echo "scale=0;($nodes*0.1)" | bc)
    echo Atackers: $pAttackers
    ./waf --run "scot-scenario --nodes=$nodes --comm=5 --attack=2 --attackers=$pAttackers --run=run-$trial"
  done
done

#
#Another SQL command which just collects raw numbers of frames receved.
#
#CMD="select Experiments.input,avg(Singletons.value) \
#    from Singletons,Experiments \
#    where Singletons.run = Experiments.run AND \
#          Singletons.name='wifi-rx-frames' \
#    group by Experiments.input \
#    order by abs(Experiments.input) ASC;"


#CMD="select exp.input,avg(100-((rx.value*100)/tx.value)) \
#    from Singletons rx, Singletons tx, Experiments exp \
#    where rx.run = tx.run AND \
#          rx.run = exp.run AND \
#          rx.variable='receiver-rx-packets' AND \
#          tx.variable='sender-tx-packets' \
#    group by exp.input \
#    order by abs(exp.input) ASC;"

CMD="select substr(exp.run,7,1), tx.value/cx.value AS mean,  (1.960 *dx.value) AS standard_deviation \
	from Singletons cx, Singletons tx, Singletons sx, Singletons dx, Experiments exp \
	where cx.variable='-count' AND \
	tx.variable='-total' AND \
	sx.variable='-sqrsum' AND \
	dx.variable='-stddev' AND \
	exp.input = '100' AND \
	substr(cx.run,0,5) = substr(exp.run,0,5) AND \
	substr(tx.run,0,5) = substr(exp.run,0,5) AND \
	substr(sx.run,0,5) = substr(exp.run,0,5) AND \
	substr(dx.run,0,5) = substr(exp.run,0,5) AND \
	substr(cx.run,0,5) = substr(tx.run,0,5) AND \
	substr(tx.run,0,5) = substr(sx.run,0,5) AND \
	substr(sx.run,0,5) = substr(dx.run,0,5) AND \
	substr(cx.run,7,1) = substr(exp.run,7,1) AND \
	substr(tx.run,7,1) = substr(exp.run,7,1)AND \
	substr(sx.run,7,1) = substr(exp.run,7,1) AND \
	substr(dx.run,7,1) = substr(exp.run,7,1) AND \
	substr(cx.run,7,1) = substr(tx.run,7,1) AND \
	substr(tx.run,7,1) = substr(sx.run,7,1) AND \
	substr(sx.run,7,1) = substr(dx.run,7,1) \
	group by  exp.input, substr(exp.run,7,1) \
	order by abs(exp.input) ASC;"

sqlite3 -noheader data.db "$CMD" > electron-detection-100.data

CMD="select substr(exp.run,7,1), tx.value/cx.value AS mean,  (1.960 *dx.value) AS standard_deviation \
	from Singletons cx, Singletons tx, Singletons sx, Singletons dx, Experiments exp \
	where cx.variable='-count' AND \
	tx.variable='-total' AND \
	sx.variable='-sqrsum' AND \
	dx.variable='-stddev' AND \
	exp.input = '150' AND \
	substr(cx.run,0,5) = substr(exp.run,0,5) AND \
	substr(tx.run,0,5) = substr(exp.run,0,5) AND \
	substr(sx.run,0,5) = substr(exp.run,0,5) AND \
	substr(dx.run,0,5) = substr(exp.run,0,5) AND \
	substr(cx.run,0,5) = substr(tx.run,0,5) AND \
	substr(tx.run,0,5) = substr(sx.run,0,5) AND \
	substr(sx.run,0,5) = substr(dx.run,0,5) AND \
	substr(cx.run,7,1) = substr(exp.run,7,1) AND \
	substr(tx.run,7,1) = substr(exp.run,7,1)AND \
	substr(sx.run,7,1) = substr(exp.run,7,1) AND \
	substr(dx.run,7,1) = substr(exp.run,7,1) AND \
	substr(cx.run,7,1) = substr(tx.run,7,1) AND \
	substr(tx.run,7,1) = substr(sx.run,7,1) AND \
	substr(sx.run,7,1) = substr(dx.run,7,1) \
	group by  exp.input, substr(exp.run,7,1) \
	order by abs(exp.input) ASC;"

sqlite3 -noheader data.db "$CMD" > electron-detection-150.data

CMD="select substr(exp.run,7,1), tx.value/cx.value AS mean,  (1.960 *dx.value) AS standard_deviation \
	from Singletons cx, Singletons tx, Singletons sx, Singletons dx, Experiments exp \
	where cx.variable='-count' AND \
	tx.variable='-total' AND \
	sx.variable='-sqrsum' AND \
	dx.variable='-stddev' AND \
	exp.input = '200' AND \
	substr(cx.run,0,5) = substr(exp.run,0,5) AND \
	substr(tx.run,0,5) = substr(exp.run,0,5) AND \
	substr(sx.run,0,5) = substr(exp.run,0,5) AND \
	substr(dx.run,0,5) = substr(exp.run,0,5) AND \
	substr(cx.run,0,5) = substr(tx.run,0,5) AND \
	substr(tx.run,0,5) = substr(sx.run,0,5) AND \
	substr(sx.run,0,5) = substr(dx.run,0,5) AND \
	substr(cx.run,7,1) = substr(exp.run,7,1) AND \
	substr(tx.run,7,1) = substr(exp.run,7,1)AND \
	substr(sx.run,7,1) = substr(exp.run,7,1) AND \
	substr(dx.run,7,1) = substr(exp.run,7,1) AND \
	substr(cx.run,7,1) = substr(tx.run,7,1) AND \
	substr(tx.run,7,1) = substr(sx.run,7,1) AND \
	substr(sx.run,7,1) = substr(dx.run,7,1) \
	group by  exp.input, substr(exp.run,7,1) \
	order by abs(exp.input) ASC;"

sqlite3 -noheader data.db "$CMD" > electron-detection-200.data

mv data.db db/
mv electron-detection-100.data db/
mv electron-detection-150.data db/
mv electron-detection-200.data db/

sed -i.bak -e "s/|/   /" -e "s/|/   /" -e "s/|/   /" db/electron-detection-100.data
rm db/electron-detection-100.data.bak

sed -i.bak -e "s/|/   /" -e "s/|/   /" -e "s/|/   /" db/electron-detection-150.data
rm db/electron-detection-150.data.bak

sed -i.bak -e "s/|/   /" -e "s/|/   /" -e "s/|/   /" db/electron-detection-200.data
rm db/electron-detection-200.data.bak

#remove the \n in the file for the histogram
sed -i ':a;N;$!ba;s/\n/ /g' db/electron-detection-100.data
sed -i ':a;N;$!ba;s/\n/ /g' db/electron-detection-150.data
sed -i ':a;N;$!ba;s/\n/ /g' db/electron-detection-200.data
#gnuplot wifi-example.gnuplot

echo "Done; data in electron-detection.data"