/*
 * statistics.cc
 *
 *  Created on: May 18, 2018
 *      Author: gustavo
 */
#include <iostream>


#ifndef STATISTICS_H_
#define STATISTICS_H_

class Statistics : public Object
{

public:

	/**
	 * Number of Multiples Id attacks with Id stolen
	 */
	uint32_t NUM_ATTACK_MULTI_STOLEN_ID = 0;
	/**
	 * Number of Multiples Id attacks with Id fabricated
	 */
	uint32_t NUM_ATTACK_MULTI_FABRICATED_ID = 0;
	/**
	 * Number of Churn attacks with Id stolen
	 */
	uint32_t NUM_ATTACK_CHURN_STOLEN_ID = 0;
	/**
	 * Number of Churn attacks with Id fabricated
	 */
	uint32_t NUM_ATTACK_CHURN_FABRICATED_ID = 0;
	/**
	 * Real number of Multiples Id attacks with Id stolen
	 */
	uint32_t NUM_ATTACK_MULTI_STOLEN_ID_REAL = 0;
	/**
	 * Real number of Multiples Id attacks with Id fabricated
	 */
	uint32_t NUM_ATTACK_MULTI_FABRICATED_ID_REAL = 0;
	/**
	 * Real number of Churn attacks with Id stolen
	 */
	uint32_t NUM_ATTACK_CHURN_STOLEN_ID_REAL = 0;
	/**
	 * Real number Churn attacks with Id fabricated
	 */
	uint32_t NUM_ATTACK_CHURN_FABRICATED_ID_REAL = 0;
	/**
	 * Number of nodes access denied
	 */
	uint32_t NUM_NODE_DENIED = 0;
	/**
	 * Number of legal requests
	 */
	uint32_t NUM_LEGAL_REQ = 0;
	/**
	 * Total of requests
	 */
	uint32_t NUM_TOTAL_REQ = 0;
	/**
	 * Total of attack correctly target as attack
	 */
	uint32_t NUM_TRUE_POSITIVE = 0;
	/**
	 * Total of legal request target as attack
	 */
	uint32_t NUM_FALSE_POSITIVE = 0;
	/**
	 * Total of attack target as legal request
	 */
	uint32_t NUM_FALSE_NEGATIVE = 0;


	/**
	 * Number of attacks
	 */
	uint32_t NUM_ATTACK = 0;
	/**
	 * Real number attacks
	 */
	uint32_t NUM_ATTACK_REAL = 0;

	uint32_t community;

	uint32_t m_packetsSybilRxTotal;
	uint32_t m_packetsSybilTxTotal;

	std::ofstream fout;

	std::vector<double> cdf_owner;
	std::vector<double> cdf_local;
	std::vector<double> cdf_paren;
	std::vector<double> cdf_cowork;
	std::vector<double> cdf_social;

	std::vector<double> energyHonest;
	std::vector<double> energyAttacker;

	std::vector<double> cdf_owner_candidates;
	std::vector<double> cdf_local_candidates;
	std::vector<double> cdf_paren_candidates;
	std::vector<double> cdf_cowork_candidates;
	std::vector<double> cdf_social_candidates;

	std::vector<double> boxplot_owner;
	std::vector<double> boxplot_local;
	std::vector<double> boxplot_paren;
	std::vector<double> boxplot_cowork;
	std::vector<double> boxplot_social;

//	std::vector<double> boxplot_owner_candidates;
//	std::vector<double> boxplot_local_candidates;
//	std::vector<double> boxplot_paren_candidates;
//	std::vector<double> boxplot_cowork_candidates;
//	std::vector<double> boxplot_social_candidates;

	std::map<double, double> detectedRateMultIdStolen;
	std::map<double, double> detectedRateMultiIdFabricated;
	std::map<double, double> detectedRateChurnStolen;
	std::map<double, double> detectedRateChurnFabricated;

	std::map<double, double> detectedRate;

	std::map<double, double> falseNegatives;

	//------------------------------------------------------------
	//-- Setup stats and data collection
	//--------------------------------------------

	// Create a DataCollector object to hold information about this run.
	DataCollector data;
	Ptr<MinMaxAvgTotalCalculator<double>> detectedRateAvg;
//	Ptr<CounterCalculator<> > time;


	// ************************** PUBLIC METHODS *****************************

	Statistics();

	Statistics(uint32_t community, std::string experiment, std::string strategy, std::string input, std::string runID);

	/**
	 * Reset all statistical fields (integer counters) back to zero.
	 */
	void reset ();

	void AppRxCallback(std::string path, Ptr<const Packet> packet, const Address &from);
	void AppTxCallback(std::string path, Ptr<const Packet> packet);
	void TrustCallback(std::string path, double vTrust);

	//void writeTrustOutputFile(int32_t nFrom, int32_t nTo, double value);
	void 	writeTrustOutputFile(double value);
	void 	writeSocialTrustOutputFile(uint32_t from, uint32_t to, double value);
	void 	writeRelationTrustOutputFile(int siot, double gamma, double value);
	void 	writeRelationTrustOutputFile3d(int siot, double gamma, double value, double similarity);
	void 	writeRelationTrustOutputFile3dAll(int siot, double gamma, double value, double similarity);

	void 	writeCDFRelationTrustOutputFile(int siot, double value, double frequency);
	void 	processCDF(int siot,std::vector<double>* cdf, bool nodes);
	void 	generateCDF();
	double 	normalCDF(double x);

	void 	generateBoxplot();
	double 	calcmedian(std::vector<double>* data);
	double 	quartile(std::vector<double>* data, uint32_t p);
	void 	plotTheBox(int siot, std::vector<double>* data, bool nodes);


	void 		increseNumMultiStolenId();
	uint32_t 	getNumMultiStolenId();
	void 		increseNumMultiFabricatedId();
	uint32_t 	getNumMultiFabricatedId();
	void 		increseNumChurnStolenId();
	uint32_t 	getNumChurnStolenId();
	void 		increseNumChurnFabricatedId();
	uint32_t 	getNumChurnFabricatedId();

	void 		increseNumMultiStolenIdReal();
	uint32_t 	getNumMultiStolenIdReal();
	void 		increseNumMultiFabricatedReal();
	uint32_t 	getNumMultiFabricatedReal();
	void 		increseNumChurnStolenIdReal();
	uint32_t 	getNumChurnStolenIdReal();
	void 		increseNumChurnFabricatedIdReal();
	uint32_t 	getNumChurnFabricatedIdReal();

	void 		increseNumAccDenied();
	uint32_t 	getTotalAccDenied();
	void 		increseNumLegalReq();
	uint32_t 	getNumLegalReq();
	void 		increseNumTotalReq();
	uint32_t 	getNumTotalReq();
	void 		increseNumTruePositive();
	uint32_t 	getNumTruePositive();
	void 		increseNumFalsePositive();
	uint32_t 	getNumFalsePositive();
	void 		increseNumFalseNegative();
	uint32_t 	getNumFalseNegative();

	void 		increseNumAttack();
	uint32_t 	getNumAttack();
	void 		increseNumAttackReal();
	uint32_t 	getNumAttackReal();

	float 	calculateStandardDeviation(std::map<double, double>& detectedRate);
	void 	dectecedRateMultiId();
	void 	writedectecedRateMultiId();
	void 	dectecedRateChurn();
	void 	writedectecedRateChurn();
	void 	writeFalsesNegativesRate();
	void 	writeFalsesPositivesRate();

	void dectecedRate();
	void writedectecedRate();

	void energyConsumption(double volt);

	void writeRelationTrustOutputFileCandidates(int siot, double gamma, double value);
	void writeCDFRelationTrustOutputFileCandidates(int siot, double value, double frequency);

	void writeAccuracyDetection();

	void writeThis(uint32_t id, double t);

	double Output();

};

Statistics::Statistics(){}

Statistics::Statistics(uint32_t community, std::string experiment, std::string strategy, std::string input, std::string runID)
{
	this->community = community;
	runID = runID +"-"+std::to_string(community);
	data.DescribeRun (experiment,
						strategy,
						input,
						runID,
						std::to_string(community));
	// Add any information we wish to record about this run.
	data.AddMetadata ("author", "gustavo");
	detectedRateAvg = CreateObject<MinMaxAvgTotalCalculator<double>>();
	//detectedRateAvg->SetKey("test-dc");
//	CreateObject<CounterCalculator<> >();
//		  appRx->SetKey ("receiver-rx-packets");
	data.AddDataCalculator(detectedRateAvg);
}

void Statistics::reset()
{
	 NUM_ATTACK_MULTI_STOLEN_ID = 0;
	 NUM_ATTACK_MULTI_FABRICATED_ID = 0;
	 NUM_ATTACK_CHURN_STOLEN_ID = 0;
	 NUM_ATTACK_CHURN_FABRICATED_ID = 0;
	 NUM_ATTACK_MULTI_STOLEN_ID_REAL = 0;
	 NUM_ATTACK_MULTI_FABRICATED_ID_REAL = 0;
	 NUM_ATTACK_CHURN_STOLEN_ID_REAL = 0;
	 NUM_ATTACK_CHURN_FABRICATED_ID_REAL = 0;
	 NUM_NODE_DENIED = 0;
	 NUM_LEGAL_REQ = 0;
	 NUM_TOTAL_REQ = 0;
}

void Statistics::AppRxCallback(std::string path, Ptr<const Packet> packet, const Address &from)
{
//	int64_t timeReceived = Simulator::Now().GetMicroSeconds();
//
//	// Extrai o conteudo do quadro para obter tempo de envio
//	uint8_t *buffer = new uint8_t[packet->GetSize()];
//	packet->CopyData (buffer, packet->GetSize());
//	std::string data = std::string((char*)buffer);
//	int64_t timeSent = std::atoll(data.c_str());
//
//	int64_t d = timeReceived - timeSent;
//
//	this->m_packetsSybilRxTotal++;
}


void Statistics::AppTxCallback(std::string path, Ptr<const Packet> packet){}

//void Statistics::writeTrustOutputFile(int32_t nFrom, int32_t nTo, double value)
void Statistics::writeTrustOutputFile(double value)
{
	fout.open("/home/gustavo/workspace/ns-allinone-3.27/ns-3.27/statistics/trust/trustValues.txt", std::ios_base::app);

	//fout << nFrom << '\t' << nTo << '\t'<< value;
	fout << Simulator::Now ().GetSeconds () << "\t" << value;
	fout << "\n";

	fout.close();
}

void Statistics::writeSocialTrustOutputFile(uint32_t from, uint32_t to, double value)
{
	fout.open("/home/gustavo/workspace/ns-allinone-3.27/ns-3.27/statistics/socialTrust/socialTrustCommunity-" + std::to_string(community) + ".txt", std::ios_base::app);

	fout << Simulator::Now ().GetSeconds () << "\t" << from << "\t" << to << "\t" << value;
	fout << "\n";

	fout.close();

}

void Statistics::writeRelationTrustOutputFile(int siot, double gamma, double value)
{
	switch(siot)
	{
		case 0 : fout.open("/home/gustavo/workspace/ns-allinone-3.27/ns-3.27/statistics/relations-trust/relationTrustCommunity-" + std::to_string(community)  + "-OWNERSHIP.txt", std::ios_base::app);
			cdf_owner.push_back(value);
			break;
		case 2 : fout.open("/home/gustavo/workspace/ns-allinone-3.27/ns-3.27/statistics/relations-trust/relationTrustCommunity-" + std::to_string(community)  + "-LOCATION.txt", std::ios_base::app);
			cdf_local.push_back(value);
			break;
		case 3 : fout.open("/home/gustavo/workspace/ns-allinone-3.27/ns-3.27/statistics/relations-trust/relationTrustCommunity-" + std::to_string(community)  + "-COWORK.txt", std::ios_base::app);
			cdf_cowork.push_back(value);
			break;
		case 4 : fout.open("/home/gustavo/workspace/ns-allinone-3.27/ns-3.27/statistics/relations-trust/relationTrustCommunity-" + std::to_string(community)  + "-SOCIAL.txt", std::ios_base::app);
			cdf_social.push_back(value);
			break;
		case 1 : fout.open("/home/gustavo/workspace/ns-allinone-3.27/ns-3.27/statistics/relations-trust/relationTrustCommunity-" + std::to_string(community)  + "-PARENTAL.txt", std::ios_base::app);
			cdf_paren.push_back(value);
			break;
	}

	fout << Simulator::Now ().GetSeconds () << "\t" << gamma << "\t" << value;
	fout << "\n";

	fout.close();
}

void Statistics::writeRelationTrustOutputFile3d(int siot, double gamma, double value, double similarity)
{
	switch(siot)
	{
		case 0 : fout.open("/home/gustavo/workspace/ns-allinone-3.27/ns-3.27/statistics/relations-trust3d/relationTrustCommunity3d-" + std::to_string(community)  + "-OWNERSHIP.txt", std::ios_base::app);
			break;
		case 2 : fout.open("/home/gustavo/workspace/ns-allinone-3.27/ns-3.27/statistics/relations-trust3d/relationTrustCommunity3d-" + std::to_string(community)  + "-LOCATION.txt", std::ios_base::app);
			break;
		case 3 : fout.open("/home/gustavo/workspace/ns-allinone-3.27/ns-3.27/statistics/relations-trust3d/relationTrustCommunity3d-" + std::to_string(community)  + "-COWORK.txt", std::ios_base::app);
			break;
		case 4 : fout.open("/home/gustavo/workspace/ns-allinone-3.27/ns-3.27/statistics/relations-trust3d/relationTrustCommunity3d-" + std::to_string(community)  + "-SOCIAL.txt", std::ios_base::app);
			break;
		case 1 : fout.open("/home/gustavo/workspace/ns-allinone-3.27/ns-3.27/statistics/relations-trust3d/relationTrustCommunity3d-" + std::to_string(community)  + "-PARENTAL.txt", std::ios_base::app);
			break;
	}

	fout << Simulator::Now ().GetSeconds () << "\t" << gamma << "\t" << value << "\t" << similarity;
	fout << "\n";

	fout.close();
}

void Statistics::writeRelationTrustOutputFile3dAll(int siot, double gamma, double value, double similarity)
{
	fout.open("/home/gustavo/workspace/ns-allinone-3.27/ns-3.27/statistics/relations-trust3dAll/relationTrustCommunity3dAll.txt", std::ios_base::app);

	fout << Simulator::Now ().GetSeconds () << "\t" << gamma << "\t" << value << "\t" << similarity;
	fout << "\n";

	fout.close();
}

void Statistics::writeRelationTrustOutputFileCandidates(int siot, double gamma, double value)
{
	switch(siot)
	{
		case 0 : fout.open("/home/gustavo/workspace/ns-allinone-3.27/ns-3.27/statistics/relations-trust/relationTrustCandidatesCommunity-" + std::to_string(community)  + "-OWNERSHIP.txt", std::ios_base::app);
			cdf_owner_candidates.push_back(value);
			if(Simulator::Now ().GetSeconds ()>90.) boxplot_owner.push_back(value);
			break;
		case 2 : fout.open("/home/gustavo/workspace/ns-allinone-3.27/ns-3.27/statistics/relations-trust/relationTrustCandidatesCommunity-" + std::to_string(community)  + "-LOCATION.txt", std::ios_base::app);
			cdf_local_candidates.push_back(value);
			if(Simulator::Now ().GetSeconds ()>90.)	boxplot_local.push_back(value);
			break;
		case 3 : fout.open("/home/gustavo/workspace/ns-allinone-3.27/ns-3.27/statistics/relations-trust/relationTrustCandidatesCommunity-" + std::to_string(community)  + "-COWORK.txt", std::ios_base::app);
			cdf_cowork_candidates.push_back(value);
			if(Simulator::Now ().GetSeconds ()>90.) boxplot_cowork.push_back(value);
			break;
		case 4 : fout.open("/home/gustavo/workspace/ns-allinone-3.27/ns-3.27/statistics/relations-trust/relationTrustCandidatesCommunity-" + std::to_string(community)  + "-SOCIAL.txt", std::ios_base::app);
			cdf_social_candidates.push_back(value);
			if(Simulator::Now ().GetSeconds ()>90.) boxplot_social.push_back(value);
			break;
		case 1 : fout.open("/home/gustavo/workspace/ns-allinone-3.27/ns-3.27/statistics/relations-trust/relationTrustCandidatesCommunity-" + std::to_string(community)  + "-PARENTAL.txt", std::ios_base::app);
			cdf_paren_candidates.push_back(value);
			if(Simulator::Now ().GetSeconds ()>90.) boxplot_paren.push_back(value);
			break;
	}

	fout << Simulator::Now ().GetSeconds () << "\t" << gamma << "\t" << value;
	fout << "\n";

	fout.close();
}

double Statistics::normalCDF(double x) // Phi(-∞, x) aka N(x)
{
	return std::erfc(-x/std::sqrt(2))/2;
}

void Statistics::processCDF(int siot, std::vector<double>* cdf, bool nodes)
{
	if(!cdf->empty())
	{
		std::sort(cdf->begin(), cdf->end());

		double frequency = 0.;
		int size = cdf->size();
		for(auto i = cdf->begin(); i != cdf->end(); ++i)
		{
			frequency += ((double) 1/size);
			(nodes) ? writeCDFRelationTrustOutputFile(siot,(*i),frequency) : writeCDFRelationTrustOutputFileCandidates(siot, (*i), frequency);
		}
	}
}

void Statistics::writeCDFRelationTrustOutputFile(int siot, double value, double frequency)
{
	switch(siot)
	{
		case 0 : fout.open("/home/gustavo/workspace/ns-allinone-3.27/ns-3.27/statistics/cdf/CDF-" + std::to_string(community)  + "-OWNERSHIP.txt", std::ios_base::app);
			break;
		case 2 : fout.open("/home/gustavo/workspace/ns-allinone-3.27/ns-3.27/statistics/cdf/CDF-" + std::to_string(community)  + "-LOCATION.txt", std::ios_base::app);
			break;
		case 3 : fout.open("/home/gustavo/workspace/ns-allinone-3.27/ns-3.27/statistics/cdf/CDF-" + std::to_string(community)  + "-COWORK.txt", std::ios_base::app);
			break;
		case 4 : fout.open("/home/gustavo/workspace/ns-allinone-3.27/ns-3.27/statistics/cdf/CDF-" + std::to_string(community)  + "-SOCIAL.txt", std::ios_base::app);
			break;
		case 1 : fout.open("/home/gustavo/workspace/ns-allinone-3.27/ns-3.27/statistics/cdf/CDF-" + std::to_string(community)  + "-PARENTAL.txt", std::ios_base::app);
			break;
	}

	fout << value << "\t" << frequency;
	fout << "\n";

	fout.close();
}

void Statistics::writeCDFRelationTrustOutputFileCandidates(int siot, double value, double frequency)
{
	switch(siot)
	{
		case 0 : fout.open("/home/gustavo/workspace/ns-allinone-3.27/ns-3.27/statistics/cdf/CDFCandidates-" + std::to_string(community)  + "-OWNERSHIP.txt", std::ios_base::app);
			break;
		case 2 : fout.open("/home/gustavo/workspace/ns-allinone-3.27/ns-3.27/statistics/cdf/CDFCandidates-" + std::to_string(community)  + "-LOCATION.txt", std::ios_base::app);
			break;
		case 3 : fout.open("/home/gustavo/workspace/ns-allinone-3.27/ns-3.27/statistics/cdf/CDFCandidates-" + std::to_string(community)  + "-COWORK.txt", std::ios_base::app);
			break;
		case 4 : fout.open("/home/gustavo/workspace/ns-allinone-3.27/ns-3.27/statistics/cdf/CDFCandidates-" + std::to_string(community)  + "-SOCIAL.txt", std::ios_base::app);
			break;
		case 1 : fout.open("/home/gustavo/workspace/ns-allinone-3.27/ns-3.27/statistics/cdf/CDFCandidates-" + std::to_string(community)  + "-PARENTAL.txt", std::ios_base::app);
			break;
	}

	fout << value << "\t" << frequency;
	fout << "\n";

	fout.close();
}

void Statistics::generateCDF()
{
	processCDF(3, &cdf_cowork, true);
	processCDF(2, &cdf_local, true);
	processCDF(0, &cdf_owner, true);
	processCDF(1, &cdf_paren, true);
	processCDF(4, &cdf_social, true);

	processCDF(3, &cdf_cowork_candidates, false);
	processCDF(2, &cdf_local_candidates, false);
	processCDF(0, &cdf_owner_candidates, false);
	processCDF(1, &cdf_paren_candidates, false);
	processCDF(4, &cdf_social_candidates, false);
}

/*determine distribution quartiles (0 = min, 2 = median, 4 = max)*/
double Statistics::quartile(std::vector<double>* data, uint32_t p)
{
	double q, t, v;

	q = (double) p/4;
	int l = data->size();
	t = (l-1)*q+1; // our array is one-based
	v = (*data)[(int) t];
	if (t > (int)t)
		return v + q * ((*data)[(int)t + 1 ] - v);
	else
		return v;
}

double Statistics::calcmedian(std::vector<double>* data)
{
	return quartile(data, 2);
	int n = data->size();
	if (n % 2)
	{
		/* odd number of samples,*/
		/* median = middle value */
		return (*data)[ (n + 1) / 2 ];
	}
	else
	{
		/* even number of samples,
		   median = average of the two middle values*/
		return ( (*data)[ n/2 ] + (*data)[ n/2 + 1 ] ) / 2;
	}
}

void Statistics::plotTheBox(int siot, std::vector<double>* data, bool nodes)
{
	std::sort(data->begin(), data->end());

	double Q1 = quartile(data, 1);
	double Q3 = quartile(data, 3);
	//double sum = std::accumulate(data->begin(), data->end(), 0.0);

	/* determine n and calculate mean */
	int n = data->size();
	//double mean = sum / n;
	double median = calcmedian(data);

	/* calculate standard deviation, determine min and max */
	double min = (*data)[1];
	double max = (*data)[n-1];

	(nodes) ? fout.open("/home/gustavo/workspace/ns-allinone-3.27/ns-3.27/statistics/boxplot/box-" + std::to_string(community)  + ".txt", std::ios_base::app)
			: fout.open("/home/gustavo/workspace/ns-allinone-3.27/ns-3.27/statistics/boxplot/box-" + std::to_string(community)  + "-candidates.txt", std::ios_base::app);

	switch(siot)
	{
		case 0 :
			/* output */
			fout << "1" << "\t" << min << "\t" << Q1 << "\t" << median << "\t" << Q3 << "\t" << max;
			fout << "\n";
			break;
		case 2 :
			/* output */
			fout << "2" << "\t" << min << "\t" << Q1 << "\t" << median << "\t" << Q3 << "\t" << max;
			fout << "\n";
			break;
		case 3 :
			/* output */
			fout << "3" << "\t" << min << "\t" << Q1 << "\t" << median << "\t" << Q3 << "\t" << max;
			fout << "\n";
			break;
		case 4 :
			/* output */
			fout << "4" << "\t" << min << "\t" << Q1 << "\t" << median << "\t" << Q3 << "\t" << max;
			fout << "\n";
			break;
		case 1 :
			/* output */
			fout << "5" << "\t" << min << "\t" << Q1 << "\t" << median << "\t" << Q3 << "\t" << max;
			fout << "\n";
			break;
	}

	fout.close();
}

void Statistics::generateBoxplot()
{
	/* get upper and lower quartiles and median (2nd quartile)*/
	plotTheBox(3, &boxplot_cowork, false);
	plotTheBox(2, &boxplot_local, false);
	plotTheBox(0, &boxplot_owner, false);
	plotTheBox(1, &boxplot_paren, false);
	plotTheBox(4, &boxplot_social, false);

//	plotTheBox(3, &boxplot_cowork_candidates, false);
//	plotTheBox(2, &boxplot_local_candidates, false);
//	plotTheBox(0, &boxplot_owner_candidates, false);
//	plotTheBox(1, &boxplot_paren_candidates, false);
//	plotTheBox(4, &boxplot_social_candidates, false);

//	fout.open("/home/gustavo/workspace/ns-allinone-3.27/ns-3.27/statistics/boxplot/box-" + std::to_string(community)  + ".txt", std::ios_base::app);
//
//	fout << "# 1\t2\t3\t4\t5\n";
//
//	for(uint32_t i=0; i<boxplot_owner.size(); i++)
//	{
//		fout << boxplot_owner[i] << "\t" << boxplot_paren[i] << "\t" << boxplot_local[i] << "\t" << boxplot_cowork[i] << "\t" << boxplot_social[i];
//		fout << "\n";
//	}
//	fout.close();
}

void Statistics::increseNumMultiStolenId()
{
	this->NUM_ATTACK_MULTI_STOLEN_ID++;
}

uint32_t Statistics::getNumMultiStolenId()
{
	return this->NUM_ATTACK_MULTI_STOLEN_ID;
}

void Statistics::increseNumMultiFabricatedId()
{
	this->NUM_ATTACK_MULTI_FABRICATED_ID++;
}

uint32_t Statistics::getNumMultiFabricatedId()
{
	return this->NUM_ATTACK_MULTI_FABRICATED_ID;
}

void Statistics::increseNumChurnStolenId()
{
	this->NUM_ATTACK_CHURN_STOLEN_ID++;
}

uint32_t Statistics::getNumChurnStolenId()
{
	return this->NUM_ATTACK_CHURN_STOLEN_ID;
}

void Statistics::increseNumChurnFabricatedId()
{
	this->NUM_ATTACK_CHURN_FABRICATED_ID++;
}

uint32_t Statistics::getNumChurnFabricatedId()
{
	return this->NUM_ATTACK_CHURN_FABRICATED_ID;
}

void Statistics::increseNumAccDenied()
{
	this->NUM_NODE_DENIED++;
}

uint32_t Statistics::getTotalAccDenied()
{
	return this->NUM_NODE_DENIED;
}

void Statistics::increseNumMultiStolenIdReal()
{
	this->NUM_ATTACK_MULTI_STOLEN_ID_REAL++;
}

uint32_t Statistics::getNumMultiStolenIdReal()
{
	return this->NUM_ATTACK_MULTI_STOLEN_ID_REAL;
}

void Statistics::increseNumMultiFabricatedReal()
{
	this->NUM_ATTACK_MULTI_FABRICATED_ID_REAL++;
}

uint32_t Statistics::getNumMultiFabricatedReal()
{
	return this->NUM_ATTACK_MULTI_FABRICATED_ID_REAL;
}

void Statistics::increseNumChurnStolenIdReal()
{
	this->NUM_ATTACK_CHURN_STOLEN_ID_REAL++;
}

uint32_t Statistics::getNumChurnStolenIdReal()
{
	return this->NUM_ATTACK_CHURN_STOLEN_ID_REAL;
}

void Statistics::increseNumChurnFabricatedIdReal()
{
	this->NUM_ATTACK_CHURN_FABRICATED_ID_REAL++;
}

uint32_t Statistics::getNumChurnFabricatedIdReal()
{
	return this->NUM_ATTACK_CHURN_FABRICATED_ID_REAL;
}

void Statistics::increseNumLegalReq()
{
	this->NUM_LEGAL_REQ++;
}

uint32_t Statistics::getNumLegalReq()
{
	return this->NUM_LEGAL_REQ;
}

void Statistics::increseNumTotalReq()
{
	this->NUM_TOTAL_REQ++;
}

uint32_t Statistics::getNumTotalReq()
{
	return this->NUM_TOTAL_REQ;
}

void Statistics::increseNumTruePositive()
{
	this->NUM_TRUE_POSITIVE++;
}

uint32_t Statistics::getNumTruePositive()
{
	return this->NUM_TRUE_POSITIVE;
}

void Statistics::increseNumFalsePositive()
{
	this->NUM_FALSE_POSITIVE++;
}

uint32_t Statistics::getNumFalsePositive()
{
	return this->NUM_FALSE_POSITIVE;
}

void Statistics::increseNumFalseNegative()
{
	this->NUM_FALSE_NEGATIVE++;
}

uint32_t Statistics::getNumFalseNegative()
{
	return this->NUM_FALSE_NEGATIVE;
}

void Statistics::increseNumAttack()
{
	this->NUM_ATTACK++;
}

uint32_t Statistics::getNumAttack()
{
	return this->NUM_ATTACK;
}

void Statistics::increseNumAttackReal()
{
	this->NUM_ATTACK_REAL++;
}

uint32_t Statistics::getNumAttackReal()
{
	return this->NUM_ATTACK_REAL;
}

void Statistics::energyConsumption(double volt)
{
	fout.open("/home/gustavo/workspace/ns-allinone-3.27/ns-3.27/statistics/energy/nodeConsumption-" + std::to_string(community)  + ".txt", std::ios_base::app);

	fout << Simulator::Now ().GetSeconds () << "\t" << volt;
	fout << "\n";

	fout.close();
}

/* -------------- Detected Rate ----------- */
float Statistics:: calculateStandardDeviation(std::map<double, double>& detectedRate)
{
    float sum = 0.0, mean, standardDeviation = 0.0;

    for(auto & r : detectedRate)
    {
        sum += r.second;
    }

    mean = sum/detectedRate.size();

    for(auto & r : detectedRate)
    {
        standardDeviation += pow(r.second - mean, 2);
    }

    return sqrt(standardDeviation / detectedRate.size());
}

void Statistics::dectecedRate()
{
	Time time = Simulator::Now ();
	double val = (double) NUM_ATTACK/NUM_ATTACK_REAL;
	val = val*100; //Percentage
	detectedRate.insert( std::pair<double, double>(time.GetSeconds (), val));
	detectedRateAvg->Update(val);
}

void Statistics::writedectecedRate()
{
	fout.open("/home/gustavo/workspace/ns-allinone-3.27/ns-3.27/statistics/detectedRate/detectedRate-" + std::to_string(community)  + ".txt", std::ios_base::app);

	float sd = calculateStandardDeviation(detectedRate);

	for(auto & r : detectedRate)
	{
		fout << r.first << "\t" << r.second << "\t" << sd;
		fout << "\n";
	}

	fout.close();
}

void Statistics::dectecedRateMultiId()
{
	Time time = Simulator::Now ();
	double val = (double) NUM_ATTACK_MULTI_STOLEN_ID/NUM_ATTACK_MULTI_STOLEN_ID_REAL;
	val = val*100; //Percentage
	detectedRateMultIdStolen.insert( std::pair<double, double>(time.GetSeconds (), val));

	val = (double) NUM_ATTACK_MULTI_FABRICATED_ID/NUM_ATTACK_MULTI_FABRICATED_ID_REAL;
	val = val*100; //Percentage
	detectedRateMultiIdFabricated.insert( std::pair<double, double>(time.GetSeconds (), val));
}

void Statistics::writedectecedRateMultiId()
{
	fout.open("/home/gustavo/workspace/ns-allinone-3.27/ns-3.27/statistics/detectedRate/detectedRateMultiIdStolen-" + std::to_string(community)  + ".txt", std::ios_base::app);

	float sd = calculateStandardDeviation(detectedRateMultIdStolen);

	for(auto & r : detectedRateMultIdStolen)
	{
		fout << r.first << "\t" << r.second << "\t" << sd;
		fout << "\n";
	}

	fout.close();

	fout.open("/home/gustavo/workspace/ns-allinone-3.27/ns-3.27/statistics/detectedRate/detectedRateMultiIdFabricated-" + std::to_string(community)  + ".txt", std::ios_base::app);

	sd = calculateStandardDeviation(detectedRateMultiIdFabricated);

	for(auto & r : detectedRateMultiIdFabricated)
	{
		fout << r.first << "\t" << r.second << "\t" << sd;
		fout << "\n";
	}

	fout.close();
}

void Statistics::dectecedRateChurn()
{
	Time time = Simulator::Now ();
	double val = (double) NUM_ATTACK_CHURN_STOLEN_ID/NUM_ATTACK_CHURN_STOLEN_ID_REAL;
	val = val*100; //Percentage
	detectedRateChurnStolen.insert( std::pair<double, double>(time.GetSeconds (), val));

	val = (double) NUM_ATTACK_CHURN_FABRICATED_ID/NUM_ATTACK_CHURN_FABRICATED_ID_REAL;
	val = val*100; //Percentage
	detectedRateChurnFabricated.insert( std::pair<double, double>(time.GetSeconds (), val));
}

void Statistics::writedectecedRateChurn()
{
	fout.open("/home/gustavo/workspace/ns-allinone-3.27/ns-3.27/statistics/detectedRate/detectedRateChurnStolen-" + std::to_string(community)  + ".txt", std::ios_base::app);

	float sd = calculateStandardDeviation(detectedRateChurnStolen);

	for(auto & r : detectedRateChurnStolen)
	{
		fout << r.first << "\t" << r.second << "\t" << sd;
		fout << "\n";
	}

	fout.close();

	fout.open("/home/gustavo/workspace/ns-allinone-3.27/ns-3.27/statistics/detectedRate/detectedRateChurnFabricated-" + std::to_string(community)  + ".txt", std::ios_base::app);

	sd = calculateStandardDeviation(detectedRateChurnFabricated);

	for(auto & r : detectedRateChurnFabricated)
	{
		fout << r.first << "\t" << r.second << "\t" << sd;
		fout << "\n";
	}

	fout.close();
}

void Statistics::writeFalsesNegativesRate()
{
	if(Simulator::Now ().GetSeconds ()>0)
	{
		fout.open("/home/gustavo/workspace/ns-allinone-3.27/ns-3.27/statistics/falsesNegatives/falseNegativeRate-" + std::to_string(community)  + ".txt", std::ios_base::app);

		uint32_t total = NUM_FALSE_NEGATIVE+NUM_TRUE_POSITIVE;

		double fn = (double) NUM_FALSE_NEGATIVE/total;
		fn = fn*100; //Percentage

		fout << Simulator::Now ().GetSeconds () << "\t" << fn;
		fout << "\n";

		fout.close();
	}
}

void Statistics::writeAccuracyDetection()
{
	fout.open("/home/gustavo/workspace/ns-allinone-3.27/ns-3.27/statistics/accuracy/AccuracyDetection-" + std::to_string(community)  + ".txt", std::ios_base::app);

//	uint32_t tAttacks = NUM_ATTACK_MULTI_STOLEN_ID+NUM_ATTACK_MULTI_FABRICATED_ID+NUM_ATTACK_CHURN_STOLEN_ID+NUM_ATTACK_CHURN_FABRICATED_ID;
//	uint32_t true_negative = NUM_TOTAL_REQ - tAttacks;
	uint32_t true_negative = NUM_TOTAL_REQ - NUM_ATTACK;
	double accuracy = (double) (NUM_TRUE_POSITIVE+true_negative)/(NUM_TRUE_POSITIVE+true_negative+NUM_FALSE_POSITIVE+NUM_FALSE_NEGATIVE);
	if(accuracy>1.) accuracy = 1.0;

	fout << Simulator::Now ().GetSeconds () << "\t" << accuracy;
	fout << "\n";


	fout.close();
}

void Statistics::writeFalsesPositivesRate()
{
	fout.open("/home/gustavo/workspace/ns-allinone-3.27/ns-3.27/statistics/falsesPositives/falsePositiveRate-" + std::to_string(community)  + ".txt", std::ios_base::app);

	double fp = (double) NUM_FALSE_POSITIVE/NUM_TOTAL_REQ;
	fp = fp*100; //Percentage

	fout << Simulator::Now ().GetSeconds () << "\t" << fp;
	fout << "\n";

	fout.close();
}

void Statistics::writeThis(uint32_t id, double t)
{
	fout.open("/home/gustavo/workspace/ns-allinone-3.27/ns-3.27/statistics/trustAttacker/trust-" + std::to_string(community)  + ".txt", std::ios_base::app);

	fout << Simulator::Now ().GetSeconds () << "\t" << id << "\t" << t;
	fout << "\n";

	fout.close();
}

double Statistics::Output()
{
	//------------------------------------------------------------
	//-- Generate statistics output.
	//--------------------------------------------

	// Pick an output writer based in the requested format.
	Ptr<DataOutputInterface> output = 0;
	#ifdef STATS_HAS_SQLITE3
	  output = CreateObject<SqliteDataOutput>();
	#endif

	// Finally, have that writer interrogate the DataCollector and save
	// the results.
	if (output != 0)
	  output->Output (data);

	return detectedRateAvg->getMean();
}

#endif
