#include "ACManager.h"

#ifndef _SENDPACKET_H_
#define _SENDPACKET_H_

std::vector<Ptr<AccessControl>> acs;
DWORD memory;

void DisplayMessage(Ptr<Socket> socket);
void DCNET(Ptr<Socket> socket);

//----------------------------------------------------------------------------------
// Change the position
//----------------------------------------------------------------------------------
//Declarations
uint32_t m_bytesTotal;
uint32_t lastNode = 1;
bool positionControl = true;

int randomBitGeneratorWithProb(double p)
{
    double rndDouble = (double)rand() / RAND_MAX;
    return rndDouble > p;
}

//Fuction to change positions - Random mobility ------------
void SetPosition (Ptr<Node> node, Vector position)
{
  Ptr<MobilityModel> mobility = node->GetObject<MobilityModel> ();
  mobility->SetPosition (position);
}

Vector GetPosition (Ptr<Node> node)
{
  Ptr<MobilityModel> mobility = node->GetObject<MobilityModel> ();
  return mobility->GetPosition ();
}

uint32_t GetRandomNode()
{
	srand (time(NULL));
    uint32_t random_integer;
    random_integer = (rand() % communities[0].GetN());
    if (random_integer == lastNode){
    	random_integer = (rand() % communities[0].GetN());
    }
    NS_LOG_INFO ("Set new position to : " << random_integer);
    lastNode = random_integer;
    return random_integer;

}

void AdvancePosition ()
{

  Ptr<Node> node = communities[0].Get(GetRandomNode());

  Vector pos = GetPosition (node);
  //double mbs = ((m_bytesTotal * 8.0) / 1000000);
  m_bytesTotal = 0;
  //m_output.Add (pos.x, mbs);
  if (positionControl){
	  pos.x += 1.0;
	  pos.y += 1.5;
	  positionControl = false;
	  if (pos.x >= 210.0)
	  	{
	  	  return;
	  	}
  }else{
	  pos.x -= 0.8;
	  pos.y -= 1.1;
	  positionControl = true;
	  if (pos.x == 0.0 || pos.y == 0.0)
	  	{
	  	  return;
	  	}
  }
  SetPosition (node, pos);
  Simulator::Schedule (Seconds (5.0), &AdvancePosition);
}

static void  PrintCellInfo (Ptr<LiIonEnergySource> es, Ptr<Node> nodebatery, uint32_t community)
{
	NS_LOG_INFO ("[BATTERY] "  << "simulation time: " << Simulator::Now ().GetSeconds () <<  " node: " << nodebatery->GetId()<< " voltage: " << es->GetSupplyVoltage () << " remaining power: " <<
	es->GetRemainingEnergy () / (3.6 * 3600) << " Ah");

	if (!Simulator::IsFinished ())
	  Simulator::Schedule (Seconds (1), &PrintCellInfo, es, nodebatery, community);

	if(cMap::getNodeIndex(nodebatery->GetId(), community) == 0)
		statistics[community]->energyConsumption(es->GetSupplyVoltage ());

}
/*-------- End ----------*/

//----------------------------------------------------------------------------------
// Callback functions
//----------------------------------------------------------------------------------

/*-------- Callbacks -------- */

void MakeACallbackSocialTrust()
{
	NS_LOG_INFO ("---------- MekeACallbackSocialTrust --------");
	for(uint32_t i=0; i<g_tCommunities; i++)
	{
		for (uint32_t index1 = 0; index1 < g_cSizes[i]; index1++)
		{
			for (uint32_t index2 = 0; index2 < g_cSizes[i]; index2++)
			{
				if(index1 != index2)
				{
					acs[i]->callBackSocial(index1, index2);
				}
			}
		}
	}
	Simulator::Schedule (Seconds (1.0), &MakeACallbackSocialTrust);
}

void MakeACallbackSocialTrustCandidates()
{
	NS_LOG_INFO ("---------- MakeACallbackSocialTrustCandidates --------");
	for(uint32_t i=0; i<g_tCommunities; i++)
	{
		for (uint32_t index1 = 0; index1 < g_cSizes[i]; index1++)
		{
			for (uint32_t index2 = g_cSizes[i]; index2 < g_tNodesInComm; index2++)
			{
				if(index1 != index2)
				{
					acs[i]->callBackSocialCandidates(index1, index2);
				}
			}
		}
	}
	Simulator::Schedule (Seconds (1.0), &MakeACallbackSocialTrustCandidates);
}

void MakeACallbackSocialTrust3d()
{
	NS_LOG_INFO ("---------- MakeACallbackSocialTrust3d --------");
	if(candidatesOn)
	{
		for(uint32_t i=0; i<g_tCommunities; i++)
		{
			for (uint32_t index1 = 0; index1 < g_cSizes[i]; index1++)
			{
				for (uint32_t index2 = 0; index2 < g_tNodesInComm; index2++)
				{
					if(index1 != index2)
					{
						acs[i]->callBackSocial3d(index1, index2);
					}
				}
			}
		}
	}else
	{
		for(uint32_t i=0; i<g_tCommunities; i++)
		{
			for (uint32_t index1 = 0; index1 < g_cSizes[i]; index1++)
			{
				for (uint32_t index2 = 0; index2 < g_cSizes[i]; index2++)
				{
					if(index1 != index2)
					{
						acs[i]->callBackSocial3d(index1, index2);
					}
				}
			}
		}
	}

	Simulator::Schedule (Seconds (1.0), &MakeACallbackSocialTrust3d);
}

void MakeCallbackDetectedRate()
{
	NS_LOG_INFO ("---------- MakeCallbackDetectedRate --------");
	for(uint32_t i=0; i<g_tCommunities; i++)
	{
		statistics[i]->dectecedRate();
//		statistics[i]->dectecedRateMultiId();
//		statistics[i]->dectecedRateChurn();
	}

	Simulator::Schedule (Seconds (100.0), &MakeCallbackDetectedRate);
}

void MakeCallbackfalsePositiveRate()
{
	NS_LOG_INFO ("---------- MakeCallbackDetectedRate --------");
	for(uint32_t i=0; i<g_tCommunities; i++)
	{
		statistics[i]->writeFalsesNegativesRate();
	}

	Simulator::Schedule (Seconds (100.0), &MakeCallbackfalsePositiveRate);
}

void MakeACallbackBoxplot()
{
	for(uint32_t i=0; i<g_tCommunities; i++)
	{
		statistics[i]->generateBoxplot();
	}
}

void MakeCallbackAccuracyDetection()
{
	NS_LOG_INFO ("---------- MakeCallbackDetectedRate --------");
	for(uint32_t i=0; i<g_tCommunities; i++)
	{
		statistics[i]->writeAccuracyDetection();
	}

	Simulator::Schedule (Seconds (100.0), &MakeCallbackAccuracyDetection);
}

void MakeCallbackFalsePositiveRate()
{
	NS_LOG_INFO ("---------- MakeCallbackDetectedRate --------");
	for(uint32_t i=0; i<g_tCommunities; i++)
	{
		statistics[i]->writeFalsesPositivesRate();
	}

	Simulator::Schedule (Seconds (100.0), &MakeCallbackFalsePositiveRate);
}

//----------------------------------------------------------------------------------
// Trust functions
//----------------------------------------------------------------------------------

///* --- Update Recommendation trust ---*/

void UpdateRecommendationMatrix()
{
	for(uint32_t i=0; i<g_tCommunities; i++)
	{
		acs[i]->directToRecommendation();
	}
}

static void SendMsgTrust (Ptr<Socket> socket, std::string message, uint32_t index, uint32_t community)
{
	NS_LOG_INFO ("---------- SendMsgTrust --------");
    Ptr<Packet> sendPacket = Create<Packet> ((uint8_t*)message.c_str(),message.size());

    MyTag sendTag;
    sendTag.SetSimpleValue(index);
    sendTag.SetCommunityValue(community);
    sendPacket->AddPacketTag(sendTag);
    socket->Send (sendPacket);
    stage2SentPacketCount += 1;//increment sent packet counter for stage2
    socket->Close ();
}

void ReceiveMsgTrust (Ptr<Socket> socket)
{
	NS_LOG_INFO ("---------- ReceiveMsgTrust --------");
    Ptr<Packet> recPacket = socket->Recv();
    stage2RecvPacketCount += 1;//increment recv packet counter for stage2

    Ptr<Node> recvnode = socket->GetNode();

    uint8_t *buffer = new uint8_t[recPacket->GetSize()];
    recPacket->CopyData(buffer,recPacket->GetSize());

    std::string recMessage = std::string((char*)buffer);
    recMessage = recMessage.substr (0,messageLen-1);

    MyTag recTag;
    recPacket->PeekPacketTag(recTag);
    uint32_t srcNodeIndex = recTag.GetSimpleValue();
	uint32_t community = recTag.GetCommunityValue();

	uint32_t recNodeIndex = apps[community]->getNodeFromMap(recvnode);

    SecByteBlock key(SHA256::DIGESTSIZE);
    SHA256().CalculateDigest(key, apps[community]->getSecretKeyFromGlobalMap(srcNodeIndex,recNodeIndex), apps[community]->getSecretKeyFromGlobalMap(srcNodeIndex,recNodeIndex).size());

    //Decryption using the Shared secret key
    CFB_Mode<AES>::Decryption cfbDecryption(key, aesKeyLength, iv);
    cfbDecryption.ProcessData((byte*)recMessage.c_str(), (byte*)recMessage.c_str(), messageLen);

    int value = atoi(recMessage.c_str());
    NS_LOG_INFO ("-------- Message: " << value);

    acs[community]->updateRecommendation(srcNodeIndex, recNodeIndex);

}

void IncreaseRecommendations()
{
	NS_LOG_INFO ("-------- Update recommendation trust  -----------");
	simulatorCounter = (((g_tNodesInComm/g_tCommunities) * (g_tNodesInComm/g_tCommunities)) - (g_tNodesInComm/g_tCommunities)) * g_tCommunities;
	// Generate a random IV
	rnd.GenerateBlock(iv, AES::BLOCKSIZE);

	for (uint32_t i=0; i < g_tCommunities; i++)
	{
		//sharing the random bit using dh secret key
		for (uint32_t index1 = 0; index1 < g_cSizes[i]; index1++)
		{
			for (uint32_t index2 = 0; index2 < g_cSizes[i]; index2++)
			{
				if(index1 != index2)
				{

					uint32_t randomBit = randomBitGeneratorWithProb(0.5);
					// Calculate a SHA-256 hash over the Diffie-Hellman session key
					SecByteBlock key(SHA256::DIGESTSIZE);
					SHA256().CalculateDigest(key, apps[i]->getSecretKeyFromGlobalMap(index1,index2), apps[i]->getSecretKeyFromGlobalMap(index1,index2).size());

					std::ostringstream ss;
					ss << randomBit;
					std::string message = ss.str();
					messageLen = (int)strlen(message.c_str()) + 1;

					// Encrypt
					CFB_Mode<AES>::Encryption cfbEncryption(key, aesKeyLength, iv);
					cfbEncryption.ProcessData((byte*)message.c_str(), (byte*)message.c_str(), messageLen);

					//Send the encrypted message
					Ptr<Socket> recvNodeSink = Socket::CreateSocket (communities[i].Get (index2), tid);
					Inet6SocketAddress localSocket = Inet6SocketAddress (Ipv6Address::GetAny (), 9804);
					recvNodeSink->Bind (localSocket);
					recvNodeSink->SetRecvCallback (MakeCallback (&ReceiveMsgTrust));

					Inet6SocketAddress remoteSocket = Inet6SocketAddress (ipv6Containers[i].GetAddress (index2, 0), 9804);
					Ptr<Socket> sourceNodeSocket = Socket::CreateSocket (communities[i].Get (index1), tid);
					sourceNodeSocket->Connect (remoteSocket);
					Simulator::ScheduleNow(&SendMsgTrust, sourceNodeSocket,message,index1,i);
				}
			}
		}
	}
}

/* --------- update Direct trust ------- */
static void SendTask (Ptr<Socket> socket, std::string message, uint32_t index, uint32_t community, bool reply)
{
	NS_LOG_INFO ("---------- sendTask --------");
    Ptr<Packet> sendPacket = Create<Packet> ((uint8_t*)message.c_str(),message.size());

    MyTag sendTag;
    sendTag.SetSimpleValue(index);
    sendTag.SetCommunityValue(community);
    sendTag.SetTaskCommunication(reply);
    sendPacket->AddPacketTag(sendTag);
    socket->Send (sendPacket);
    //stage2SentPacketCount += 1;//increment sent packet counter for stage2
    socket->Close ();
}

void ReceiveTask (Ptr<Socket> socket)
{
	NS_LOG_INFO ("---------- ReceiveTask --------");
    Ptr<Packet> recPacket = socket->Recv();
    //stage2RecvPacketCount += 1;//increment recv packet counter for stage2

    Ptr<Node> recvnode = socket->GetNode();

    uint8_t *buffer = new uint8_t[recPacket->GetSize()];
    recPacket->CopyData(buffer,recPacket->GetSize());

    std::string recMessage = std::string((char*)buffer);
    messageLen = (int)strlen(recMessage.c_str()) + 1;
    recMessage = recMessage.substr (0,messageLen-1);

    MyTag recTag;
    recPacket->PeekPacketTag(recTag);
    uint32_t trustor = recTag.GetSimpleValue();
	uint32_t community = recTag.GetCommunityValue();
	bool reply = recTag.GetTaskCommunication();

	uint32_t trustee = apps[community]->getNodeFromMap(recvnode);

    SecByteBlock key(SHA256::DIGESTSIZE);
    SHA256().CalculateDigest(key, apps[community]->getSecretKeyFromGlobalMap(trustor,trustee), apps[community]->getSecretKeyFromGlobalMap(trustor,trustee).size());

    //Decryption using the Shared secret key
    CFB_Mode<AES>::Decryption cfbDecryption(key, aesKeyLength, iv);
    cfbDecryption.ProcessData((byte*)recMessage.c_str(), (byte*)recMessage.c_str(), messageLen);


    if(!reply)
    {
    	NS_LOG_INFO ("Message to reply: " << recMessage << " from: " << trustor << " to " << trustee);
		// Encrypt
		CFB_Mode<AES>::Encryption cfbEncryption(key, aesKeyLength, iv);
		cfbEncryption.ProcessData((byte*)recMessage.c_str(), (byte*)recMessage.c_str(), messageLen);

		/* Send the reply for the trustor */
		Ptr<Socket> trustorSocket = Socket::CreateSocket (communities[community].Get (trustor), tid);
		Inet6SocketAddress trustorAdd = Inet6SocketAddress (Ipv6Address::GetAny (), 9805);
		trustorSocket->Bind (trustorAdd);
		trustorSocket->SetRecvCallback (MakeCallback (&ReceiveTask));

		Ptr<Socket> trusteeSocket = Socket::CreateSocket (communities[community].Get (trustee), tid);
		Inet6SocketAddress trusteeAdd = Inet6SocketAddress (ipv6Containers[community].GetAddress (trustor, 0), 9805);
		trusteeSocket->Connect (trusteeAdd);
		Simulator::ScheduleNow(&SendTask, trusteeSocket,recMessage,trustee,community, 1);
    }else
    {
    	NS_LOG_INFO ("Reply from candidate trustee: " << recMessage << " from: " << trustor << " to " << trustee);
    	if(recMessage.compare(std::to_string(trustor)) == 0)
			acs[community]->incrementPositives(trustor, trustee);
    	else
			acs[community]->incrementNegatives(trustor, trustee);
    }
}

void TasksForDirectTrust()
{
	NS_LOG_INFO ("-------- Update direct trust  ----------- At: " << Seconds(Simulator::Now().GetSeconds()));
	simulatorCounter = (((g_tNodesInComm/g_tCommunities) * (g_tNodesInComm/g_tCommunities)) - (g_tNodesInComm/g_tCommunities)) * g_tCommunities;
	// Generate a random IV
	rnd.GenerateBlock(iv, AES::BLOCKSIZE);

	for (uint32_t i=0; i < g_tCommunities; i++)
	{
		//sharing the random bit using dh secret key
		for (uint32_t index1 = 0; index1 < g_cSizes[i]; index1++)
		{
			for (uint32_t index2 = 0; index2 < g_cSizes[i]; index2++)
			{
				if(index1 != index2)
				{
					// Calculate a SHA-256 hash over the Diffie-Hellman session key
					SecByteBlock key(SHA256::DIGESTSIZE);
					SHA256().CalculateDigest(key, apps[i]->getSecretKeyFromGlobalMap(index1,index2), apps[i]->getSecretKeyFromGlobalMap(index1,index2).size());

					std::ostringstream ss;
					ss << index2;
					std::string message = ss.str();
					messageLen = (int)strlen(message.c_str()) + 1;

					// Encrypt
					CFB_Mode<AES>::Encryption cfbEncryption(key, aesKeyLength, iv);
					cfbEncryption.ProcessData((byte*)message.c_str(), (byte*)message.c_str(), messageLen);

					/* Send the task for neighbor */
					Ptr<Socket> trusteeSocket = Socket::CreateSocket (communities[i].Get (index2), tid);
					Inet6SocketAddress trusteeAdd = Inet6SocketAddress (Ipv6Address::GetAny (), 9805);
					trusteeSocket->Bind (trusteeAdd);
					trusteeSocket->SetRecvCallback (MakeCallback (&ReceiveTask));

					Ptr<Socket> trustorSocket = Socket::CreateSocket (communities[i].Get (index1), tid);
					Inet6SocketAddress trustorAdd = Inet6SocketAddress (ipv6Containers[i].GetAddress (index2, 0), 9805);
					trustorSocket->Connect (trustorAdd);
					Simulator::ScheduleNow(&SendTask, trustorSocket,message,index1,i, 0);
				}
			}
		}
	}

	Simulator::Schedule (Seconds (10.0), &TasksForDirectTrust);
	Simulator::Schedule (Seconds (15.0), &IncreaseRecommendations);
}

static void SendMsgTrustCandidates (Ptr<Socket> socket, std::string message, uint32_t index, uint32_t community)
{
	NS_LOG_INFO ("---------- SendMsgTrustCandidates --------");
    Ptr<Packet> sendPacket = Create<Packet> ((uint8_t*)message.c_str(),message.size());

    MyTag sendTag;
    sendTag.SetSimpleValue(index);
    sendTag.SetCommunityValue(community);
    sendPacket->AddPacketTag(sendTag);
    socket->Send (sendPacket);
    stage2SentPacketCount += 1;//increment sent packet counter for stage2
    socket->Close ();
}

void ReceiveMsgTrustCandidates (Ptr<Socket> socket)
{
	NS_LOG_INFO ("---------- ReceiveMsgTrustCandidates --------");
    Ptr<Packet> recPacket = socket->Recv();
    stage2RecvPacketCount += 1;//increment recv packet counter for stage2

    Ptr<Node> recvnode = socket->GetNode();

    uint8_t *buffer = new uint8_t[recPacket->GetSize()];
    recPacket->CopyData(buffer,recPacket->GetSize());

    std::string recMessage = std::string((char*)buffer);
    recMessage = recMessage.substr (0,messageLen-1);

    MyTag recTag;
    recPacket->PeekPacketTag(recTag);
    uint32_t srcNodeIndex = recTag.GetSimpleValue();
	uint32_t community = recTag.GetCommunityValue();

	uint32_t recNodeIndex = apps[community]->getNodeFromMap(recvnode);

    int value = atoi(recMessage.c_str());
    NS_LOG_INFO ("-------- Message candidates: " << value);

    acs[community]->updateRecommendationCandidates(srcNodeIndex, recNodeIndex);

}

void IncreaseRecommendationsCandidates()
{
	NS_LOG_INFO ("-------- Update recommendation trust candidates -----------");
	simulatorCounter = (((g_tNodesInComm/g_tCommunities) * (g_tNodesInComm/g_tCommunities)) - (g_tNodesInComm/g_tCommunities)) * g_tCommunities;

	for (uint32_t i=0; i < g_tCommunities; i++)
	{
		//sharing the random bit using dh secret key
		for (uint32_t index1 = 0; index1 < g_cSizes[i]; index1++)
		{
			for (uint32_t index2 = g_cSizes[i]; index2 < g_tNodesInComm; index2++)
			{
				if(index1 != index2)
				{
					std::ostringstream ss;
					ss << index1;
					std::string message = ss.str();

					//Send the encrypted message
					Ptr<Socket> recvNodeSink = Socket::CreateSocket (communities[i].Get (index2), tid);
					Inet6SocketAddress localSocket = Inet6SocketAddress (Ipv6Address::GetAny (), 9812);
					recvNodeSink->Bind (localSocket);
					recvNodeSink->SetRecvCallback (MakeCallback (&ReceiveMsgTrustCandidates));

					Inet6SocketAddress remoteSocket = Inet6SocketAddress (ipv6Containers[i].GetAddress (index2, 0), 9812);
					Ptr<Socket> sourceNodeSocket = Socket::CreateSocket (communities[i].Get (index1), tid);
					sourceNodeSocket->Connect (remoteSocket);
					Simulator::ScheduleNow(&SendMsgTrustCandidates, sourceNodeSocket,message,index1,i);
				}
			}
		}
	}
}

static void SendTaskCandidates (Ptr<Socket> socket, std::string message, uint32_t index, uint32_t community, bool reply)
{
	NS_LOG_INFO ("---------- SendTaskCandidates --------");
    Ptr<Packet> sendPacket = Create<Packet> ((uint8_t*)message.c_str(),message.size());

    MyTag sendTag;
    sendTag.SetSimpleValue(index);
    sendTag.SetCommunityValue(community);
    sendTag.SetTaskCommunication(reply);
    sendPacket->AddPacketTag(sendTag);
    socket->Send (sendPacket);
    //stage2SentPacketCount += 1;//increment sent packet counter for stage2
    socket->Close ();
}

void ReceiveTaskCandidates (Ptr<Socket> socket)
{
	NS_LOG_INFO ("---------- ReceiveTaskCandidates --------");
    Ptr<Packet> recPacket = socket->Recv();
    //stage2RecvPacketCount += 1;//increment recv packet counter for stage2

    Ptr<Node> recvnode = socket->GetNode();

    uint8_t *buffer = new uint8_t[recPacket->GetSize()];
    recPacket->CopyData(buffer,recPacket->GetSize());

    std::string recMessage = std::string((char*)buffer);

    MyTag recTag;
    recPacket->PeekPacketTag(recTag);
    uint32_t trustor = recTag.GetSimpleValue();
	uint32_t community = recTag.GetCommunityValue();
	bool reply = recTag.GetTaskCommunication();

	uint32_t trustee = apps[community]->getNodeFromMap(recvnode);

    if(!reply)
    {
    	NS_LOG_INFO ("Message to reply: " << recMessage << " from: " << trustor << " to " << trustee);

    	std::vector<Ptr<Sybil>> attackers = sybils.at(community);
		for(auto & a : attackers)
		{
			if(cMap::getNodeIndex(a->getId(), community) == trustee && a->getGoodBehaviour() == false)
			{
				recMessage += "sybil";
				NS_LOG_INFO ("This is a Sybil node, changing message to : " << recMessage);
			}
		}


		/* Send the reply for the trustor */
		Ptr<Socket> trustorSocket = Socket::CreateSocket (communities[community].Get (trustor), tid);
		Inet6SocketAddress trustorAdd = Inet6SocketAddress (Ipv6Address::GetAny (), 9810);
		trustorSocket->Bind (trustorAdd);
		trustorSocket->SetRecvCallback (MakeCallback (&ReceiveTaskCandidates));

		Ptr<Socket> trusteeSocket = Socket::CreateSocket (communities[community].Get (trustee), tid);
		Inet6SocketAddress trusteeAdd = Inet6SocketAddress (ipv6Containers[community].GetAddress (trustor, 0), 9810);
		trusteeSocket->Connect (trusteeAdd);
		Simulator::ScheduleNow(&SendTaskCandidates, trusteeSocket,recMessage,trustee,community, 1);
    }else
    {
    	NS_LOG_INFO ("Reply from candidate trustee: " << recMessage << " from: " << trustor << " to " << trustee);

    	if(recMessage.compare(std::to_string(trustor)) == 0)
			acs[community]->incrementPositives(trustee, trustor);
		else
			acs[community]->incrementNegatives(trustee, trustor);
    }
}

void TaskDirectTrustForCandidates()
{
	NS_LOG_INFO ("-------- Update direct trust for the candidates ----------- At: " << Seconds(Simulator::Now().GetSeconds()));
	simulatorCounter = (((g_tNodesInComm/g_tCommunities) * (g_tNodesInComm/g_tCommunities)) - (g_tNodesInComm/g_tCommunities)) * g_tCommunities;

	for (uint32_t i=0; i < g_tCommunities; i++)
	{
		//sharing the random bit using dh secret key
		for (uint32_t index1 = 0; index1 < g_cSizes[i]; index1++)
		{
			for (uint32_t index2 = g_cSizes[i]; index2 < g_tNodesInComm; index2++)
			{
				std::ostringstream ss;
				ss << index2;
				std::string message = ss.str();

				/* Send the task for neighbor */
				Ptr<Socket> trusteeSocket = Socket::CreateSocket (communities[i].Get (index2), tid);
				Inet6SocketAddress trusteeAdd = Inet6SocketAddress (Ipv6Address::GetAny (), 9810);
				trusteeSocket->Bind (trusteeAdd);
				trusteeSocket->SetRecvCallback (MakeCallback (&ReceiveTaskCandidates));

				Ptr<Socket> trustorSocket = Socket::CreateSocket (communities[i].Get (index1), tid);
				Inet6SocketAddress trustorAdd = Inet6SocketAddress (ipv6Containers[i].GetAddress (index2, 0), 9810);
				trustorSocket->Connect (trustorAdd);
				Simulator::ScheduleNow(&SendTaskCandidates, trustorSocket,message,index1,i, 0);
			}
		}
	}

	Simulator::Schedule (Seconds (10.0), &TaskDirectTrustForCandidates);
	Simulator::Schedule (Seconds (15.0), &IncreaseRecommendationsCandidates);
}

//----------------------------------------------------------------------------------
// Attack functions
//----------------------------------------------------------------------------------

/* ----------- Starting attack ------------ */
void StartSybil()
{
	NS_LOG_INFO ("---------- startSybil --------");
	std::vector<Ptr<Sybil>> tmp;
	//uint32_t nAttacker = g_tAttackers/g_tCommunities;
	int nAttackerGoodBehaviour, aux;

	if(g_tAttackers < 10)
		nAttackerGoodBehaviour = 1;
	else
		nAttackerGoodBehaviour = (int) g_tAttackers * 0.1;

	aux = nAttackerGoodBehaviour;
	for(uint32_t i=0; i<g_tCommunities; i++)
	{
		for(uint32_t j=0; j<g_tAttackers; j++)
		{
			if(aux>0)
				tmp.push_back(CreateObject<Sybil>(communities[i].Get(g_tNodesInComm-(j+1)), i, g_cSizes[i], true));
			else
				tmp.push_back(CreateObject<Sybil>(communities[i].Get(g_tNodesInComm-(j+1)), i, g_cSizes[i], false));

			if(aux>0)
				aux--;
		}
		sybils.insert(std::pair<uint32_t, std::vector<Ptr<Sybil>>>(i, tmp));
		aux = nAttackerGoodBehaviour;
		tmp.clear();
	}
}

void StartCandidates()
{
	NS_LOG_INFO ("---------- startCandidates --------");
	for(uint32_t i=0; i<g_tCommunities; i++)
	{
		acs[i]->configCandidates();
	}
	candidatesOn = true;
}

void AllCandidatesRequestAccess()
{
	NS_LOG_INFO ("---------- AllCandidatesRequestAccess --------");
	Timestamp timestamp;
	std::vector<uint32_t> identity;
	for(uint32_t i=0; i<g_tCommunities; i++)
	{
		/* ---- only no attackers nodes request access here ----*/
		for (uint32_t index1 = g_cSizes[i]; index1 < (g_tNodesInComm-sybils[i].size()); index1++)
		{
			//memory = (DWORD) GetPointer (communities[i].Get(index1));
			identity.push_back(index1);
			identity.push_back(index1);
			timestamp.SetTimestamp(Simulator::Now());
			acs[i]->requestAccess(identity, timestamp);
			statistics[i]->increseNumLegalReq();
			statistics[i]->increseNumTotalReq();
			identity.clear();
		}
	}

	Simulator::Schedule (Seconds (15.0), &AllCandidatesRequestAccess);
}

void Attacking(bool attackType, bool idType, uint32_t community)
{
	NS_LOG_INFO ("---------- Attacking --------");

	Timestamp timestamp;
	std::vector<uint32_t> identity;
	std::vector<Ptr<Sybil>> attackers = sybils.at(community);
	if(attackType)
	{
		/* ---- multi Id attack ----*/
		if(idType)
		{
			NS_LOG_INFO ("---------- Beginning attack multi id with stolen ids in the community : " << community << " --------");
			for(auto& attacker : attackers)
			{
				identity.push_back(cMap::getNodeIndex(attacker->getId(), community));
				if(attacker->getidsIdentificados().size() > 0)//stolen ids
				{
					for(uint32_t id=0; id< attacker->getidsIdentificados().size(); id++)
					{
						//memory = (DWORD) GetPointer (communities[community].Get(cMap::getNodeIndex(attacker->getId(), community)));
						identity.push_back(attacker->getIdetified(id));
					}
					timestamp.SetTimestamp(Simulator::Now());
					statistics[community]->increseNumMultiStolenIdReal();
					statistics[community]->increseNumAttackReal();
					statistics[community]->increseNumTotalReq();
					acs[community]->requestAccess(identity, timestamp);
					identity.clear();
				}
			}
		}else
		{
			NS_LOG_INFO ("---------- Beginning attack multi id with fabricated ids in the community : " << community << " --------");
			uint32_t high = 2000;
			uint32_t low = 1000;
			for(auto& attacker : attackers)
			{
				identity.push_back(cMap::getNodeIndex(attacker->getId(), community));
				if(attacker->getidsIdentificados().size() > 0)//stolen ids
				{
					for(uint32_t id=0; id< attacker->getidsIdentificados().size(); id++)
					{
						/* ---- the key for the challenge ----*/
						//memory = (DWORD) GetPointer (communities[community].Get(cMap::getNodeIndex(attacker->getId(), community)));
						uint32_t fabId = rand() % ( high - low + 1 ) + low;
						identity.push_back(fabId);
					}
					timestamp.SetTimestamp(Simulator::Now());
					statistics[community]->increseNumMultiFabricatedReal();
					statistics[community]->increseNumAttackReal();
					statistics[community]->increseNumTotalReq();
					acs[community]->requestAccess(identity, timestamp);
					identity.clear();
				}
			}
		}
	}else
	{
		/* ---- churn attack ----*/
		if(idType)
		{
			NS_LOG_INFO ("---------- Beginning churn attack with id with stolen ids in the community : " << community << " --------");
			for(auto& attacker : attackers)
			{
				identity.push_back(cMap::getNodeIndex(attacker->getId(), community));
				if(attacker->getidsIdentificados().size() > 0)//stolen ids
				{
					//memory = (DWORD) GetPointer (communities[community].Get(cMap::getNodeIndex(attacker->getId(), community)));
					uint32_t id = rand() % ( g_cSizes[community] - 0 + 1 ) + 0;
					if(id >= g_cSizes[community]) id = 0;
					identity.push_back(attacker->getIdetified(id));
					timestamp.SetTimestamp(Simulator::Now());
					statistics[community]->increseNumChurnStolenIdReal();
					statistics[community]->increseNumAttackReal();
					statistics[community]->increseNumTotalReq();
					acs[community]->requestAccess(identity, timestamp);
					identity.clear();
				}
			}
		}else
		{
			NS_LOG_INFO ("---------- Beginning churn attack with fabricated ids in the community : " << community << " --------");

			uint32_t high = 2000;
			uint32_t low = 1000;
			for(auto& attacker : attackers)
			{
				identity.push_back(cMap::getNodeIndex(attacker->getId(), community));
				/* ---- the key for the challenge ----*/
				//memory = (DWORD) GetPointer (communities[community].Get(cMap::getNodeIndex(attacker->getId(), community)));
				uint32_t fabId = rand() % ( high - low + 1 ) + low;
				identity.push_back(fabId);
				timestamp.SetTimestamp(Simulator::Now());
				statistics[community]->increseNumChurnFabricatedIdReal();
				statistics[community]->increseNumAttackReal();
				statistics[community]->increseNumTotalReq();
				acs[community]->requestAccess(identity, timestamp);
				identity.clear();
			}
		}
	}


	(attackType) ? Simulator::Schedule (Seconds (15.0), &Attacking, attackType, idType, community) : Simulator::Schedule (Seconds (5.0), &Attacking, attackType, idType, community);
}


std::string hexStr(byte *data, int len)
{
    std::stringstream ss;
    ss<<std::hex;
    for(int i(0); i<len; ++i)
        ss<<(int)data[i];
    return ss.str();
}

void printSecrectBit ()
{
	for (uint32_t i=0; i < g_tCommunities; i++)
	{
		for (uint32_t index1 = 0; index1 < g_cSizes[i]; index1++)
		{
			for (uint32_t index2 = 0; index2 < g_cSizes[i]; index2++)
			{
				if(index1 != index2)
				{
					NS_LOG_INFO ("Secrect bit [" << index1 << "][" << index2 << "]:[" << i << "] - " << apps[i]->getSecretBitFromGlobalMap(index1, index2));
				}
			}
		}
	}
}

static void SendMessage (Ptr<Socket> socket, std::string message, uint32_t index, uint32_t community)
{
	NS_LOG_INFO ("---------- sendMessage --------");
    Ptr<Packet> sendPacket = Create<Packet> ((uint8_t*)message.c_str(),message.size());

    MyTag sendTag;
    sendTag.SetSimpleValue(index);
    sendTag.SetCommunityValue(community);
    sendPacket->AddPacketTag(sendTag);
    socket->Send (sendPacket);
    stage2SentPacketCount += 1;//increment sent packet counter for stage2
    socket->Close ();
}

void ReceiveMessage (Ptr<Socket> socket)
{
	NS_LOG_INFO ("---------- receiveMessage --------");
    Ptr<Packet> recPacket = socket->Recv();
    stage2RecvPacketCount += 1;//increment recv packet counter for stage2

    Ptr<Node> recvnode = socket->GetNode();

    uint8_t *buffer = new uint8_t[recPacket->GetSize()];
    recPacket->CopyData(buffer,recPacket->GetSize());

    std::string recMessage = std::string((char*)buffer);
    recMessage = recMessage.substr (0,messageLen-1);

    MyTag recTag;
    recPacket->PeekPacketTag(recTag);
    uint32_t srcNodeIndex = recTag.GetSimpleValue();
	uint32_t community = recTag.GetCommunityValue();

	uint32_t recNodeIndex = apps[community]->getNodeFromMap(recvnode);

    SecByteBlock key(SHA256::DIGESTSIZE);
    SHA256().CalculateDigest(key, apps[community]->getSecretKeyFromGlobalMap(srcNodeIndex,recNodeIndex), apps[community]->getSecretKeyFromGlobalMap(srcNodeIndex,recNodeIndex).size());

    //Decryption using the Shared secret key
    CFB_Mode<AES>::Decryption cfbDecryption(key, aesKeyLength, iv);
    cfbDecryption.ProcessData((byte*)recMessage.c_str(), (byte*)recMessage.c_str(), messageLen);

    int value = atoi(recMessage.c_str());

    //put in node's map
    apps[community]->putSecretBitInGlobalMap(srcNodeIndex,recNodeIndex,value);
    apps[community]->putSecretBitInGlobalMap(recNodeIndex,srcNodeIndex,value);

    randomBitCounter--;
    if(randomBitCounter == 0)
    {
		NS_LOG_INFO ("time: "+std::to_string(Simulator::Now().GetSeconds()));
		stage2EndTime = Simulator::Now();
		Simulator::ScheduleNow (&DisplayMessage,source);
    }
}

void DisplayMeasurements()
{
	for (uint32_t i=0; i < g_tCommunities; i++)
	{
		acs[i]->printValues();
		acs[i]->printValuesCandidates();
	}

	NS_LOG_INFO ("Shared Message after " << MessageLength << " rounds is : " << sharedMessage.str());
	NS_LOG_INFO ("Sent Packet Count Stage 1: " << stage1SentPacketCount);
	NS_LOG_INFO ("Sent Packet Count Stage 2: " << stage2SentPacketCount);
	NS_LOG_INFO ("Sent Packet Count new node: " << SentPacketNewNode);
	NS_LOG_INFO ("Sent Packet Count attack: " << SentPacketAttack);
	NS_LOG_INFO ("Recv Packet Count Stage 1: " << stage1RecvPacketCount);
	NS_LOG_INFO ("Recv Packet Count Stage 2: " << stage2RecvPacketCount);
	NS_LOG_INFO ("Recv Packet Count new node: " << RecvPacketNewNode);
	NS_LOG_INFO ("Recv Packet Count attack: " << RecvPacketAttack);

    stage1Latency = (stage1EndTime.GetSeconds() - stage1StartTime.GetSeconds());
    NS_LOG_INFO ("Stage 1 latency: " << stage1Latency);

    stage2Latency = (stage2EndTime.GetSeconds() - stage2StartTime.GetSeconds());
    NS_LOG_INFO ("Stage 2 latency: " << stage2Latency);

    totalLatency = (stage1Latency + stage2Latency);
   // std::cout<<"goodPut: "<<goodPut<<"\n";

	totalTimeEnd = Simulator::Now();
	totalRunningTime = totalTimeEnd.GetSeconds() - totalTimeStart.GetSeconds();
	NS_LOG_INFO ("Total time taken : " << totalRunningTime << " seconds");

	//output to csv
//	for (uint32_t i=0; i < g_tCommunities; i++)
//	{
//		if(option == 1)
//			apps[i]->writeOutputToFile((char*)"g_tNodesInCommvsMeasurements.csv",option,g_tNodesInComm,MessageLength,totalLatency,totalRunningTime);
//		else
//			apps[i]->writeOutputToFile((char*)"MsgLengthvsMeasurements.csv",option,g_tNodesInComm,MessageLength,totalLatency,totalRunningTime);
//	}
}

static void SimulatorLoop(Ptr<Socket> socket)
{
	NS_LOG_INFO ("---------- simulatorLoop --------");
	simulatorCounter = (((g_tNodesInComm/g_tCommunities) * (g_tNodesInComm/g_tCommunities)) - (g_tNodesInComm/g_tCommunities)) * g_tCommunities;
    // Generate a random IV
    rnd.GenerateBlock(iv, AES::BLOCKSIZE);

    for (uint32_t i=0; i < g_tCommunities; i++)
	{
		//sharing the random bit using dh secret key
		for (uint32_t index1 = 0; index1 < g_cSizes[i]; index1++)
		{
			for (uint32_t index2 = 0; index2 < g_cSizes[i]; index2++)
			{
				if(index1 != index2)
				{
					int randomBit = randomBitGeneratorWithProb(0.5);
					NS_LOG_INFO ("Random bit : "<<randomBit<<" "<<index1<<" "<<index2);

					//put random bit in both the maps - src and dest maps
					apps[i]->putSecretBitInGlobalMap(index1,index2,randomBit);
					apps[i]->putSecretBitInGlobalMap(index2,index1,randomBit);

					// Calculate a SHA-256 hash over the Diffie-Hellman session key
					SecByteBlock key(SHA256::DIGESTSIZE);
					SHA256().CalculateDigest(key, apps[i]->getSecretKeyFromGlobalMap(index1,index2), apps[i]->getSecretKeyFromGlobalMap(index1,index2).size());

					std::ostringstream ss;
					ss << randomBit;
					std::string message = ss.str();
					messageLen = (int)strlen(message.c_str()) + 1;

					// Encrypt
					CFB_Mode<AES>::Encryption cfbEncryption(key, aesKeyLength, iv);
					cfbEncryption.ProcessData((byte*)message.c_str(), (byte*)message.c_str(), messageLen);

					//Send the encrypted message
					Ptr<Socket> recvNodeSink = Socket::CreateSocket (communities[i].Get (index2), tid);
					Inet6SocketAddress localSocket = Inet6SocketAddress (Ipv6Address::GetAny (), 9801);
					recvNodeSink->Bind (localSocket);
					recvNodeSink->SetRecvCallback (MakeCallback (&ReceiveMessage));

					Inet6SocketAddress remoteSocket = Inet6SocketAddress (ipv6Containers[i].GetAddress (index2, 0), 9801);
					Ptr<Socket> sourceNodeSocket = Socket::CreateSocket (communities[i].Get (index1), tid);
					sourceNodeSocket->Connect (remoteSocket);
					//waitTime += 20.0;
					Simulator::ScheduleNow(&SendMessage, sourceNodeSocket,message,index1,i);
				}
			}
		}
	}
//    NS_LOG_INFO ("--------- Secrect bit simultor loop ");
//    printSecrectBit();
}

static void SendPublicKey (Ptr<Socket> socket, SecByteBlock pub, uint32_t src, uint32_t community)
{
	NS_LOG_INFO ("---------- sendPublicKey --------");
    Ptr<Packet> sendPacket = Create<Packet> ((uint8_t*)pub.BytePtr(),(uint8_t) pub.SizeInBytes());
    NS_LOG_INFO ("Debug: Inside dcnet send public key - source: "<< src << " community: " << community);
    MyTag sendTag;
    sendTag.SetSimpleValue(src);
    sendTag.SetCommunityValue(community);
    sendPacket->AddPacketTag(sendTag);

    socket->Send(sendPacket);
    stage1SentPacketCount += 1;//increment sent packet counter for stage1
    std::string sendData = hexStr(pub.BytePtr(),pub.SizeInBytes());

    socket->Close();
}

void ReceivePublicKey (Ptr<Socket> socket)
{
	NS_LOG_INFO ("---------- receivePublicKey --------");
	NS_LOG_INFO ("Debug : Inside dcnet receive public key");
    Ptr<Node> recvnode = socket->GetNode();

    Ptr<Packet> recPacket = socket->Recv();
    stage1RecvPacketCount +=1; //increment received packet count for stage 1

    uint8_t *buffer = new uint8_t[recPacket->GetSize()];
    recPacket->CopyData(buffer,recPacket->GetSize());

    SecByteBlock pubKey((byte *)buffer,recPacket->GetSize());

    MyTag recTag;
    recPacket->PeekPacketTag(recTag);
	uint32_t srcNodeIndex = recTag.GetSimpleValue();
	uint32_t community = recTag.GetCommunityValue();

    std::string recvData = hexStr(pubKey.BytePtr(),pubKey.SizeInBytes());

    uint32_t recNodeIndex = apps[community]->getNodeFromMap(recvnode);

    DH dh;
    dh.AccessGroupParameters().Initialize(p, q, g);
    SecByteBlock sharedKey(apps[community]->getDhAgreedLength());

    dh.Agree(sharedKey, apps[community]->getPrivateKeyFromMap(recNodeIndex),pubKey);

    apps[community]->putSecretKeyInGlobalMap(recNodeIndex,srcNodeIndex,sharedKey);

    simulatorCounter--;
    NS_LOG_INFO ("Public key counter:" << simulatorCounter);
    if(simulatorCounter == 0)
    {
    	NS_LOG_INFO ("Debug : calling simulator loop");
    	stage1EndTime = Simulator::Now();
    	stage2StartTime = Simulator::Now();
        Simulator::ScheduleNow (&SimulatorLoop, socket);
    }
}

void generateKeys(uint32_t& index, uint32_t& community)
{
    try
    {
		DH dh;
		AutoSeededRandomPool rnd;

		dh.AccessGroupParameters().Initialize(p, q, g);

		if(!dh.GetGroupParameters().ValidateGroup(rnd, 3))
			throw runtime_error("Failed to validate prime and generator");


		p = dh.GetGroupParameters().GetModulus();
		q = dh.GetGroupParameters().GetSubgroupOrder();
		g = dh.GetGroupParameters().GetGenerator();

		Integer v = ModularExponentiation(g, q, p);
		if(v != Integer::One())
			throw runtime_error("Failed to verify order of the subgroup");

		//////////////////////////////////////////////////////////////

		SecByteBlock priv(dh.PrivateKeyLength());
		SecByteBlock pub(dh.PublicKeyLength());
		dh.GenerateKeyPair(rnd, priv, pub);

		//////////////////////////////////////////////////////////////

		apps[community]->putPrivateKeyInMap(index, priv);
		apps[community]->putPublicKeyInMap(index, pub);
		apps[community]->setDhAgreedLength(dh.AgreedValueLength());

    }
    catch(const CryptoPP::Exception& e)
    {
        std::cerr << "Crypto error : "<< e.what() << std::endl;
    }

    catch(const std::exception& e)
    {
        std::cerr << "Standard error : "<<e.what() << std::endl;
    }
}

//sending and receiving announcements
static void SendAnnouncement (Ptr<Socket> socket, uint32_t result, uint32_t index, uint32_t community)
{
	NS_LOG_INFO ("---------- sendAnnouncement --------");
	std::ostringstream ss;
	ss << result;
	std::string message = ss.str();
	Ptr<Packet> sendPacket = Create<Packet> ((uint8_t*)message.c_str(),message.size());
	stage1SentPacketCount += 1;

	MyTag sendTag;
	sendTag.SetSimpleValue(index);
	sendTag.SetCommunityValue(community);
	sendPacket->AddPacketTag(sendTag);

	socket->Send(sendPacket);
	socket->Close();
	NS_LOG_INFO ("Announcement sent from: " << index << " to :" << socket->GetNode()->GetId());
}

void ReceiveAnnouncement (Ptr<Socket> socket)
{
	NS_LOG_INFO ("---------- receiveAnnoucement --------");

	stage1RecvPacketCount +=1;
	Ptr<Packet> recPacket = socket->Recv();
	Ptr<Node> recvnode = socket->GetNode();


	uint8_t *buffer = new uint8_t[recPacket->GetSize()];
	recPacket->CopyData(buffer,recPacket->GetSize());

	std::string recMessage = std::string((char*)buffer);
	recMessage = recMessage.substr (0,messageLen-1);

	MyTag recTag;
	recPacket->PeekPacketTag(recTag);
	uint32_t srcNodeIndex = recTag.GetSimpleValue();
	uint32_t community = recTag.GetCommunityValue();
	uint32_t recNodeIndex = apps[community]->getNodeFromMap(recvnode);
	NS_LOG_INFO ("Announcement received from: " << srcNodeIndex << " to: " << recNodeIndex);

	apps[community]->putAnnouncementInReceivedMap(recNodeIndex, srcNodeIndex, atoi(recMessage.c_str()));

	simulatorCounter-=1;
	if(simulatorCounter == 0)
	{
		int x=0;
		//xoring outputs
		for (uint32_t i=0; i < g_tCommunities; i++)
		{
			for(uint32_t index=0; index<g_cSizes[i]; index++)
			{
				x ^= apps[i]->getAnnouncement(index);
			}
		}

		sharedMessage<<x;
		simulatorCounter = (((g_tNodesInComm/g_tCommunities) * (g_tNodesInComm/g_tCommunities)) - (g_tNodesInComm/g_tCommunities)) * g_tCommunities; //(g_tNodesInComm * g_tNodesInComm) - g_tNodesInComm;
		randomBitCounter = (((g_tNodesInComm/g_tCommunities) * ((g_tNodesInComm/g_tCommunities)-1)* g_tCommunities)/2); //(g_tNodesInComm * (g_tNodesInComm-1)/2);
		/* Increase simulation rounds */
		rounds++;
		Simulator::ScheduleNow (&DCNET, source);
	}
}

void callTrustAttacker()
{
	acs[0]->writeTrustAttackers();

	Simulator::Schedule (Seconds (Simulator::Now().GetSeconds() + 1), &callTrustAttacker);
}

void DisplayMessage(Ptr<Socket> socket)
{
	NS_LOG_INFO ("---------- displayMessage --------");
    int bit = 0;//Message.at(rounds)-48 ;

    NS_LOG_INFO ("Current Round : " << rounds << " and current bit : " << bit);
    for (uint32_t i=0; i < g_tCommunities; i++)
	{
		for (uint32_t index = 0; index < g_cSizes[i]; index++)
		{
			uint32_t result = 0;
			map<uint32_t,uint32_t> NodeSecretBitMap = apps[i]->getSecretBitSubMap(index);

			for (map<uint32_t,uint32_t>::iterator it=NodeSecretBitMap.begin(); it!=NodeSecretBitMap.end(); ++it)
			{
				NS_LOG_INFO ("Adj bits of node " << index << ": " << (int)it->second);
				//Exor the adjacent node bits stored in the map
				result ^= (int)it->second;
			}
			if(sender == index)	//exor result with message
			{
				result ^= bit;
			}

			NS_LOG_INFO ("Result for Node " << index << " in the community " << i << " is: " << result << " in round " << rounds);
			apps[i]->putAnnouncementInGlobalMap(index, result);

		}
	}

    //----------------------------------------------------------------------------------
	// Init the trust values
	//----------------------------------------------------------------------------------
	Simulator::Schedule (Seconds (Simulator::Now().GetSeconds()), &TasksForDirectTrust);
	Simulator::Schedule (Seconds (Simulator::Now().GetSeconds() + 10), &UpdateRecommendationMatrix);

	//----------------------------------------------------------------------------------
	// Config the candidates and start the trust evaluation
	//----------------------------------------------------------------------------------
	Simulator::Schedule (Seconds (Simulator::Now().GetSeconds() + 20), &StartCandidates);
	Simulator::Schedule (Seconds (Simulator::Now().GetSeconds() + 21), &TaskDirectTrustForCandidates);
	Simulator::Schedule (Seconds (Simulator::Now().GetSeconds() + 22), &UpdateRecommendationMatrix);
	Simulator::Schedule (Seconds (Simulator::Now().GetSeconds() + 23), &MakeACallbackSocialTrustCandidates);

	Simulator::Schedule (Seconds (Simulator::Now().GetSeconds() + 25), &callTrustAttacker);

	//----------------------------------------------------------------------------------
	// Start the attacks in the communities
	//----------------------------------------------------------------------------------
	for (uint32_t i=0; i < g_tCommunities; i++)
	{
		switch(g_tActualAttack)
		{
		case 0: Simulator::Schedule (Seconds (Simulator::Now().GetSeconds() + 25), &Attacking, true, true, i); //multi id. with stolen id
		break;
		case 1: Simulator::Schedule (Seconds (Simulator::Now().GetSeconds() + 27), &Attacking, true, false, i); //multi id. with fabricated id
		break;
		case 2: Simulator::Schedule (Seconds (Simulator::Now().GetSeconds() + 26), &Attacking, false, true, i); //churn with stolen id
		break;
		case 3: Simulator::Schedule (Seconds (Simulator::Now().GetSeconds() + 28), &Attacking, false, false, i); //churn with fabricated id
		break;
		}
	}

	//----------------------------------------------------------------------------------
	// Node candidate request access to the network
	//----------------------------------------------------------------------------------
	Simulator::Schedule (Seconds (Simulator::Now().GetSeconds() + 29), &AllCandidatesRequestAccess);

	//----------------------------------------------------------------------------------
	// Callback Detected rate
	//----------------------------------------------------------------------------------
	Simulator::Schedule (Seconds (Simulator::Now().GetSeconds() + 30), &MakeCallbackDetectedRate);
	//----------------------------------------------------------------------------------
	// Callback accuracy of detection
	//----------------------------------------------------------------------------------
	Simulator::Schedule (Seconds (Simulator::Now().GetSeconds() + 80.5), &MakeCallbackAccuracyDetection);
	//----------------------------------------------------------------------------------
	// Callback false-negative rate
	//----------------------------------------------------------------------------------
	Simulator::Schedule (Seconds (Simulator::Now().GetSeconds() + 90), &MakeCallbackfalsePositiveRate);
	//----------------------------------------------------------------------------------
	// Callback false-negative rate
	//----------------------------------------------------------------------------------
	Simulator::Schedule (Seconds (Simulator::Now().GetSeconds() + 90.5), &MakeCallbackFalsePositiveRate);
}

static void GenerateTraffic (Ptr<Socket> socket, uint32_t pktSize,
                             uint32_t pktCount, Time pktInterval )
{
	NS_LOG_INFO ("---------- generateTraffic --------");
  if (pktCount > 0)
    {
      socket->Send (Create<Packet> (pktSize));
      Simulator::Schedule (pktInterval, &GenerateTraffic,
                           socket, pktSize,pktCount, pktInterval);
    }
  else
    {
      socket->Close ();
    }
}

void ShowSybil()
{
	for (uint32_t i=0; i < g_tCommunities; i++)
	{
		for(auto s : sybils[i])
		{
			s->showResults();
		}
	}
}

void DCNET(Ptr<Socket> socket)
{
	NS_LOG_INFO ("---------- DCNET --------");
	/*Symmetric key generation */
	for (uint32_t i=0; i < g_tCommunities; i++)
	{
		if(apps[i]->getCommunity() == i)
		{
			for(uint32_t nodeind=0; nodeind < g_cSizes[i]; nodeind++)
			{
				generateKeys(nodeind, i);
			}
		}
	}

	/*send the public key to everyone*/
	for (uint32_t i=0; i < g_tCommunities; i++)
	{
		for (uint32_t index1 = 0; index1 < g_cSizes[i]; index1++)
		{
			for (uint32_t index2 = 0; index2 < g_cSizes[i]; index2++)
			{
				if(index1 != index2)
				{
					Ptr<Socket> recvNodeSink = Socket::CreateSocket (communities[i].Get (index2), tid);
					Inet6SocketAddress localSocket = Inet6SocketAddress (Ipv6Address::GetAny (),9803);
					recvNodeSink->Bind (localSocket);
					recvNodeSink->SetRecvCallback (MakeCallback (&ReceivePublicKey));

					Inet6SocketAddress remoteSocket = Inet6SocketAddress (ipv6Containers[i].GetAddress (index2, 0), 9803);
					Ptr<Socket> sourceNodeSocket = Socket::CreateSocket (communities[i].Get (index1), tid);
					sourceNodeSocket->Connect (remoteSocket);
					Simulator::Schedule (Seconds(Simulator::Now().GetSeconds() + index2),&SendPublicKey, sourceNodeSocket,apps[i]->getPublicKeyFromMap(index1),index1, i);
				}
			}

		}
	}
}

void StartKeyExchange()
{
	NS_LOG_INFO ("Starting key exchange");
	for (uint32_t i=0; i < g_tCommunities; i++)
	{
		for (uint32_t index1 = 0; index1 < g_cSizes[i]; index1++)
		{
			for (uint32_t index2 = 0; index2 < g_cSizes[i]; index2++)
			{
				if(index1 != index2)
				{
					Ptr<Socket> recvNodeSink = Socket::CreateSocket (communities[i].Get (index2), tid);
					Inet6SocketAddress localSocket = Inet6SocketAddress (Ipv6Address::GetAny (),9802);
					recvNodeSink->Bind (localSocket);
					recvNodeSink->SetRecvCallback (MakeCallback (&ReceiveAnnouncement));

					Inet6SocketAddress remoteSocket = Inet6SocketAddress (ipv6Containers[i].GetAddress (index2, 1), 9802);
					Ptr<Socket> sourceNodeSocket = Socket::CreateSocket (communities[i].Get (index1), tid);
					sourceNodeSocket->Connect (remoteSocket);

					Simulator::Schedule (Seconds(Simulator::Now().GetSeconds() + index2), &SendAnnouncement, sourceNodeSocket,apps[i]->getAnnouncement(index1), index1, i);
				}
			}
		}
	}
}

#endif
