/*
 * file.h
 *
 *  Created on: Jun 29, 2018
 *      Author: gustavo
 */

#ifndef SCRATCH_SCOT_FILE_H_
#define SCRATCH_SCOT_FILE_H_


class File : public Object
{

private:

	/**
	 * The owner of this FileCopy.
	 */
	uint32_t owner;

	/**
	 * The validity of this FileCopy.
	 */
	bool valid;

public:

	File();

	File(uint32_t owner, uint32_t valid);

	void config(uint32_t owner, uint32_t valid);

	uint32_t getOwner();

	bool getValid();
};

File::File(){} // @suppress("Class members should be properly initialized")

File::File(uint32_t owner, uint32_t valid)
{
	this->owner = owner;
	this->valid = valid;
}

void File::config(uint32_t owner, uint32_t valid)
{
	this->owner = owner;
	this->valid = valid;
}

uint32_t File::getOwner()
{
	return this->owner;
}

bool File::getValid()
{
	return this->valid;
}

#endif /* SCRATCH_SCOT_FILE_H_ */
