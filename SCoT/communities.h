/*
 * communities.h
 *
 *  Created on: Jun 10, 2018
 *      Author: gustavo
 */

#ifndef SCRATCH_SCOT_COMMUNITIES_H_
#define SCRATCH_SCOT_COMMUNITIES_H_

#include <iostream>
#include <vector>

class Community
{
public:

	enum Type{HOME, GYM, SCHOOL, OFFICE};

	Community();
	Community(Type type, int id);
	~Community();

private:

	int id;

	Type type;

};

Community::Community(){} // @suppress("Class members should be properly initialized")

Community::~Community()
{}

Community::Community(Type type, int id)
{
	this->id = id;
	this->type = type;
}



#endif /* SCRATCH_SCOT_COMMUNITIES_H_ */
