/*
 * experienceManager.h
 *
 *  Created on: Jul 19, 2018
 *      Author: gustavo
 */

#ifndef SCRATCH_SCOT_EXPERIENCEMANAGER_H_
#define SCRATCH_SCOT_EXPERIENCEMANAGER_H_


class ExperienceManager : public Object
{
private:

	map<uint32_t, std::vector<double>> historical;

public:

	ExperienceManager();

	std::vector<double> getPastExperience(uint32_t trustee);
};

ExperienceManager::ExperienceManager()
{
	std::vector<double> tmp;

	/* -- Positive iterations R ---*/
	tmp.push_back(5.);
	/* -- Negative iterations S ---*/
	tmp.push_back(0.);
	historical.insert(std::pair<uint32_t, std::vector<double>>(5, tmp));

	tmp.clear();
	/* -- Positive iterations R ---*/
	tmp.push_back(7.);
	/* -- Negative iterations S ---*/
	tmp.push_back(5.);
	historical.insert(std::pair<uint32_t, std::vector<double>>(8, tmp));

	tmp.clear();
	/* -- Positive iterations R ---*/
	tmp.push_back(5.);
	/* -- Negative iterations S ---*/
	tmp.push_back(20.);
	historical.insert(std::pair<uint32_t, std::vector<double>>(10, tmp));
}

std::vector<double> ExperienceManager::getPastExperience(uint32_t trustee)
{
	map<uint32_t, std::vector<double>>::iterator it;
	it = historical.find(trustee);
	if(it != historical.end())
	{
		return it->second;
	}else
	{
		std::vector<double> tmp;
		tmp.push_back(0.);
		tmp.push_back(0.);
		return tmp;
	}
}


#endif /* SCRATCH_SCOT_EXPERIENCEMANAGER_H_ */
