#include "ns3/core-module.h"
#include "ns3/network-module.h"
#include "ns3/mobility-module.h"
#include "ns3/config-store-module.h"
#include "ns3/wifi-module.h"
#include "ns3/internet-module.h"
#include "ns3/olsr-helper.h"
#include "ns3/ipv4-static-routing-helper.h"
#include "ns3/ipv4-list-routing-helper.h"
#include "ns3/aodv-helper.h"
#include "MyTag.h"
#include "ns3/nstime.h"
#include <cstdlib>
#include <iostream>
#include <fstream>
#include <vector>
#include <string>
#include "crypto++/aes.h" 
#include "crypto++/sha.h"
#include "crypto++/modes.h"
#include "crypto++/integer.h"
#include "crypto++/osrng.h"
#include "crypto++/nbtheory.h"
#include <stdexcept>
#include "crypto++/dh.h"
#include "crypto++/secblock.h"
#include <crypto++/hex.h>
#include <crypto++/filters.h>
#include <map>
#include <sstream>
#include <iomanip>

using CryptoPP::AutoSeededRandomPool;
using CryptoPP::ModularExponentiation;
using std::runtime_error;
using CryptoPP::DH;
using CryptoPP::SecByteBlock;
using CryptoPP::StringSink;
using CryptoPP::HexEncoder;
using std::map;
using std::pair;
using namespace ns3;
using namespace CryptoPP;

Integer p("0xB10B8F96A080E01DDE92DE5EAE5D54EC52C99FBCFB06A3C6"
		"9A6A9DCA52D23B616073E28675A23D189838EF1E2EE652C0"
		"13ECB4AEA906112324975C3CD49B83BFACCBDD7D90C4BD70"
		"98488E9C219A73724EFFD6FAE5644738FAA31A4FF55BCCC0"
		"A151AF5F0DC8B4BD45BF37DF365C1A65E68CFDA76D4DA708"
		"DF1FB2BC2E4A4371");

Integer g("0xA4D1CBD5C3FD34126765A442EFB99905F8104DD258AC507F"
		"D6406CFF14266D31266FEA1E5C41564B777E690F5504F213"
		"160217B4B01B886A5E91547F9E2749F4D7FBD7D3B9A92EE1"
		"909D0D2263F80A76A6A24C087A091F531DBF0A0169B6A28A"
		"D662A4D18E73AFA32D779D5918D08BC8858F4DCEF97C2A24"
		"855E6EEB22B3B2E5");

Integer q("0xF518AA8781A8DF278ABA4E7D64B7CB9D49462353");

TypeId tid;
std::vector<Ipv4InterfaceContainer> ipv4Containers;
//Ipv6InterfaceContainer ipv6ContainerCandidates;
std::vector<Ipv6InterfaceContainer> ipv6Containers;
std::vector<NodeContainer> communities; //list of communities
uint32_t g_tAttackers; //global total of attackers
uint32_t g_tNodes; //global total of nodes
uint32_t g_tNodesInComm; // global total nodes in the communities
uint32_t g_tCommunities; //total of communities
uint32_t g_tActualAttack; //attack type running in the simulation
Ptr<Socket> source;
int option;
uint32_t cSize;
std::vector<uint32_t> g_cSizes;

std::string experiment ("electron-ac-test");
std::string strategy ("electron-social-trust");
std::string input;
std::string runID;

//int nodeSpeed = 20; //in m/s
//int nodePause = 0; //in s
//double txp = 7.5;

int rounds = 0;
int MessageLength = 0;
double waitTime = 0;
std::stringstream sharedMessage;
uint32_t sender = 0;
//std::string Message = "0110000101100010001100110011010100111001011000010110000100110111001101100110000100110110001101110011011100110011011001010110010000110111011000010011100100110011011000100011001000110001001101000110010001100010001100000110001100110010001101010110010000110000001100010011011000110000001110000011000100110111011000100011100001100001001110000011100100110011011000110011000000110000001100010110001100110111001101100011000101100101001100010011100100111000011000010011001100110110001110010011010000110101001100000011100101100101011000100110010100111000000011010000101000110111011000010011010100110011001100010011001101100101001100000011001100110100001110010110010000111001001101010011000000111000001100110110010100110101001101000011000100110010011000110011100101100110011000110011100000110001001101010110001001100110011001000011011000110001011001100011100100110101011001000110010001100101011000110110010100110100001100110011001100110111001101100011010100110101001100000110011001100100011000110011011000110010001101000110010100111001001100100110011001100110001100110011100001100001001101000011000100110101001101110011100000110011011000100011100100110111001100100011011000110001001100100011000000001101000010100011010001100101001100000011010101100100001101100011010100110111001100110011000101100010011000100110000100110001011000110110001101100110011001100011000001100101001110000011010001100011001110000110001101100100001100100011000000111001001101110110001000110111001101010110011001100101011000110110000100110001001100000011001000111001001100100011011000110001011000010110010100110001001110010110000100110011001110000011100101100001001100100110010100110001001101010110010000110010001110010011001100111001001100110011000100110100011000100011000100111000001101000110000101100101011001100011011100110000001101110110001000111000001100100000110100001010011001010110001000111001001101000011010000110001001100100011000000110110001101010011000100111000001100010110010000110010001100110110010100110000001101000110001001100110001100000011011000110101011001100011010001100001011000110011010000110001001100110110011001101000";
std::string Message = "101";
std::string phyMode ("OfdmRate54Mbps");
double distance = 1;  // m
uint32_t packetSize = 1024; // bytes
uint32_t numPackets = 60;
uint32_t numFiles = 1000; //Number of files in the simulation
uint32_t sinkNode = 0;
uint32_t sourceNode = 2;
double interval = 1.0; // seconds
double keyExchangeInterval = 5.0; // seconds
bool verbose = false;
bool tracing = true;
int messageLen=0;	
bool candidatesOn = false;
uint32_t nAttackers = 2;

int aesKeyLength = SHA256::DIGESTSIZE;
AutoSeededRandomPool rnd;
byte iv[AES::BLOCKSIZE];	
SecByteBlock key(SHA256::DIGESTSIZE);
static std::string msgs[20];

//measurement variables

int stage1SentPacketCount = 0;
int stage2SentPacketCount = 0;
int stage1RecvPacketCount = 0;
int stage2RecvPacketCount = 0;
int stage3RecvPacketNewNode = 0;
int SentPacketNewNode = 0;
int RecvPacketNewNode = 0;
int SentPacketAttack = 0;
int RecvPacketAttack = 0;
double stage1Latency;
double stage2Latency;
Time totalTimeStart, totalTimeEnd, stage1StartTime, stage2StartTime, stage1EndTime, stage2EndTime;
double totalRunningTime;
double totalLatency;

int simulatorCounter = 0;
int randomBitCounter = 0;

std::vector<uint32_t> lastSIoT;

/* ------- Statics ------- */
/**
* Sybil feedbacks committed.
*/
int NUM_FBACK_SYBL = 0;

/**
* Transactions w/valid file exchange and 'good' receiver
*/
int NUM_GOOD_SUCC = 0;

/**
* Transactions (completed) resulting in exchange of invalid file.
*/
int NUM_INVAL_TRANS = 0;

/**
* Incomplete transactions due to complications on receiving end.
*/
int NUM_RECV_BLK_TR = 0;

/**
* Incomplete transactions due to complications on source end.
*/
int NUM_SEND_BLK_TR = 0;

/**
* Honest feedbacks committed.
*/
int NUM_FBACK_TRUE = 0;

/**
* Dishonest feedbacks committed.
*/
int NUM_FBACK_LIES = 0;

/**
 * Transactions w/invalid file exchange and 'good' receiver
 */
int NUM_GOOD_FAIL = 0;


class ApplicationUtil : public Object
{	
private:

	uint32_t community;
	static bool instanceFlag;
	int dhAgreedLength;
	static ApplicationUtil *appUtil;

	map<uint32_t,SecByteBlock> publicKeyMap;
	map<uint32_t,SecByteBlock> privateKeyMap;
	map<uint32_t,SecByteBlock> dhSecretKeyMapSub;
	map<uint32_t,map<uint32_t, SecByteBlock>> dhSecretKeyMapGlobal;
	map<uint32_t, uint32_t> dhSecretBitMapSub;
	map<uint32_t,map<uint32_t, uint32_t> > dhSecretBitMapGlobal;
	map<Ptr<Node>, uint32_t> nodeMap;
	map<uint32_t, uint32_t> announcement;
	map<uint32_t, uint32_t> receivedAnnouncementSubMap;
	map<uint32_t, map<uint32_t, uint32_t> > receivedAnnouncement;

public:

	ApplicationUtil();
	ApplicationUtil(uint32_t community);

	void writeOutputToFile(std::string fileName, uint32_t option, uint32_t numNodes,uint32_t length, double latency, double totalTime );
	//static int publicKeyPairCount;
	void setDhAgreedLength(uint32_t len);
	void putPublicKeyInMap(uint32_t nodeId, SecByteBlock key);
	void putSecretBitInGlobalMap(uint32_t nodeId, uint32_t destNodeId, uint32_t value);
	void eraseSecretBitMapGlobal();
	void putPrivateKeyInMap(uint32_t nodeId, SecByteBlock key);
	void putNodeInMap(Ptr<Node> node,uint32_t index);
	void putSecretKeyInGlobalMap(uint32_t nodeId, uint32_t destNodeId, SecByteBlock key);
	void putAnnouncementInGlobalMap(uint32_t nodeId,uint32_t value);
	void putAnnouncementInReceivedMap(uint32_t nodeId, uint32_t senderNode, uint32_t value);
	void showMap();

	uint32_t getDhAgreedLength();
	uint32_t getSecretBitFromGlobalMap(uint32_t nodeId,uint32_t destNodeId);
	uint32_t getNodeFromMap(Ptr<Node> node);
	uint32_t getAnnouncement(uint32_t nodeId);
	uint32_t getReceivedAnnouncement(uint32_t nodeId, uint32_t senderNodeId);

	SecByteBlock getPublicKeyFromMap(uint32_t nodeId);
	SecByteBlock getPrivateKeyFromMap(uint32_t nodeId);
	SecByteBlock getSecretKeyFromGlobalMap(uint32_t nodeId,uint32_t destNodeId);


	map<uint32_t,uint32_t> getSecretBitSubMap(uint32_t nodeId);
	map<uint32_t, uint32_t> getAnnouncementSubMap(uint32_t nodeId);

	NodeContainer installNewNodes(uint32_t nNodes, uint32_t community);
	void generateKeys(uint32_t& index);

	uint32_t getCommunity();

	int getSIoTScenario();

	static ApplicationUtil* getInstance();	
	
	~ApplicationUtil();
};

bool ApplicationUtil::instanceFlag = false;
ApplicationUtil* ApplicationUtil::appUtil = NULL;

ApplicationUtil::ApplicationUtil()
{
	this->dhAgreedLength = 0;
}

ApplicationUtil::ApplicationUtil(uint32_t community)
{
	this->community = community;
	this->dhAgreedLength = 0;
}

ApplicationUtil::~ApplicationUtil()
{
	instanceFlag = false;
}

uint32_t ApplicationUtil::getDhAgreedLength()
{
	return dhAgreedLength;
}

void ApplicationUtil::setDhAgreedLength(uint32_t len)
{
	dhAgreedLength = len;
}


ApplicationUtil* ApplicationUtil::getInstance()
{
	if(!instanceFlag)
	{
		//publicKeyPairCount = 0;
		appUtil = new ApplicationUtil();
		instanceFlag = true;
	}
	return appUtil;
    
}

uint32_t ApplicationUtil::getCommunity()
{
	return this->community;
}

void ApplicationUtil::putNodeInMap(Ptr<Node> node,uint32_t index)
{
	nodeMap.insert(pair<Ptr<Node>,uint32_t>(node,index));
}

uint32_t ApplicationUtil::getNodeFromMap(Ptr<Node> node)
{
	map<Ptr<Node>,uint32_t>::iterator p;
	p = nodeMap.find(node);
	if(p != nodeMap.end())
		return p->second;
	else 
		return -1;	
}
SecByteBlock ApplicationUtil::getPublicKeyFromMap(uint32_t nodeId)
{
	map<uint32_t,SecByteBlock>::iterator p;
	p = publicKeyMap.find(nodeId);
	if(p != publicKeyMap.end())
		return p->second;
	else 
		return SecByteBlock(0);
}

void ApplicationUtil::putPublicKeyInMap(uint32_t nodeId, SecByteBlock key)
{
	publicKeyMap.insert(pair<uint32_t,SecByteBlock>(nodeId,key));
}

SecByteBlock ApplicationUtil::getPrivateKeyFromMap(uint32_t nodeId)
{
	map<uint32_t,SecByteBlock>::iterator p;
	p = privateKeyMap.find(nodeId);
	if(p != privateKeyMap.end())
		return p->second;
	else 
		return SecByteBlock(0);
}

void ApplicationUtil::putPrivateKeyInMap(uint32_t nodeId, SecByteBlock key)
{
	privateKeyMap.insert(pair<uint32_t,SecByteBlock>(nodeId,key));
}	

SecByteBlock ApplicationUtil::getSecretKeyFromGlobalMap(uint32_t nodeId, uint32_t destNodeId)
{
	map<uint32_t,map<uint32_t,SecByteBlock> >::iterator p;

	p = dhSecretKeyMapGlobal.find(nodeId);

	if(p != dhSecretKeyMapGlobal.end())
	{
		map<uint32_t,SecByteBlock>::iterator p1;
		p1 = p->second.find(destNodeId);
		if(p1 != dhSecretKeyMapSub.end())
			return p1->second;
		else 
		{
			return SecByteBlock(0);
		}
	}
	else 
	{
		return SecByteBlock(0);
	}
}

void ApplicationUtil::putSecretKeyInGlobalMap(uint32_t nodeId, uint32_t destNodeId, SecByteBlock key)
{

	map<uint32_t,map<uint32_t,SecByteBlock> >::iterator p;
	p = dhSecretKeyMapGlobal.find(nodeId);
	if(p != dhSecretKeyMapGlobal.end())
	{
		p->second.insert(pair<uint32_t,SecByteBlock>(destNodeId,key));
		
	}
	else
	{	
		map<uint32_t,SecByteBlock> tempMap;
		tempMap.insert(pair<uint32_t,SecByteBlock>(destNodeId,key));
		dhSecretKeyMapGlobal.insert(pair<uint32_t,map<uint32_t,SecByteBlock> >(nodeId,tempMap));
	}	
	
}	

uint32_t ApplicationUtil::getSecretBitFromGlobalMap(uint32_t nodeId, uint32_t destNodeId)
{

	map<uint32_t,map<uint32_t,uint32_t> >::iterator p;
	p = dhSecretBitMapGlobal.find(nodeId);

	if(p != dhSecretBitMapGlobal.end())
	{
		map<uint32_t,uint32_t>::iterator p1;
		p1 = p->second.find(destNodeId);
		if(p1 != dhSecretBitMapSub.end())
			return p1->second;
		else 
		{
			std::cout<<"hello";
			return -99;
		}
	}
	else 
		{
			std::cout<<"hello1";
			return -99;
		}	
}

void ApplicationUtil::putSecretBitInGlobalMap(uint32_t nodeId, uint32_t destNodeId, uint32_t value)
{

	map<uint32_t,map<uint32_t,uint32_t> >::iterator p;
	p = dhSecretBitMapGlobal.find(nodeId);
	if(p != dhSecretBitMapGlobal.end())
	{
		map<uint32_t,uint32_t>::iterator p1;
		p1 = p->second.find(destNodeId);	
		if(p1 != p->second.end())
			p->second[destNodeId] = value;
		else
			p->second.insert(pair<uint32_t,uint32_t>(destNodeId,value));
	}
	else
	{	
		map<uint32_t,uint32_t> tempMap;
		tempMap.insert(pair<uint32_t,uint32_t>(destNodeId,value));
		dhSecretBitMapGlobal.insert(pair<uint32_t,map<uint32_t,uint32_t> >(nodeId,tempMap));
	}	
	
}					

map<uint32_t,uint32_t> ApplicationUtil::getSecretBitSubMap(uint32_t nodeId)
{
	map<uint32_t,map<uint32_t,uint32_t> >::iterator p;
	p = dhSecretBitMapGlobal.find(nodeId);
		
	return p->second;
}

void ApplicationUtil::eraseSecretBitMapGlobal()
{
	 map<uint32_t,map<uint32_t,uint32_t> >::iterator p;
	 dhSecretBitMapGlobal.erase ( p, dhSecretBitMapGlobal.end() );
}

//swati
void ApplicationUtil::putAnnouncementInGlobalMap(uint32_t nodeId, uint32_t value)
{
	map<uint32_t,uint32_t>::iterator p;
	p = announcement.find(nodeId);
	if(p != announcement.end())
		announcement[nodeId] = value;
	else
	announcement.insert(pair<uint32_t,uint32_t>(nodeId,value));
}					

uint32_t ApplicationUtil::getAnnouncement(uint32_t nodeId)
{
	map<uint32_t,uint32_t>::iterator p;
	p = announcement.find(nodeId);
	//std::cout<<"Node "<<nodeId<<" stores "<<p->second<<"\n";
	return p->second;
}
void ApplicationUtil::putAnnouncementInReceivedMap(uint32_t nodeId, uint32_t senderNode, uint32_t value)
{
	map<uint32_t,map<uint32_t,uint32_t> >::iterator p;
	p = receivedAnnouncement.find(nodeId);
	if(p != receivedAnnouncement.end())
	{
		map<uint32_t,uint32_t>::iterator p1;
		p1 = p->second.find(senderNode);	
		if(p1 != p->second.end())
			p->second[senderNode] = value;
		else
		p->second.insert(pair<uint32_t,uint32_t>(senderNode,value));

	//	std::cout<<"Inserting "<<nodeId<<","<<senderNode<<","<<value<<"\n";
		
	}
	else
	{	
		map<uint32_t,uint32_t> tempMap;
		tempMap.insert(pair<uint32_t,uint32_t>(senderNode,value));
		receivedAnnouncement.insert(pair<uint32_t,map<uint32_t,uint32_t> >(nodeId,tempMap));
	//	std::cout<<"Inserting "<<nodeId<<","<<senderNode<<","<<value<<"\n";
	}
}
uint32_t ApplicationUtil::getReceivedAnnouncement(uint32_t nodeId, uint32_t senderNodeId)
{
	map<uint32_t,map<uint32_t,uint32_t> >::iterator p;
	p = receivedAnnouncement.find(nodeId);

	if(p != receivedAnnouncement.end())
	{
		map<uint32_t,uint32_t>::iterator p1;
		p1 = p->second.find(senderNodeId);
		if(p1 != receivedAnnouncementSubMap.end())
			return p1->second;
		else 
		{
			std::cout<<"hello";
			return -99;
		}
	}
	else 
		{
			std::cout<<"hello1";
			return -99;
		}	
}


map<uint32_t,uint32_t> ApplicationUtil::getAnnouncementSubMap(uint32_t nodeId)
{
	map<uint32_t,map<uint32_t,uint32_t> >::iterator p;
	p = receivedAnnouncement.find(nodeId);
	
	return p->second;
}


//write to csv file

void ApplicationUtil::writeOutputToFile(std::string fileName, uint32_t option, uint32_t numNodes,uint32_t length, double latency, double totalTime ) {
   std::ofstream fout;

   fout.open(fileName, std::ios_base::app);

    if(option == 1)
    {
	fout << numNodes << ',' << latency << ','<<totalTime;
    }
    else if(option == 2)
    {
	fout << length << ',' << latency << ','<<totalTime;
    }
	    
    fout << "\n";
    fout.close();
}

std::vector<int> relationsControl = {0,1,2,3,4};
bool boe = true;
int ApplicationUtil::getSIoTScenario()
{
	std::vector<int>::iterator it;
	int relation;

	if(!relationsControl.empty())
	{
		if(boe)
		{
			it = relationsControl.begin();
			relation = (*it);
			relationsControl.erase(it);
			boe = !boe;
			return relation;
		}else
		{
			it = relationsControl.end();
			relation = (*it);
			relationsControl.pop_back();
			boe = !boe;
			return relation;
		}
	}else
	{
		relationsControl = {0,1,2,3,4};

		it = relationsControl.begin();
		relation = (*it);
		relationsControl.erase(it);
		return relation;
	}
}

void ApplicationUtil::showMap()
{
	map<uint32_t,map<uint32_t,SecByteBlock>>::iterator it;
	for(it=dhSecretKeyMapGlobal.begin(); it!=dhSecretKeyMapGlobal.end(); it++)
	{
		map<uint32_t,SecByteBlock>::iterator ite;
		for(ite = it->second.begin(); ite != it->second.end(); ite++)
		{
			std::cout << "[" << it->first << "]" << "[" << ite->first << "] - : " << std::endl;
		}
	}

}

NodeContainer ApplicationUtil::installNewNodes(uint32_t nNodes, uint32_t community)
{
	NodeContainer c;
	c.Create (nNodes);
	/* Install interfaces */
	InternetStackHelper interv6;
	interv6.Install (c);
	CsmaHelper csmaHelper;
	csmaHelper.SetChannelAttribute ("DataRate", DataRateValue (5000000));
	csmaHelper.SetChannelAttribute ("Delay", TimeValue (MilliSeconds (2)));
	csmaHelper.SetDeviceAttribute ("Mtu", UintegerValue (150));
	NetDeviceContainer netDevice = csmaHelper.Install (c);

	SixLowPanHelper slowpan;
	slowpan.SetDeviceAttribute ("ForceEtherType", BooleanValue (true) );
	NetDeviceContainer sixNet = slowpan.Install (netDevice);

	Ipv6AddressHelper ipv6;
	std::string addBase ("2018:");
	std::stringstream ss;
	ss << community;
	addBase += ss.str ();
	addBase += "::";
	ipv6.SetBase (Ipv6Address (addBase.c_str()), Ipv6Prefix (64));
//	Ipv6InterfaceContainer ipv6Container = ipv6.Assign (sixNet);
	ipv6Containers[community].Add(ipv6.Assign (sixNet));

	MobilityHelper mobility;

	Ptr<ListPositionAllocator> positionAlloc = CreateObject<ListPositionAllocator>();
	double angle = 2 * 3.141592653589 / 50;
	double x = 25.0 + 15. * cos(angle);
	double y = 25.0 - 15. * sin(angle);
	positionAlloc->Add (Vector (x, y, 1.5));
	mobility.SetPositionAllocator(positionAlloc);
	mobility.Install (c);

	AsciiTraceHelper ascii;
	Ptr<OutputStreamWrapper> stream = ascii.CreateFileStream ("new-node.tr");
	csmaHelper.EnableAsciiAll (stream);
	csmaHelper.EnablePcapAll ("new-node", false);

	return c;
}

void ApplicationUtil::generateKeys(uint32_t& index)
{
    try
    {
		DH dh;
		AutoSeededRandomPool rnd;

		dh.AccessGroupParameters().Initialize(p, q, g);

		if(!dh.GetGroupParameters().ValidateGroup(rnd, 3))
			throw runtime_error("Failed to validate prime and generator");


		p = dh.GetGroupParameters().GetModulus();
		q = dh.GetGroupParameters().GetSubgroupOrder();
		g = dh.GetGroupParameters().GetGenerator();

		Integer v = ModularExponentiation(g, q, p);
		if(v != Integer::One())
			throw runtime_error("Failed to verify order of the subgroup");

		//////////////////////////////////////////////////////////////

		SecByteBlock priv(dh.PrivateKeyLength());
		SecByteBlock pub(dh.PublicKeyLength());
		dh.GenerateKeyPair(rnd, priv, pub);

		//////////////////////////////////////////////////////////////

		putPrivateKeyInMap(index, priv);
		putPublicKeyInMap(index, pub);
		setDhAgreedLength(dh.AgreedValueLength());

    }
    catch(const CryptoPP::Exception& e)
    {
        std::cerr << "Crypto error : "<< e.what() << std::endl;
    }

    catch(const std::exception& e)
    {
        std::cerr << "Standard error : "<<e.what() << std::endl;
    }
}

std::vector<Ptr<ApplicationUtil>> apps; //util of the communities
#include "statistics.h"
std::vector<Ptr<Statistics>> statistics;
