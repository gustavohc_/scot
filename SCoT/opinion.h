/*
 * opinion.h
 *
 *  Created on: Aug 2, 2018
 *      Author: gustavo
 */

#ifndef SCOT_OPINION_H_
#define SCOT_OPINION_H_

class Opinion : public Object
{
private:
	/**
	 * Belief field.
	 */
	double b;
	/**
	 * Disbelief field.
	 */
	double d;
	/**
	 * Uncertainty field.
	 */
	double u;
	/**
	 * Base-rate field. For a-priori notions of trust.
	 */
	double a;

	/**
	 * positive past observations.
	 */
	double r;
	/**
	 * negative past observations.
	 */
	double s;


public:

	Opinion ();

	/**
	 * Update an Opinion using feedback data.
	 * @param pos_fbacks Number of positive feedbacks
	 * @param neg_fbacks Number of negative feedbacks
	 */
	Opinion(double b, double d, double u, double a);

	Opinion(double b, double d, double u, double a, double r, double s);

	/**
	 * Calculate the expected value of this Opinion.
	 * @return The expected value of this Opinion
	 */
	void edit();

	/**
	 * Calculate the expected value of this Opinion.
	 * @return The expected value of this Opinion
	 */
	double expectedValue();

	/**
	 * The discount operator used in the analysis of transitive chains.
	 * @param that Second Opinion in 'this -> that' chain. Order matters!
	 * @return The discounted Opinion.
	 */
	Ptr<Opinion> discount(Ptr<Opinion> that);

	/**
	 * The consensus operator used in the 'fusion' of two Opinions.
	 * @param that The second Opinion being averaged with 'this' one.
	 * @return The fused Opinion.
	 */
	Ptr<Opinion> consensus(Ptr<Opinion> that);

	/**
	 * Compare this Opinion to another Opinion object. Comparisons are made on
	 * the basis of confidence=(1-uncertainty). Ties are broken by belief.
	 * @param that The other Opinion being compared to 'this' one
	 * @return 1 if this > that, 0 if this == that, -1 otherwise
	 */
	int32_t compareTo(Ptr<Opinion> that);

	/**
	 * Create a duplicate of this Opinion object.
	 * @return a duplicate of this Opinion object
	 */
	Ptr<Opinion> clone();

	void SetCallback(Callback<void, double> cb);

	// ************************** GETS AND SETS *****************************

	double getB();
	double getD();
	double getU();
	double getA();
	double getR();
	double getS();

	void setS(double s);
	void setR(double r);
	void setB(double b);
	void setD(double d);
	void setU(double u);
	void setA(double a);

	void incR();
	void incS();

};

/**
 * This class implements the
 * 'Trust Network Analysis with Subjective Logic' approach of Josang et. al.
 */

// *************************** CONSTRUCTORS ******************************

/**
 * Construct an Opinion by passing all field values.
 * @param b Belief
 * @param d Disbelief
 * @param u Uncertainty
 * @param a Base-rate
 */
Opinion::Opinion(){} // @suppress("Class members should be properly initialized")

Opinion::Opinion(double b, double d, double u, double a) : b(b), d(d), u(u), a(a) {} // @suppress("Class members should be properly initialized")

Opinion::Opinion(double b, double d, double u, double a, double r, double s) : b(b), d(d), u(u), a(a), r(r), s(s) {}

// ************************** PUBLIC METHODS *****************************

double Opinion::getB()
{
	return b;
}

double Opinion::getD()
{
	return d;
}

double Opinion::getU()
{
	return u;
}

double Opinion::getA()
{
	return a;
}

void Opinion::setB(double belief)
{
	b = belief;
}
void Opinion::setD(double disbelief)
{
	d = disbelief;
}
void Opinion::setU(double uncertainty)
{
	u = uncertainty;
}
void Opinion::setA(double alpha)
{
	a = alpha;
}

double Opinion::getR()
{
	return r;
}

double Opinion::getS()
{
	return s;
}

void Opinion::setS(double s)
{
	this->s = s;
}

void Opinion::setR(double r)
{
	this->r = r;
}

void Opinion::incR()
{
	this->r++;
}

void Opinion::incS()
{
	this->s++;
}

void Opinion::edit()
{
	this->b = (r / (r + s + 2.0));
	this->d = (s / (r + s + 2.0));
	this->u = (2.0 / (r + s + 2.0));
	return;
}


double Opinion::expectedValue()
{
	//statistic->writeTrustOutputFile(this->b + (this->a * this->u));
	return (this->b + (this->a * this->u));
}

Ptr<Opinion> Opinion::discount(Ptr<Opinion> that)
{
	double belief = this->b * that->b;
	double disbelief = this->d * that->d;
	double uncertainty = (1.0 - belief - disbelief);
	double alpha = that->a;
	return (CreateObject<Opinion>(belief, disbelief, uncertainty, alpha));
}


Ptr<Opinion> Opinion::consensus(Ptr<Opinion> that)
{
	double belief, disbelief, uncertainty;
	if((this->u==0.0) && (that->u == 0.0)){
		belief = ((this->b + (that->b) / 2.0));
		disbelief = (1.0 - belief);
		uncertainty = 0.0;
	}else{
		double demon = ((this->u + that->u) - (this->u * that->u));
		belief = (((this->b * that->u) + (that->b * this->u)) / demon);
		disbelief = (((this->d * that->u) + (that->d * this->u)) / demon);
		uncertainty = (1.0 - belief - disbelief);
	}//Math differs based on uncertainty values
	double alpha = this->a;
	return (CreateObject<Opinion>(belief, disbelief, uncertainty, alpha));
}


int32_t Opinion::compareTo(Ptr<Opinion> that)
{
	if (this->u < that->u)
		return (1);
	else if(this->u > that->u)
		return (-1);
	else{
		if(this->b > that->b)
			return (1);
		else if(this->b < that->b)
			return (-1);
		else
			return (0);
	}// Compare firt by confidence, ties broken by belief
}


Ptr<Opinion> Opinion::clone()
{
	return CreateObject<Opinion>(this->b, this->d, this->u, this->a);
}

#endif /* SCOT_OPINION_H_ */
