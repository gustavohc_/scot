/*
 * sybil.h
 *
 *  Created on: May 20, 2018
 *      Author: gustavo
 */
#include <iostream>
#include "ns3/core-module.h"
#include "ns3/applications-module.h"
#include "ns3/internet-module.h"
#include "ns3/network-module.h"
#include "cMap.h"

NS_LOG_COMPONENT_DEFINE ("SCoTScenario");

#ifndef _SYBIL_H_
#define _SYBIL_H_

using namespace ns3;

// classe para aplicacao da rede
class Sybil : public Object
{

public:

	Sybil ();
	Sybil (Ptr<Node> node, uint32_t community, uint32_t communitySize, bool goodBehaviour);

	uint32_t 										getId();
	void 											setId(uint32_t id);
	void 											idIdentificado (uint32_t id);
	uint32_t* 										getListaIdentificados (void);
	void 											setMaxIdentificados (uint32_t max);
	uint32_t 										getMaxIdentificados (void);
	void 											SetNode(Ptr<Node> node);
	Ptr<Node> 										GetNode(void) const;
	static uint32_t 								getIdByMacAddress (Mac48Address endereco);
	static Mac48Address 							getMacAddressById (uint32_t id);
	uint32_t 										getIdetified(uint32_t index);
	std::vector< std::pair<uint32_t, Address> >		getidsIdentificados();
	uint32_t										getCommunity();
	bool											getGoodBehaviour();
	void 											setGoodBehaviour(bool goodBehaviour);

	void StartApplicationNode (Ptr<Node> node);
	void showResults();

private:

	uint32_t 										id;
	uint32_t 										community;
	uint32_t 										communitySize;
	std::vector< std::pair<uint32_t, Address> >		idsIdentificados;
	uint32_t 										count_idsIdentificados;
	uint32_t	 									m_maxIds;
	Ptr<Node> 										node;
	std::vector<uint32_t>							fakeids;
	uint32_t										t_fakeids;
	bool											goodBehaviour;

	bool Sniffer(Ptr<NetDevice> device, Ptr<const Packet> packet, uint16_t protocol, const Address& sender, const Address& receiver, enum NetDevice::PacketType packetType);

	void AddListIdentifyier (const Address& endereco);

	//void Attacking(uint32_t attackType);

	void SendMessageAttack (Ptr<Socket> socket, std::string message, uint32_t index, uint32_t dest);

	void ReceiveMessageAttack (Ptr<Socket> socket);

	/**
	* \brief Application specific startup code
	*
	* The StartApplication method is called at the start time specified by Start
	* This method should be overridden by all or most application
	* subclasses.
	*/
	void StartApplication (void);

	/**
	* \brief Application specific shutdown code
	*
	* The StopApplication method is called at the stop time specified by Stop
	* This method should be overridden by all or most application
	* subclasses.
	*/
	void StopApplication (void);
};


Sybil::Sybil() {} // @suppress("Class members should be properly initialized")

Sybil::Sybil(Ptr<Node> node, uint32_t community, uint32_t communitySize, bool goodBehaviour)
{
	SetNode(node);
	setId(node->GetId());
	setMaxIdentificados(communitySize);
	StartApplication();
	this->community = community;
	this->communitySize = communitySize;
	this->goodBehaviour = goodBehaviour;

	t_fakeids = (rand() %50) + 1;
}

uint32_t Sybil::getId()
{
	return this->id;
}

void Sybil::setId(uint32_t id)
{
	this->id = id;
}

uint32_t Sybil::getCommunity()
{
	return this->community;
}

void Sybil::idIdentificado(uint32_t id) {}

//uint32_t* Sybil::getListaIdentificados (void){}

void Sybil::setMaxIdentificados (uint32_t max)
{
	NS_LOG_INFO ("[ATTACKER "<< GetNode()->GetId() << "] Defining the maximum number of id's: " << max);
	this->m_maxIds = max;
}

uint32_t Sybil::getMaxIdentificados (void)
{
	return this->m_maxIds;
}

//uint32_t Sybil::getMaxIdentificados (void){}

void Sybil::SetNode(Ptr<Node> node)
{
  this->node = node;
}

Ptr<Node> Sybil::GetNode(void) const
{
  return this->node;
}

bool Sybil::getGoodBehaviour()
{
	return this->goodBehaviour;
}

void Sybil::setGoodBehaviour(bool goodBehaviour)
{
	this->goodBehaviour = goodBehaviour;
}

//static uint32_t Sybil::getIdByMacAddress (Mac48Address endereco){}

//static Mac48Address Sybil::getMacAddressById (uint32_t id){}


void Sybil::StartApplication (void)
{
	count_idsIdentificados = 0;

	NS_LOG_INFO ("[ATTACKER "<< GetNode()->GetId() << "] Starting attacker application");

	NS_ASSERT_MSG (this->node, "Attacker is null!");

	Ptr<NetDevice> device = NULL;
	for(uint32_t i = 0; i < this->node->GetNDevices(); i++)
	{
		device = this->node->GetDevice(i);
		device->SetPromiscReceiveCallback(MakeCallback(&Sybil::Sniffer, this));
	}
}

void Sybil::StartApplicationNode (Ptr<Node> node)
{
	NS_LOG_INFO ("[ATTACKER] Starting attacker application - creating " << node->GetId() << " attackers");

	count_idsIdentificados = 0;

	SetNode(node);

	Ptr<NetDevice> device = NULL;
	for(uint32_t i = 0; i < node->GetNDevices(); i++)
	{
		device = node->GetDevice(i);
		device->SetPromiscReceiveCallback(MakeCallback(&Sybil::Sniffer, this));
	}
}

void Sybil::StopApplication (void)
{
	NS_LOG_INFO ("Ending attacker application in the node " << GetNode()->GetId());
//	Ptr<NetDevice> device = NULL;
//	for(uint32_t i = 0; i < node->GetNDevices(); i++)
//	{
//		device = node->GetDevice(i);
//		device->SetPromiscReceiveCallback(&Sybil::Sniffer, nullptr);
//	}
}

void Sybil::AddListIdentifyier (const Address& endereco)
{
	NS_LOG_INFO ("[ATTACKER "<< GetNode()->GetId() << "] Attempt to add " << endereco << " in the list of identifiers");
	uint32_t idDoNo = 32767;
	uint8_t * buffer = (uint8_t *) malloc(sizeof(uint8_t)*20);
	std::pair<uint32_t, Address> pair_ids;
	std::pair<uint32_t, Address> pair_temp;
	int search = -1;


	endereco.CopyTo(buffer);
	idDoNo = (uint32_t) buffer[5];
	idDoNo = cMap::getNodeIndex(idDoNo, community);
	/*for(uint32_t i = 0; i < 20; i++) {
	NS_LOG_INFO("[BYTE " << i <<"] " << (uint32_t)buffer[i] );
	}*/

	if(idDoNo >= communitySize)
		return;

	for(uint32_t i = 0; i < idsIdentificados.size(); i++)
	{
		pair_temp = idsIdentificados[i];

		if(pair_temp.first == idDoNo) {
		  search = i;
		  break;
		}
	}


	if(search != -1)
	{
		NS_LOG_INFO ("[ATTACKER "<< GetNode()->GetId() << "] (Time: " << Simulator::Now().GetSeconds() << "s) - Identifier " << idDoNo << " already in the list. No need to add again.");
	}
	else
	{
		NS_LOG_INFO ("[ATTACKER "<< GetNode()->GetId() << "] Identifier " << idDoNo << " isn't in the list. Add in the list.");
		if(count_idsIdentificados == m_maxIds)
		{
			NS_LOG_INFO ("[ATTACKER - Maximum number of " << m_maxIds << " reached. " << idDoNo << " won't be add to the list.");
			//NS_LOG_INFO ("[ATTACKER - Initiating the attack now!");
			//Simulator::ScheduleNow (&Attacking, 0, node->GetId(), 0);
		}
		else
		{
		  pair_ids = std::make_pair(idDoNo, endereco);
		  NS_LOG_INFO ("[ATTACKER "<< GetNode()->GetId() << "] Pair (" << pair_ids.first << ", " << pair_ids.second << ") are going to be add to the list of identifiers.");
		  idsIdentificados.push_back(pair_ids);
		  count_idsIdentificados++;
		}
	}
}

bool Sybil::Sniffer(Ptr<NetDevice> device, Ptr<const Packet> packet, uint16_t protocol, const Address& sender, const Address& receiver, enum NetDevice::PacketType packetType)
{
	NS_LOG_INFO ("[ATTACKER "<< GetNode()->GetId() << "] received one package in promiscuous mode from "<<  sender <<" to " << receiver);

	AddListIdentifyier(sender);
	AddListIdentifyier(receiver);

	return true;
}

void Sybil::showResults()
{
	NS_LOG_INFO ("-------------- Sybil Attacker " << this->node->GetId() << "-------------");
	NS_LOG_INFO ("Total Id identified: " << count_idsIdentificados);
	NS_LOG_INFO ("Ids: ");
	for(uint32_t i=0; i<idsIdentificados.size(); i++)
	{
		NS_LOG_INFO ("[" << idsIdentificados[i].first << ", " << idsIdentificados[i].second << "]");
	}
}

uint32_t Sybil::getIdetified(uint32_t index)
{
	return idsIdentificados[index].first;
}

std::vector< std::pair<uint32_t, Address> >	Sybil::getidsIdentificados()
{
	return this->idsIdentificados;
}

//void Sybil::Attacking(uint32_t attackType){}

#endif /* SCRATCH_SYBIL_H_ */

map<uint32_t, std::vector<Ptr<Sybil>>> sybils;
