/*
 * Social.h
 *
 *  Created on: May 10, 2018
 *      Author: gustavo
 */
#include <iostream>

#ifndef _SOCIAL_H_
#define _SOCIAL_H_


class SIoT{
private:

	/* Ownership */
	int oor;

	/* Location */
	int clor;

	/* Parental */
	int por;

	/* Work */
	int cwor;

	/* Social */
	int sor;

public:

	SIoT();

	SIoT(int oor, int clor, int por, int cwor, int sor);

	int getOor();

	void setOor(int oor);

	int getClor();

	void setClor(int clor);

	int getPor();

	void setPor(int por);

	int getCwor();

	void setCwor(int cwor);

	int getSor();

	void setSor(int sor);

};

SIoT::SIoT();

SIoT::SIoT(int oor, int clor, int por, int cwor, int sor)	: oor(oor), clor(clor), por(por), cwor(cwor), sor(sor) {};

int SIoT::getOor(){
	return this->oor;
}

void SIoT::setOor(int oor){
	this->oor = oor;
}

int SIoT::getClor(){
	return this->clor;
}

void SIoT::setClor(int clor){
	this->clor = clor;
}

int SIoT::getPor(){
	return this->por;
}

void SIoT::setPor(int por){
	this->por = por;
}

int SIoT::getCwor(){
	return this->cwor;
}

void SIoT::setCwor(int cwor){
	this->cwor = cwor;
}

int SIoT::getSor(){
	return this->sor;
}

void SIoT::setSor(int sor){
	this->sor = sor;
}

#endif /* SCRATCH_SOCIAL_H_ */
