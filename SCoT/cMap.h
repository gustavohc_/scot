/*
 * cMap.h
 *
 *  Created on: Nov 18, 2018
 *      Author: gustavo
 */

#ifndef SCRATCH_SCOT_CMAP_H_
#define SCRATCH_SCOT_CMAP_H_


class cMap
{
public:

	static uint32_t getNodeIndex(uint32_t sysId, uint32_t community);

	static void createMapping();

private:

	cMap();

	static std::vector<std::map<uint32_t, uint32_t>> mapping;
};

std::vector<std::map<uint32_t, uint32_t>> cMap::mapping;

cMap::cMap(){}

void cMap::createMapping()
{
	uint32_t communitySize = g_tNodes/g_tCommunities;
	uint32_t sysId = 0;
	std::map<uint32_t, uint32_t> tmp;


	for(uint32_t i=0; i<g_tCommunities; i++)
	{
		for(uint32_t j=0; j<communitySize;j++)
		{
			tmp.insert(std::pair<uint32_t, uint32_t>(sysId, j));
			sysId++;
		}
		mapping.push_back(tmp);
		tmp.clear();
	}
}

uint32_t cMap::getNodeIndex(uint32_t sysId, uint32_t community)
{
	return mapping[community].find(sysId)->second;
}

#endif /* SCRATCH_SCOT_CMAP_H_ */
