/*
 * ACManager.h
 *
 *  Created on: Oct 6, 2018
 *      Author: gustavo
 */
#ifndef SCRATCH_SCOT_ACMANAGER_H_
#define SCRATCH_SCOT_ACMANAGER_H_

#include "trustManager.h"

class AccessControl : public Object
{
private:

	uint32_t 	community;
	uint32_t 	network;
	double		requestLimit;

	Ptr<TrustManagement> tm;

	std::vector<uint32_t> 			suspectsNodes;
	std::vector<uint32_t> 			attackers;
	std::map<uint32_t, Timestamp> 	lastRequest;

public:

	AccessControl();
	AccessControl(uint32_t sizeCommunity, uint32_t community);

	void initNodes();

	void printValues();
	void printValuesCandidates();

	void initDirectTrust(uint32_t trustor, uint32_t trustee, bool verif);
	void initDirectTrustCandidates(uint32_t trustor, uint32_t trustee, bool verif);
	void updateDirectTrust(uint32_t trustor, uint32_t trustee, bool verif);
	void updateRecommendation(uint32_t trustor, uint32_t trustee);
	void updateRecommendationCandidates(uint32_t trustor, uint32_t trustee);
	void directToRecommendation();

	void incrementPositives(uint32_t trustor, uint32_t trustee);
	void incrementNegatives(uint32_t trustor, uint32_t trustee);

	uint32_t 	intersect(std::vector<uint32_t> u, std::vector<uint32_t> v);
	uint32_t 	join(std::vector<uint32_t> u, std::vector<uint32_t> v);
	double 		friendSet(uint32_t u, uint32_t v);
	double 		interestsSet(uint32_t u, uint32_t v);
	double 		perfilSet(uint32_t u, uint32_t v);
	double 		similarityNodes(uint32_t u, uint32_t v);
	void 		buildSimilarityNodes();
	bool 		verifyId(std::vector<uint32_t>& identity);
	bool 		verifyTimestamp(uint32_t node, Timestamp& timestamp);
	void 		updateTimestamp(uint32_t node, Timestamp& timestamp);

	void configCandidates();

	void requestAccess(std::vector<uint32_t> identity, Timestamp timestamp);

	bool findSNode(uint32_t node);

	void callBackSocial(uint32_t trustor, uint32_t trustee);
	void callBackSocialCandidates(uint32_t trustor, uint32_t trustee);
	void callBackSocial3d(uint32_t trustor, uint32_t trustee);

	void writeTrustAttackers();
};

AccessControl::AccessControl(uint32_t sizeCommunity, uint32_t community) // @suppress("Class members should be properly initialized")
{
	this->community = community;
	this->requestLimit = 8.;

	tm = CreateObject<TrustManagement>(sizeCommunity, community);

	initNodes();
}

void AccessControl::initNodes()
{
	tm->initValidation();
	buildSimilarityNodes();
}

/* --------------- Print values ---------------*/
void AccessControl::printValues()
{
	NS_LOG_INFO ("--------- Similarity values from TM of community: " << community);
	tm->showSim();

	NS_LOG_INFO ("--------- Trust values from TM of community: " << community);
	//tm->showOpinionMatrix();
	//tm->showDeviceInformations();
	tm->showSocialTrust();
	//tm->showValidators();
}

void AccessControl::printValuesCandidates()
{
	NS_LOG_INFO ("--------- Similarity values from TM of community: " << community);
	tm->showSimCandidates();

	NS_LOG_INFO ("--------- Trust values from TM of community: " << community);
	//tm->showDeviceInformationCandidates();
	//tm->showOpinionMatrixCandidates();
	tm->showSocialTrustCandidates();
	tm->showAttackinformation();

	NS_LOG_INFO ("Total of attackers identified: " << attackers.size());
}
/* --------------- End ---------------*/

//void AccessControl::updateDirectTrust(uint32_t trustor, uint32_t trustee, bool verif)
//{
//	if(!verif)
//		if(!findSNode(trustee))
//			suspectsNodes.push_back(trustee);
//	tm->computeDirectTrust(trustor, trustee, verif);
//}

void AccessControl::updateRecommendation(uint32_t trustor, uint32_t trustee)
{
	tm->computeTrust(trustor, trustee);
}

void AccessControl::directToRecommendation()
{
	/* --- Update the recommendations matrix with new direct trust --- */
	tm->updateRecommendations();
}

void AccessControl::updateRecommendationCandidates(uint32_t trustor, uint32_t trustee)
{
	if(findSNode(trustee))
		tm->computeTrustCandidates(trustor, trustee, true);
	else
		tm->computeTrustCandidates(trustor, trustee, false);
}

void AccessControl::incrementPositives(uint32_t trustor, uint32_t trustee)
{
	tm->incrementR(trustor, trustee);
}
void AccessControl::incrementNegatives(uint32_t trustor, uint32_t trustee)
{
	tm->incrementS(trustor, trustee);

	if(!findSNode(trustee))
		suspectsNodes.push_back(trustee);
}
/* ----------- End -----------------*/

/* ----------- Similarity -----------------*/
uint32_t AccessControl::intersect(std::vector<uint32_t> u, std::vector<uint32_t> v)
{
	std::vector<uint32_t> result;
	uint32_t cont=0;

	std::set_intersection (u.begin(), u.end(), v.begin(), v.end(), std::back_inserter(result));

	for(uint32_t n : result)
	{
		n++;
		cont++;
	}

	return cont;
}

uint32_t AccessControl::join(std::vector<uint32_t> u, std::vector<uint32_t> v)
{
	std::vector<uint32_t> result;
	uint32_t cont=0;

	std::set_union (u.begin(), u.end(), v.begin(), v.end(), std::back_inserter(result));

	for(uint32_t n : result)
	{
		n++;
		cont++;
	}

	return cont;
}

double AccessControl::friendSet(uint32_t u, uint32_t v)
{
	return intersect(tm->getFriendSet(u), tm->getFriendSet(v))/join(tm->getFriendSet(u), tm->getFriendSet(v));
}

double AccessControl::interestsSet(uint32_t u, uint32_t v)
{
	return intersect(tm->getInterestSet(u), tm->getInterestSet(v))/join(tm->getInterestSet(u), tm->getInterestSet(v));
}

double AccessControl::perfilSet(uint32_t u, uint32_t v)
{
	return intersect(tm->getPerfiltSet(u), tm->getPerfiltSet(v))/join(tm->getPerfiltSet(u), tm->getPerfiltSet(v));
}

double AccessControl::similarityNodes(uint32_t u, uint32_t v)
{
	double sum;

	if(u != v)
	{
		sum = (friendSet(u,v) * 0.6) + (interestsSet(u,v) * 0.4); // + (perfilSet(u,v) * 0.1);

		return sum;
	}
	return 0.0;
}

void AccessControl::buildSimilarityNodes()
{
	NS_LOG_INFO ("build similarity from TM");

	for(uint32_t i=0; i<g_cSizes[community]; i++)
	{
		for(uint32_t j=0; j<g_cSizes[community]; j++)
		{
			tm->setSimilarity(i,j, similarityNodes(i,j));

		}
	}
}
/* ----------- End -----------------*/

void AccessControl::configCandidates()
{
	NS_LOG_INFO ("Configuring candidates in the TM");
	tm->initCandidates(g_tNodesInComm);

	NS_LOG_INFO ("building similarity from nodes to candidates");
	for(uint32_t i=0; i<g_cSizes[community]; i++)
	{
		for(uint32_t j=g_cSizes[community]; j<g_tNodesInComm; j++)
		{
			tm->setSimilarity(i,j, similarityNodes(i,j));

		}
	}

	NS_LOG_INFO ("building similarity from candidates to nodes");
	for(uint32_t i=g_cSizes[community]; i<g_tNodesInComm; i++)
	{
		for(uint32_t j=0; j<g_cSizes[community]; j++)
		{
			tm->setSimilarity(i,j, similarityNodes(i,j));

		}
	}
}

bool AccessControl::findSNode(uint32_t node)
{
	for(auto i : suspectsNodes)
	{
		if(i == node)
			return true;
	}
	return false;
}

bool AccessControl::verifyId(std::vector<uint32_t>& identity)
{
	bool verif = true;
	for(uint32_t i=1; i<identity.size(); i++)
	{
		if(identity[i] >= tm->getSize())
			verif = false;
	}

	return verif;
}

bool AccessControl::verifyTimestamp(uint32_t node, Timestamp& timestamp)
{
	if(lastRequest.count(node)>0)
	{
//		double t = lastRequest.find(node)->second.GetTimestamp().GetSeconds() + requestLimit;
		if(timestamp.GetTimestamp().GetSeconds() < (lastRequest.find(node)->second.GetTimestamp().GetSeconds() + requestLimit))
			return false;
	}

	return true;
}

void AccessControl::updateTimestamp(uint32_t node, Timestamp& timestamp)
{
	 std::map<uint32_t, Timestamp>::iterator it;

	 it = lastRequest.find(node);
	 if(it != lastRequest.end())
		 it->second = timestamp;
	 else
		 lastRequest.insert(std::pair<uint32_t, Timestamp>(node, timestamp));
}

void AccessControl::requestAccess(std::vector<uint32_t> identity, Timestamp timestamp)
{
	/* ---- id vector consist: [0]: node [1]: identity ---- */

	map<uint32_t,double> tmp = tm->getSocialValues(identity[0]);
	long double sum = 0.;
	bool noChurn = true;
	bool noMultiId = true;

	for(auto row : tmp)
	{
		sum += row.second;
	}

	sum /= tmp.size();

	//----------------------------------------------------------------------------------
	// Verify if it is a attacker
	//----------------------------------------------------------------------------------
	/* ---- check if the is mult id attack ---- */
	if(identity.size() > 2 && sum < 0.6)
	{
		noMultiId = false;
		/* ---- check if the id id stolen or fabricated ---- */
		if(verifyId(identity))
		{
			bool control = true;
			for(auto & s : sybils.at(community))
			{
				if(cMap::getNodeIndex(s->getId(), community) == identity[0])
				{
					statistics[community]->increseNumTruePositive();
					control = false;
				}
			}
			if(control)
				statistics[community]->increseNumFalsePositive();
			else
				statistics[community]->increseNumAttack();
//				statistics[community]->increseNumMultiStolenId();

		}else
		{
			bool control = true;
			for(auto & s : sybils.at(community))
			{
				if(cMap::getNodeIndex(s->getId(), community) == identity[0])
				{
					statistics[community]->increseNumTruePositive();
					control = false;
				}
			}
			if(control)
				statistics[community]->increseNumFalsePositive();
			else
				statistics[community]->increseNumAttack();
//				statistics[community]->increseNumMultiFabricatedId();

		}

		if(findSNode(identity[0]))
		{
			if(std::find(attackers.begin(), attackers.end(), identity[0]) == attackers.end())
				attackers.push_back(identity[0]);
		}
	}

	if(!verifyTimestamp(identity[0], timestamp) && sum < 0.6 && identity.size() == 2)
	{
		noChurn = false;
		/* ---- churn attck ---- */
		/* ---- check if the id id stolen or fabricated ---- */
		if(verifyId(identity))
		{
			bool control = true;
			for(auto & s : sybils.at(community))
			{
				if(cMap::getNodeIndex(s->getId(), community) == identity[0])
				{
					statistics[community]->increseNumTruePositive();
					control = false;
				}
			}
			if(control)
				statistics[community]->increseNumFalsePositive();
			else
				statistics[community]->increseNumAttack();
//				statistics[community]->increseNumChurnStolenId();

		}else
		{
			bool control = true;
			for(auto & s : sybils.at(community))
			{
				if(cMap::getNodeIndex(s->getId(), community) == identity[0])
				{
					statistics[community]->increseNumTruePositive();
					control = false;
				}
			}
			if(control)
				statistics[community]->increseNumFalsePositive();
			else
				statistics[community]->increseNumAttack();
//				statistics[community]->increseNumChurnFabricatedId();

		}
		/* ---- if the id doen't exist check for fabricated id  ---- */

		if(findSNode(identity[0]))
		{
			if(std::find(attackers.begin(), attackers.end(), identity[0]) == attackers.end())
				attackers.push_back(identity[0]);
		}
	}

	/* ---- check if this request was target as attack  ---- */
	if(noMultiId && noChurn)
	{
		/* ---- check if this request as made by attacker ---- */
		for(auto & s : sybils.at(community))
		{
			if(cMap::getNodeIndex(s->getId(), community) == identity[0])
				statistics[community]->increseNumFalseNegative();
		}
	}

	if(sum > 0.6)
		NS_LOG_INFO ("Access granted to node: " << identity[0]);
	else
	{
		if(findSNode(identity[0]))
		{
			statistics[community]->increseNumAccDenied();
		}
	}



	updateTimestamp(identity[0], timestamp);
}

//----------------------------------------------------------------------------------
// Callbacks
//----------------------------------------------------------------------------------
void AccessControl::callBackSocial(uint32_t trustor, uint32_t trustee)
{
	tm->CallBackSocialTrust(trustor, trustee);
}

void AccessControl::callBackSocialCandidates(uint32_t trustor, uint32_t trustee)
{
	tm->CallBackSocialTrustCandidates(trustor, trustee);
}

void AccessControl::callBackSocial3d(uint32_t trustor, uint32_t trustee)
{
	tm->CallBackSocialTrust3d(trustor, trustee);
}

void AccessControl::writeTrustAttackers()
{
	uint32_t id;
	/* ---- check if this request as made by attacker ---- */
	for(auto & s : sybils.at(community))
	{
		id = cMap::getNodeIndex(s->getId(), community);
		map<uint32_t,double> tmp = tm->getSocialValues(id);
		long double sum = 0.;

		for(auto row : tmp)
		{
			sum += row.second;
		}

		sum /= tmp.size();
		statistics[community]->writeThis(id, sum);
	}
}


#endif /* SCRATCH_SCOT_ACMANAGER_H_ */
