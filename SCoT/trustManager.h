/*
 * trustManager.h
 *
 *  Created on: May 9, 2018
 *      Author: gustavo
 */

#include <iostream>
#include <vector>
#include <random>

#include "util.h"
#include "sybil.h"
#include "device.h"
#include "communities.h"
#include "experienceManager.h"
#include "validator.h"
#include "opinion.h"
#include "contextManager.h"
#include "timestamp.h"

#ifndef SCOT_TRUSTMANAGER_H_
#define SCOT_TRUSTMANAGER_H_

typedef std::vector<Ptr<Opinion>> Row;
typedef std::vector<Row> matrix;

class TrustManagement : public Object
{

private:

	matrix			dir_op;
	matrix 			rec_op;
	matrix 			max_matrix;
	matrix 			matrixA;
	matrix 			matrixB;

	std::vector<Ptr<Device>> 				devices;
	std::vector<Ptr<checksum>> 				validators;
	map<uint32_t, map<uint32_t,double>> 	socialTrust;

	Ptr<ExperienceManager>					experience;
	Ptr<ContextManager>						context;
	map<uint32_t, std::vector<uint32_t>> 	nodeFriendList;
	map<uint32_t, std::vector<uint32_t>> 	candidatesFriendList;

	/* The community size */
	uint32_t size;
	uint32_t communityIndex;
	uint32_t sizeCandidates;

	double aToCommunity;

	matrix 	subj_square(matrix& source);
	matrix 	subj_squareCandidates(matrix& source);
	void 	setVecRelation(Relation::Copy setting, uint32_t source);

	Callback<void, uint32_t, uint32_t, double>		cb;
	Callback<void, int, double, double> 			cb2;
	Callback<void, int, double, double>				cb3;
	Callback<void, int, double, double, double> 	cb4;
	Callback<void, int, double, double, double> 	cb5;

public:

	TrustManagement ();
	TrustManagement(uint32_t sizeCommunity, uint32_t community);

	void 	InitManager();
	void 	initValidation();
	int 	parserFriendDataSet();

	void printOpinion();
	void showOpinionMatrix();
	void showDeviceInformations();
	void showSocialTrust();
	void showValidators();
	void showSim();
	void showDeviceInformationCandidates();
	void showOpinionMatrixCandidates();
	void showSocialTrustCandidates();
	void showAttackinformation();
	void showSimCandidates();

	void 		calcTrust(uint32_t source);
	void 		update(uint32_t source, uint32_t dest);
	void 		computeTrust(uint32_t sender,uint32_t recv);
	void 		updateRecommendations();
	void 		commitFBack(uint32_t source, uint32_t dest);
	void 		incrementR(uint32_t trustor, uint32_t trustee);
	void 		incrementS(uint32_t trustor, uint32_t trustee);
	uint32_t 	getSize();


	void initDirectTrustCandidates(uint32_t trustor, uint32_t trustee, bool accomplishedTask);
	void calcTrustCandidates(uint32_t source);
	void setVecRelationCandidates(Relation::Copy setting, uint32_t source);
	void computeTrustCandidates(uint32_t sender, uint32_t recv, bool suspicious);

	void addNewNode(uint32_t node);

	void doChecksum(Ptr<checksum> chk, DWORD memory);
	void addValidator(uint32_t node);
	bool check(uint32_t node, checksum* checksum);

	void initCandidates(uint32_t candidates);

	/* --- Social Functions ---*/
	std::vector<uint32_t> 	getFriendSet(uint32_t from);
	std::vector<uint32_t> 	getInterestSet(uint32_t from);
	std::vector<uint32_t> 	getPerfiltSet(uint32_t from);
	void 					computeSocialTrust(uint32_t u, uint32_t v);
	double 					SIoTTable(Relation::SIoT social);
	Relation::SIoT 			getSIoTRelation(uint32_t u, uint32_t v);
	map<uint32_t,double>	getSocialValues(uint32_t node);

	void setSimilarity(uint32_t u, uint32_t v, double sim);

	void CallBackSocialTrust(uint32_t u, uint32_t v);
	void CallBackSocialTrustCandidates(uint32_t u, uint32_t v);
	void CallBackSocialTrust3d(uint32_t u, uint32_t v);

};


TrustManagement::TrustManagement(){} // @suppress("Class members should be properly initialized")

TrustManagement::TrustManagement(uint32_t sizeCommunity, uint32_t community) // @suppress("Class members should be properly initialized")
{
	experience = CreateObject<ExperienceManager>();
	context = CreateObject<ContextManager>(community);

	this->size = sizeCommunity;
	this->communityIndex = community;
	InitManager();

	/* --- CallBacks ----*/
	cb = MakeCallback (&Statistics::writeSocialTrustOutputFile, &(*statistics[community]));
	cb2 = MakeCallback (&Statistics::writeRelationTrustOutputFile, &(*statistics[community]));
	cb3 = MakeCallback (&Statistics::writeRelationTrustOutputFileCandidates, &(*statistics[community]));
	cb4 = MakeCallback (&Statistics::writeRelationTrustOutputFile3d, &(*statistics[community]));
	cb5 = MakeCallback (&Statistics::writeRelationTrustOutputFile3dAll, &(*statistics[community]));
}

void TrustManagement::InitManager()
{
	devices.clear();

	for(uint32_t i=0; i < size; i++)
	{
		Row tmp;
		for(uint32_t j=0; j < size; j++)
		{
			if(i != j){
				tmp.push_back(CreateObject<Opinion>(0.0,0.0,1.0,context->aValueContext(), 0.0, 0.0));
					//rec_op[i]->push_back(rec_op[i][j]->push_back(new Opinion(0.0,0.0,1.0,0.5)));
			}else{
				tmp.push_back(CreateObject<Opinion>(0.0,0.0,0.0,0.0, 0.0, 0.0));
			}//The device is the same [0][0]
		}
		rec_op.push_back(tmp);
		dir_op.push_back(tmp);
		validators.push_back(CreateObject<checksum>());
		/* Create the device with params */
		devices.push_back(CreateObject<Device>(Device::USR_GOOD, false, i, communityIndex));
	}

	for(uint32_t i=0; i < size; i++)
	{
		std::map<uint32_t, double> tmp;
		for(uint32_t j=0; j < size; j++)
		{
			tmp.insert( std::pair<uint32_t, double>(j, 0.0));
		}
		socialTrust.insert(std::pair<uint32_t, std::map<uint32_t,double>>(i, tmp));
	}

	for(uint32_t i=0; i < size; i++)
	{
		Row tmp, r;
		r = rec_op[i];
		for(uint32_t j=0; j < size; j++)
		{
			tmp.push_back(r[j]);
			//max_matrix[i]->push_back(max_matrix[i][j]->push_back(rec_op[i][j]));
		}
		max_matrix.push_back(std::move(tmp));
	}

	if(parserFriendDataSet())
	{
		NS_LOG_INFO ("Problem to parser the friend dataset!");
		std::exit (EXIT_FAILURE);
	}
}

/* --------------- Trust evaluation ---------------*/

matrix TrustManagement::subj_square(matrix& source)
{
	Ptr<Opinion> op_holder;
	matrix dest;

	//dest.reserve(devices);
	Row tmp, r, r2;
	for(uint32_t i=0; i < size; i++)
	{
		r = source[i];
		for(uint32_t j=0; j < size; j++)
		{
			for(uint32_t k=0; k < size; k++)
			{
				if(k==0)
				{
					r2 = source[0];
					tmp.push_back(r[0]->discount(r2[j]));
					//dest.push_back(dest[i].push_back(source[i][0]->discount((*source[0][j]))));
				}else
				{
					r2 = source[k];
					op_holder = r[k]->discount(r2[j]);
					tmp.push_back(op_holder->consensus(op_holder));
					//dest.push_back(dest[i].push_back((dest[i]->consensus(op_holder))));
				} // Don't consensus first discount; it isn't monotonic
			} // The inner loop of matrix multiply
		} // Discount and then consensus Opinions
		dest.push_back(std::move(tmp));
	}  // Perform 'multiply' over all user vectors and rows

	bool max_modified = false;
	for(uint32_t i=0; i < size; i++)
	{
		tmp = max_matrix[i];
		r = dest[i];
		for(uint32_t j=0; j < size; j++)
		{
			if(i != j)
			{
				if(r[j]->compareTo(tmp[j]) == 1)
				{
					tmp[j] = r[j]->clone();
					max_matrix[i] = tmp;
					max_modified = true;
				} // Flag flips if a copy to 'max_value' is made
			}
		} // Update the 'max_value' matrix at each position
	} // Iterate over all matrix positions

	if(max_modified)
		return dest;
	else
	{
		dest.clear();
		return dest;
	}
}

void TrustManagement::calcTrust(uint32_t source)
{

	matrixA = subj_square(rec_op);
	while(!matrixA.empty())
	{
		matrixB = subj_square(matrixA);
		if(matrixB.empty())
			break;
		matrixA = subj_square(matrixB);
	}// Multiply until 'max_matrix' is saturated

	for(uint32_t i=0; i < size; i++)
	{
		if(i != source)
		{
			Ptr<Relation> rel = devices[source]->getReputation(i);
			rel->setTrust(max_matrix[i][source]->expectedValue());
		}
	} // Export Opinion expected values's as trust values
}

void TrustManagement::update(uint32_t source, uint32_t dest)
{
	rec_op[source][dest]->edit();
}

void TrustManagement::computeTrust(uint32_t sender, uint32_t recv)
{

	commitFBack(sender, recv);
	update(sender, recv);


	for(uint32_t i=0; i<size; i++)
	{
		if(devices[i]->getId() == recv)
		{
			if(devices[i]->getModel() != Device::USR_GOOD)
			{
				setVecRelation(Relation::HONEST, recv);
				calcTrust(recv);
				setVecRelation(Relation::GLOBAL, recv);
				computeSocialTrust(sender, recv);
			}else
			{
				calcTrust(recv);
				computeSocialTrust(sender, recv);
			}
		}
	}
}

void TrustManagement::setVecRelation(Relation::Copy setting, uint32_t source)
{
	for(uint32_t i=0; i<size; i++)
	{
		devices[source]->getReputation(i)->setHistory(setting);
	}
}

void TrustManagement::incrementR(uint32_t trustor, uint32_t trustee)
{
	dir_op[trustor][trustee]->incR();
}

void TrustManagement::incrementS(uint32_t trustor, uint32_t trustee)
{
	dir_op[trustor][trustee]->incS();
}

void TrustManagement::updateRecommendations()
{
	rec_op = dir_op;
}

void TrustManagement::commitFBack(uint32_t source, uint32_t dest)
{
	bool a = devices[source]->getModel() == Device::USR_SYBL;
	bool b = devices[dest]->getModel() == Device::USR_SYBL;
	srand((int)time(0));

	if(a || b)
	{
		NUM_FBACK_SYBL++;
		return;
	}

	double r = (double)((rand() % 100) + 1);

	if(r > devices[dest]->getHonesty())
	{
		devices[dest]->getReputation(source)->incGlobalPos();
	}
}
/* --------------- End ---------------*/

/* --------------- Memory checksum ---------------*/
void TrustManagement::doChecksum(Ptr<checksum> chk, DWORD memory)
{
	chk->add(memory); // a DWORD
}

void TrustManagement::initValidation()
{
	DWORD key;

	for(uint32_t i=0; i<size; i++)
	{
		key = (DWORD) GetPointer (communities[communityIndex].Get(i));
		doChecksum(validators[i], key);
	}
}

void TrustManagement::addValidator(uint32_t node)
{
	DWORD key;

	key = (DWORD) GetPointer (communities[communityIndex].Get(node));
	doChecksum(validators[node], key);
}

bool TrustManagement::check(uint32_t node, checksum* checksum)
{
	if(node < validators.size())
	{
		if(validators[node]->get() == checksum->get())
			return true;
		else
			return false;
	}
	return false;
}

uint32_t TrustManagement::getSize()
{
	return this->size;
}

/* --------------- End ---------------*/

/* --------------- Add new nodes ---------------*/
int TrustManagement::parserFriendDataSet()
{
	std::string line, data_str, key("	");
	uint32_t node, nodeFriend;

	std::ifstream in ("/home/gustavo/workspace/ns-allinone-3.27/ns-3.27/scratch/Brightkite_edges.txt", std::ifstream::in);
	if (in.is_open())
	{
		while ( getline (in,line) )
		{
			if(line.size() > 0)
			{
				data_str = line.substr(0,line.find(key, 0));
				node = std::stoul(data_str);

				data_str = line.substr(line.find(key, 0)+1,line.size());
				nodeFriend = std::stoul(data_str);

				if(node >= g_tNodesInComm)
					break;

				if(node < size)
				{
					if(nodeFriend < g_tNodesInComm)
					{
						if(nodeFriendList.count(node)>0)
						{
							map<uint32_t, std::vector<uint32_t>>::iterator it;
							it = nodeFriendList.find(node);
							it->second.push_back(nodeFriend);
						}else
						{
							std::vector<uint32_t> tmp;
							tmp.push_back(nodeFriend);
							nodeFriendList.insert ( std::pair<uint32_t, std::vector<uint32_t>>(node,tmp));
						}
					}
				}else
				{
					if(nodeFriend < g_tNodesInComm)
					{
						if(candidatesFriendList.count(node)>0)
						{
							map<uint32_t, std::vector<uint32_t>>::iterator it;
							it = candidatesFriendList.find(node);
							it->second.push_back(nodeFriend);
						}else
						{
							std::vector<uint32_t> tmp;
							tmp.push_back(nodeFriend);
							candidatesFriendList.insert ( std::pair<uint32_t, std::vector<uint32_t>>(node,tmp));
						}
					}
				}
			}
		}
	}else
		return 1;

	for(auto n : nodeFriendList)
	{
		devices[n.first]->setListFriends(n.second);
	}
	return 0;
}

void TrustManagement::addNewNode(uint32_t node)
{
	Row tmp, r;
	size += 1;

	for(uint32_t j=0; j < size; j++)
	{
		tmp.push_back(CreateObject<Opinion>(0.0,0.0,1.0,context->aValueContext(), 0.0, 0.0));
	}
	rec_op.push_back(tmp);
	dir_op.push_back(tmp);

	for(uint32_t j=0; j < size-1; j++)
	{
		rec_op[j].push_back(CreateObject<Opinion>(0.0,0.0,1.0,context->aValueContext(), 0.0, 0.0));
		dir_op[j].push_back(CreateObject<Opinion>(0.0,0.0,1.0,context->aValueContext(), 0.0, 0.0));
	}

	std::map<uint32_t, map<uint32_t,double>>::iterator it;
	for(it=socialTrust.begin(); it!=socialTrust.end(); it++)
	{
		it->second.insert(std::pair<uint32_t, double>(node, 0.0));
	}

	std::map<uint32_t, double> tmp2;
	for(uint32_t j=0; j < size; j++)
	{
		tmp2.insert( std::pair<uint32_t, double>(j, 0.0));
	}
	socialTrust.insert(std::pair<uint32_t, std::map<uint32_t,double>>(node, tmp2));


	r = rec_op[size-1];
	for(uint32_t j=0; j < size; j++)
	{
		tmp.push_back(r[j]);
	}
	max_matrix.push_back(std::move(tmp));

	for(uint32_t j=0; j < size-1; j++)
	{
		max_matrix[j].push_back(r[j]);
	}

	/* Check if the device has previous interaction */
	std::vector<double> historical = experience->getPastExperience(node);

	/* Add new relation in the existing devices */
	for(uint32_t i=0; i<size; i++)
	{
		devices[i]->addRelation(i, node);
	}

	/* ---- add one more key ----*/
	validators.push_back(CreateObject<checksum>());
	addValidator(node);

}

/* -------------- Social Functions ------------------------ */

void TrustManagement::computeSocialTrust(uint32_t u, uint32_t v)
{

	std::map<uint32_t, std::map<uint32_t,double>>::iterator it = socialTrust.find(u);
	std::map<uint32_t, double>::iterator ite = it->second.find(v);
	double gamma = SIoTTable(devices[u]->getReputation(v)->getRelationSIoT());
	double beta;
	//double directTrust;

	//directTrust = (dir_op[u][v]->getB() + (dir_op[u][v]->getA() * dir_op[u][v]->getU()));


	if(gamma == 0.5)
		beta = 0.0;
	else if(gamma == 0.4)
		beta = 0.1;
	else if( gamma == 0.3)
		beta = 0.2;
	else if(gamma == 0.2)
		beta = 0.3;
	else
		beta = 0.4;

	/* -------- T = Td + S + Tr -------- */
	ite->second = (dir_op[u][v]->expectedValue() * 0.5) + (devices[u]->getReputation(v)->getSimilarity() * beta) + (devices[u]->getReputation(v)->getTrust() * gamma);


	cb (u, v, ite->second);
}

map<uint32_t,double> TrustManagement::getSocialValues(uint32_t node)
{
	map<uint32_t,double> tmp;
	for(uint32_t i=0; i<size; i++)
	{
		tmp.insert( std::pair<uint32_t, double>(i, socialTrust.at(i).at(node)));
	}
	return tmp;
}

/* -------------- Similarity and the sets ------------------------ */
std::vector<uint32_t> TrustManagement::getFriendSet(uint32_t from)
{
	return devices[from]->getListFriends();
}

std::vector<uint32_t> TrustManagement::getInterestSet(uint32_t from)
{
	return devices[from]->getListInterests();
}

std::vector<uint32_t> TrustManagement::getPerfiltSet(uint32_t from)
{
	return devices[from]->getListPerfil();
}

void TrustManagement::setSimilarity(uint32_t u, uint32_t v, double sim)
{
	devices[u]->getReputation(v)->setSimilarity(sim);
}

double TrustManagement::SIoTTable(Relation::SIoT social)
{
	switch(social)
	{
		case Relation::OWNERSHIP : return 0.5;

		case Relation::LOCATION : return 0.4;

		case Relation::COWORK : return 0.3;

		case Relation::SOCIAL : return 0.2;

		case Relation::PARENTAL : return 0.1;

		default : return 0.0;
	}
}

Relation::SIoT TrustManagement::getSIoTRelation(uint32_t u, uint32_t v)
{
	return devices[u]->getReputation(v)->getRelationSIoT();
}
/* --------------- End ---------------*/

//----------------------------------------------------------------------------------
// Functions to the nodes candidates to access the network
//----------------------------------------------------------------------------------
matrix TrustManagement::subj_squareCandidates(matrix& source)
{
	Ptr<Opinion> op_holder;
	matrix dest;

	//dest.reserve(devices);
	Row tmp, r, r2;
	for(uint32_t i=0; i < size; i++)
	{
		r = source[i];
		for(uint32_t j=size; j < sizeCandidates; j++)
		{
			for(uint32_t k=0; k < sizeCandidates; k++)
			{
				if(k==0)
				{
					r2 = source[0];
					tmp.push_back(r[0]->discount(r2[j]));
					//dest.push_back(dest[i].push_back(source[i][0]->discount((*source[0][j]))));
				}else
				{
					r2 = source[k];
					op_holder = r[k]->discount(r2[j]);
					tmp.push_back(op_holder->consensus(op_holder));
					//dest.push_back(dest[i].push_back((dest[i]->consensus(op_holder))));
				} // Don't consensus first discount; it isn't monotonic
			} // The inner loop of matrix multiply
		} // Discount and then consensus Opinions
		dest.push_back(std::move(tmp));
	}  // Perform 'multiply' over all user vectors and rows

	bool max_modified = false;
	for(uint32_t i=0; i < size; i++)
	{
		tmp = max_matrix[i];
		r = dest[i];
		for(uint32_t j=size; j < sizeCandidates; j++)
		{
			if(i != j)
			{
				if(r[j]->compareTo(tmp[j]) == 1)
				{
					tmp[j] = r[j]->clone();
					max_matrix[i] = tmp;
					max_modified = true;
				} // Flag flips if a copy to 'max_value' is made
			}
		} // Update the 'max_value' matrix at each position
	} // Iterate over all matrix positions

	if(max_modified)
		return dest;
	else
	{
		dest.clear();
		return dest;
	}
}

void TrustManagement::calcTrustCandidates(uint32_t source)
{

	matrixA = subj_squareCandidates(rec_op);
	while(!matrixA.empty())
	{
		matrixB = subj_squareCandidates(matrixA);
		if(matrixB.empty())
			break;
		matrixA = subj_squareCandidates(matrixB);
	}// Multiply until 'max_matrix' is saturated

	for(uint32_t i=size; i < sizeCandidates; i++)
	{
		if(i != source)
		{
			Ptr<Relation> rel = devices[source]->getReputation(i);
			rel->setTrust(max_matrix[i][source]->expectedValue());
		}
	} // Export Opinion expected values's as trust values

}

void TrustManagement::setVecRelationCandidates(Relation::Copy setting, uint32_t source)
{
	for(uint32_t i=size; i<sizeCandidates; i++)
	{
		devices[source]->getReputation(i)->setHistory(setting);
	}

}

void TrustManagement::computeTrustCandidates(uint32_t sender, uint32_t recv, bool suspicious)
{

	commitFBack(sender, recv);
	update(sender, recv);


	for(uint32_t i=size; i<sizeCandidates; i++)
	{
		if(devices[i]->getId() == recv)
		{
			if(suspicious)
			{
				setVecRelationCandidates(Relation::HONEST, recv);
				calcTrustCandidates(recv);
				setVecRelationCandidates(Relation::GLOBAL, recv);
				computeSocialTrust(sender, recv);
			}else
			{
				calcTrustCandidates(recv);
				computeSocialTrust(sender, recv);
			}
		}
	}
}

void TrustManagement::initCandidates(uint32_t candidates)
{
	this->sizeCandidates = candidates;

	/*-------- Between the candidates ----*/
	for(uint32_t i=size; i < sizeCandidates; i++)
	{
		Row tmp;
		for(uint32_t j=0; j < sizeCandidates; j++)
		{
			if(i != j)
			{
				tmp.push_back(CreateObject<Opinion>(0.0,0.0,1.0,context->aValueContext(), 0.0, 0.0));
			}else
			{
				tmp.push_back(CreateObject<Opinion>(0.0,0.0,0.0,0.0, 0.0, 0.0));
			}//The device is the same [0][0]
		}
		rec_op.push_back(tmp);
		dir_op.push_back(tmp);
		validators.push_back(CreateObject<checksum>());
		/* Create the device with params */
		devices.push_back(CreateObject<Device>(Device::USR_GOOD, false, i, communityIndex));
	}

	/*-------- Set the friend list on candidates by the dataset ----*/
	for(auto n : candidatesFriendList)
	{
		if(sybils.count(communityIndex) > 0)
		{
			bool control = true;
			/*-------- Sybils don't get friend relationship ----*/
			for(auto s : sybils.at(communityIndex))
			{
				if(s->getId() == n.first)
					control = false;
			}

			(control) ?	devices[n.first]->setListFriends(n.second) : devices[n.first]->setListFriends(std::vector<uint32_t>());
		}else
			devices[n.first]->setListFriends(n.second);
	}

	/*-------- Add relations between nodes and candidates ----*/
	for(uint32_t i=0; i < size; i++)
	{
		for(uint32_t j=size; j < sizeCandidates; j++)
		{
			devices[i]->addRelation(i,j);
		}
	}

	/*-------- Prepare the social trust ----*/
	for(uint32_t i=size; i < sizeCandidates; i++)
	{
		std::map<uint32_t, double> tmp;
		for(uint32_t j=0; j < sizeCandidates; j++)
		{
			tmp.insert( std::pair<uint32_t, double>(j, 0.0));
		}
		socialTrust.insert(std::pair<uint32_t, std::map<uint32_t,double>>(i, tmp));
	}

	/*-------- Add row to the max matrix ----*/
	for(uint32_t i=size; i < sizeCandidates; i++)
	{
		Row tmp, r;
		r = rec_op[i];
		for(uint32_t j=0; j < sizeCandidates; j++)
		{
			tmp.push_back(r[j]);
		}
		max_matrix.push_back(std::move(tmp));
	}

	/*-------- Build the matrix between the nodes and the candidates ----*/
	for(uint32_t i=0; i < size; i++)
	{
		for(uint32_t j=size; j < sizeCandidates; j++)
		{
			/* Check if the device has previous interaction */
			std::vector<double> historical = experience->getPastExperience(j);

			if(i != j)
			{
				rec_op[i].push_back(CreateObject<Opinion>(0.0,0.0,1.0,context->aValueContext(), historical[0], historical[1]));
				dir_op[i].push_back(CreateObject<Opinion>(0.0,0.0,1.0,context->aValueContext(), historical[0], historical[1]));
			}else
			{
				rec_op[i].push_back(CreateObject<Opinion>(0.0,0.0,0.0,0.0, 0.0, 0.0));
				dir_op[i].push_back(CreateObject<Opinion>(0.0,0.0,0.0,0.0, 0.0, 0.0));
			}//The device is the same [0][0]
		}
	}

	for(uint32_t i=0; i < sizeCandidates; i++)
	{
		for(uint32_t j=size; j < sizeCandidates; j++)
		{
			socialTrust.at(i).insert( std::pair<uint32_t, double>(j, 0.0));
		}
	}

	for(uint32_t i=0; i < sizeCandidates; i++)
	{
		Row tmp;
		tmp = rec_op[i];
		for(uint32_t j=size; j < sizeCandidates; j++)
		{
			max_matrix[i].push_back(tmp[j]);
		}
	}
}
/* --------------- End ---------------*/

/* --------------- Print informations ---------------*/
void TrustManagement::printOpinion()
{
	for(uint32_t i=0; i<size; i++)
	{
		NS_LOG_INFO ("Relation of device " << devices[i]->getId());
		for(Ptr<Relation> r : devices[i]->getAllReputarion())
		{
			NS_LOG_INFO ("from: " << r->getDeviceFrom() << " - to: " << r->getDeviceTo());
		}
	}
}

void TrustManagement::showOpinionMatrix()
{
	NS_LOG_INFO ("Matrix rec_op:");
	for(uint32_t i=0; i<size; i++)
	{
		for(uint32_t j=0; j<size; j++)
		{
			if(i!=j)
				NS_LOG_INFO ("[" << i << "]" << "[" << j << "] - b: "<< rec_op[i][j]->getB() << " d: " << rec_op[i][j]->getD() << " u: " << rec_op[i][j]->getU() << " a: " << rec_op[i][j]->getA() << " - b: "<< max_matrix[i][j]->getB() << " d: " << max_matrix[i][j]->getD() << " u: " << max_matrix[i][j]->getU() << " a: " << max_matrix[i][j]->getA());
		}
	}

	NS_LOG_INFO ("Matrix dir_op:  ");
	for(uint32_t i=0; i<size; i++)
	{
		for(uint32_t j=0; j<size; j++)
		{
			if(i!=j)
				NS_LOG_INFO ("[" << i << "]" << "[" << j << "] - b: "<< dir_op[i][j]->getB() << " d: " << dir_op[i][j]->getD() << " u: " << dir_op[i][j]->getU() << " a: " << dir_op[i][j]->getA());
		}
	}
}

void TrustManagement::showDeviceInformations()
{
	NS_LOG_INFO ("Trust values in the device's relations:");
	for(uint32_t i=0; i<size; i++)
	{
		NS_LOG_INFO ("Device: " << devices[i]->getId() << " Behavior: " << devices[i]->getModel());
		NS_LOG_INFO ("Friend lists: Size("<< devices[i]->getListFriends().size() << ") [" << devices[i]->getListFriends()[0] << ", " << devices[i]->getListFriends()[1] << ", " << devices[i]->getListFriends()[2] << ", " << devices[i]->getListFriends()[3] << ", " << devices[i]->getListFriends()[4] << "]");
		NS_LOG_INFO ("Interests lists: Size("<< devices[i]->getListInterests().size() << ") [" << devices[i]->getListInterests()[0] << ", " << devices[i]->getListInterests()[1] << ", " << devices[i]->getListInterests()[2] << ", " << devices[i]->getListInterests()[3] << ", " << devices[i]->getListInterests()[4] << "]");
		for(Ptr<Relation> r : devices[i]->getAllReputarion())
		{
			if(r->getDeviceFrom() != r->getDeviceTo())
				NS_LOG_INFO ("from: " << r->getDeviceFrom() << " - to: " << r->getDeviceTo() << " - Social Relation: " << r->getRelationSIoT() << " > trust value: " << r->getTrust());
		}
	}
}

void TrustManagement::showSocialTrust()
{
	NS_LOG_INFO ("Social Trust:");
	for(uint32_t i=0; i<size; i++)
	{
		for(uint32_t j=0; j<size; j++)
		{
			if(i!=j)
				NS_LOG_INFO ("[" << i << "]" << "[" << j << "] - : " << socialTrust.at(i).at(j));
		}
	}
}

void TrustManagement::showValidators()
{
	for(uint32_t i=0; i<size; i++)
	{
		NS_LOG_INFO ("Validator:  ");
		NS_LOG_INFO (i << ":  " << validators[i]->get());
	}
}

void TrustManagement::showDeviceInformationCandidates()
{
	NS_LOG_INFO ("Trust values in the candidates relations:");
	for(uint32_t i=size; i<sizeCandidates; i++)
	{
		NS_LOG_INFO ("Device: " << devices[i]->getId() << " Behavior: " << devices[i]->getModel());
		if(devices[i]->getListFriends().empty())
			NS_LOG_INFO ("This node don't have friend list!");
		else
			NS_LOG_INFO ("Friend lists: Size("<< devices[i]->getListFriends().size() << ") [" << devices[i]->getListFriends()[0] << ", " << devices[i]->getListFriends()[1] << ", " << devices[i]->getListFriends()[2] << ", " << devices[i]->getListFriends()[3] << ", " << devices[i]->getListFriends()[4] << "]");

		NS_LOG_INFO ("Interests lists: Size("<< devices[i]->getListInterests().size() << ") [" << devices[i]->getListInterests()[0] << ", " << devices[i]->getListInterests()[1] << ", " << devices[i]->getListInterests()[2] << ", " << devices[i]->getListInterests()[3] << ", " << devices[i]->getListInterests()[4] << "]");
		for(Ptr<Relation> r : devices[i]->getAllReputarion()){
			if(r->getDeviceFrom() != r->getDeviceTo())
				NS_LOG_INFO ("from: " << r->getDeviceFrom() << " - to: " << r->getDeviceTo() << " - Social Relation: " << r->getRelationSIoT() << " > trust value: " << r->getTrust());
		}
	}
}

void TrustManagement::showOpinionMatrixCandidates()
{
	NS_LOG_INFO ("--------------- Relations Nodes and candidates -------------------");

	NS_LOG_INFO ("Candidates Matrix rec_op:");
	for(uint32_t i=0; i<size; i++)
	{
		for(uint32_t j=size; j<sizeCandidates; j++)
		{
			if(i!=j)
				NS_LOG_INFO ("[" << i << "]" << "[" << j << "] - b: "<< rec_op[i][j]->getB() << " d: " << rec_op[i][j]->getD() << " u: " << rec_op[i][j]->getU() << " a: " << rec_op[i][j]->getA() << " - b: "<< max_matrix[i][j]->getB() << " d: " << max_matrix[i][j]->getD() << " u: " << max_matrix[i][j]->getU() << " a: " << max_matrix[i][j]->getA());
		}
	}
}

void TrustManagement::showSocialTrustCandidates()
{
	NS_LOG_INFO ("Social Trust in candidates:");
	for(uint32_t i=0; i<size; i++)
	{
		std::map<uint32_t, double>::iterator ite;
		for(uint32_t j=size; j<sizeCandidates; j++)
		{
			if(i!=j)
				NS_LOG_INFO ("[" << i << "]" << "[" << j << "] - : " << socialTrust.at(i).at(j));
		}
	}
}

void TrustManagement::showAttackinformation()
{
	NS_LOG_INFO ("--------------- Access Denied -------------------");
	NS_LOG_INFO ("Total of nodes with access denied: " << statistics[communityIndex]->getTotalAccDenied());

	NS_LOG_INFO ("--------------- Attackers -------------------");
	NS_LOG_INFO ("Total of Multi id attacks with stolen id: " << statistics[communityIndex]->getNumMultiStolenId() << " - the real number is: " << statistics[communityIndex]->getNumMultiStolenIdReal());
	NS_LOG_INFO ("Total of Multi id attacks with fabricated id: " << statistics[communityIndex]->getNumMultiFabricatedId() << " - the real number is: " << statistics[communityIndex]->getNumMultiFabricatedReal());
	NS_LOG_INFO ("Total of Churn attacks with stolen id: " << statistics[communityIndex]->getNumChurnStolenId() << " - the real number is: " << statistics[communityIndex]->getNumChurnStolenIdReal());
	NS_LOG_INFO ("Total of churn attacks with fabricated id: " << statistics[communityIndex]->getNumChurnFabricatedId() << " - the real number is: " << statistics[communityIndex]->getNumChurnFabricatedIdReal());

	uint32_t t = statistics[communityIndex]->getNumMultiStolenId()+statistics[communityIndex]->getNumMultiFabricatedId()+statistics[communityIndex]->getNumChurnStolenId()+statistics[communityIndex]->getNumChurnFabricatedId();
	//uint32_t t1 = statistics[communityIndex]->getNumMultiStolenIdReal()+statistics[communityIndex]->getNumMultiFabricatedReal()+statistics[communityIndex]->getNumChurnStolenIdReal()+statistics[communityIndex]->getNumChurnFabricatedIdReal();
	//double fn = (double) t/t1;
	NS_LOG_INFO ("--------------- False negatives -------------------");
	//NS_LOG_INFO ("Total of false positives: " << (statistics[communityIndex]->getNumMultiStolenId()+statistics[communityIndex]->getNumMultiFabricatedId()+statistics[communityIndex]->getNumChurnStolenId()+statistics[communityIndex]->getNumChurnFabricatedId())/statistics[communityIndex]->getNumTotalReq());
	NS_LOG_INFO ("Total of false negatives: " << statistics[communityIndex]->getNumFalseNegative());
	NS_LOG_INFO ("Total of requests: " << statistics[communityIndex]->getNumTotalReq());

	double accuracy = (double) (t+statistics[communityIndex]->getNumTruePositive())/statistics[communityIndex]->getNumTotalReq();
	NS_LOG_INFO ("The accuracy is: " << accuracy );

	NS_LOG_INFO ("Number of True positives: " << statistics[communityIndex]->getNumTruePositive() );
	NS_LOG_INFO ("Number of false positives: " << statistics[communityIndex]->getNumFalsePositive() );
	NS_LOG_INFO ("Number of legal requests: " << statistics[communityIndex]->getNumLegalReq());
}

void TrustManagement::showSim()
{
	NS_LOG_INFO ("--------------- Similarity between the nodes -------------------");
	for(uint32_t i=0; i<size; i++)
	{
		for(uint32_t j=0; j<size; j++)
		{
			if(i!=j)
				NS_LOG_INFO ("Similarity between [" << i << ", " << j << "]: " << devices[i]->getReputation(j)->getSimilarity());
		}
	}
}

void TrustManagement::showSimCandidates()
{
	NS_LOG_INFO ("--------------- Similarity between nodes and candidates -------------------");
	for(uint32_t i=0; i<size; i++)
	{
		for(uint32_t j=size; j<sizeCandidates; j++)
		{
			if(i!=j)
				NS_LOG_INFO ("Similarity between [" << i << ", " << j << "]: " << devices[i]->getReputation(j)->getSimilarity());
		}
	}
}
/* --------------- End ---------------*/

/* --------------- Callbacks ---------------*/
void TrustManagement::CallBackSocialTrust(uint32_t u, uint32_t v)
{
	std::map<uint32_t, std::map<uint32_t,double>>::iterator it = socialTrust.find(u);
	std::map<uint32_t, double>::iterator ite = it->second.find(v);
	int siot = devices[u]->getReputation(v)->getRelationSIoT();
	double gamma = SIoTTable((Relation::SIoT)siot);

	cb2 (siot, gamma, ite->second);
	cb4 (siot, gamma, ite->second, devices[u]->getReputation(v)->getSimilarity());
}

void TrustManagement::CallBackSocialTrustCandidates(uint32_t u, uint32_t v)
{
	std::map<uint32_t, std::map<uint32_t,double>>::iterator it = socialTrust.find(u);
	std::map<uint32_t, double>::iterator ite = it->second.find(v);
	int siot=0; // = devices[u]->getReputation(v)->getRelationSIoT();
	for(Ptr<Relation> r : devices[u]->getAllReputarion())
	{
		if(r->getDeviceTo() == v)
			siot = r->getRelationSIoT();
	}
	double gamma = SIoTTable((Relation::SIoT)siot);

	cb3 (siot, gamma, ite->second);
}

void TrustManagement::CallBackSocialTrust3d(uint32_t u, uint32_t v)
{
	std::map<uint32_t, std::map<uint32_t,double>>::iterator it = socialTrust.find(u);
	std::map<uint32_t, double>::iterator ite = it->second.find(v);
	int siot = devices[u]->getReputation(v)->getRelationSIoT();
	double gamma = SIoTTable((Relation::SIoT)siot);

	cb5 (siot, gamma, ite->second, devices[u]->getReputation(v)->getSimilarity());
}


#endif /* SCOT_TRUSTMANAGER_H_ */
