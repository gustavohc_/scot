/*
 * device.h
 *
 *  Created on: May 9, 2018
 *      Author: gustavo
 */
#include <iostream>
#include <random>
#include <ctime>
#include "relation.h"

#ifndef DEVICE_H_
#define DEVICE_H_


class Device : public Object
{
public:

	// ************************** PUBLIC FIELDS ******************************

	/**
	 * The Behavior enumeration details the initialization models to which a
	 * User's behavior may conform.
	 */
	enum Behavior{USR_GOOD, USR_PURE, USR_FEED, USR_PROV,
		USR_DISG, USR_SYBL, UNKNOWN};

	// *************************** CONSTRUCTORS ******************************

	Device ();
	/**
	 * Create a User, initializing some fields based on the Behavior argument.
	 * @param model Behavior to use for field generation
	 * @param pre_trusted Whether or not this user is pre-trusted
	 * @param GLOBALS The Network parameterization object
	 */
	Device(Behavior model, bool pre_trusted, uint32_t id, uint32_t community);

	/**
	 * Create a User by providing all relevant fields.
	 * @param model Behavior to which this User conforms
	 * @param cleanup Probability of cleaning up an invalid file
	 * @param honest Probability of providing an honest feedback
	 * @param pre_trusted Whether or not this user is pre-trusted
	 * @param GLOBALS The Network parameterization object
	 */
	Device(Behavior model, double cleanup, double honest, bool pre_trusted, uint32_t id, uint32_t community);

	// ************************** PUBLIC METHODS *****************************

		// ----------------- BEHAVIOR RELATED (STATIC) -----------------------

	/**
	 * Retrieve the integer identifier assigned to a Behavior model.
	 * @param behavior The behavior model whose identifier is desired
	 * @return The unique integer identifier of that model
	 */
	int32_t BehaviorToInt(Behavior behavior);

	/**
	 * Given an integer, return the Behavior associated with that identifier.
	 * @param behavior_int The integer identifier of the desired Behavior
	 * @return The desired Behavior type
	 */
	static Behavior IntToBehavior(int32_t behavior_int);

	// ---------------- FIELD ACCESS/MODIFY METHODS ----------------------

	/**
	 * Access method to a global Relation between this user and another.
	 * @param user_num Numerical identifier of other User
	 * @return Relation describing this User's relationship with 'user_num'
	 */
	//Relation getRelation(uint32_t user_num);
	/**
	 * Access method to the Behavior model of this User.
	 * @return Behavior model of this User
	 */
	Behavior getModel();
	/**
	 * Access method to the pre_trusted field of this User
	 * @return TRUE if this User is pre-trusted, FALSE otherwise.
	 */
	bool isPreTrusted();
	/**
	 * Access method to the cleanup field of this User.
	 * @return Cleanup rate of this User
	 */
	double getCleanup();
	/**
	 * Access method to the honesty field of this User.
	 * @return Honesty rate of this User
	 */
	double getHonesty();
	/**
	 * Access method to the num_files field of this User.
	 * @return The number of files this User possesses
	 */
	double getNumFiles();
	/**
	 * Increase the file possession counter by one.
	 */
	void 						incFileCount();
	void 						setReputation();
	void 						setListFriends(std::vector<uint32_t> list);
	void 						setListInterests(uint32_t id);
	void 						setListPerfils(uint32_t id);
	void 						changeRelation(uint32_t node);
	void 						setId(uint32_t id);
	void 						addRelation(uint32_t from, uint32_t to);
	uint32_t 					getId();
	Ptr<Relation>				getReputation(uint32_t index);
	std::vector<Ptr<Relation>> 	getAllReputarion();
	std::vector<uint32_t> 		getListFriends();
	std::vector<uint32_t> 		getListInterests();
	std::vector<uint32_t> 		getListPerfil();


private:

	// ************************** PRIVATE FIELDS *****************************

	uint32_t id;

	/**
	 * The Behavior model to which this User conforms.
	 */
	Behavior model;


	/**
	 * Whether or not the user is pre-trusted
	 */
	bool pre_trusted;

	/**
	 * The probability this User will clean up an invalid file.
	 */
	double pct_cleanup;

	/**
	 * The probability this User will provide honest feedback.
	 */
	double pct_honest;

	/**
	 * The number of files owned by this User.
	 */
	int32_t num_files;

	int32_t communityIndex;

	/**
	 * Array storing Relations (reputations) of other User's in Network
	 */
	std::vector<Ptr<Relation>> reputation;

	std::vector<uint32_t> listFriend;

	std::vector<uint32_t> listInterests;

	std::vector<uint32_t> listPerfil;

};

Device::Device(){} // @suppress("Class members should be properly initialized")

Device::Device(Behavior model, bool pre_trusted, uint32_t id, uint32_t community)
{ // @suppress("Class members should be properly initialized")
	this->id = id;
	this->model = model;
	this->pre_trusted = pre_trusted;
	this->num_files = 0;
	this->communityIndex = community;
	srand((int)time(0));

	if(model == USR_GOOD)
	{
		this->pct_cleanup = 1.0 - ((double)((rand() % 100) + 1)/10);
		this->pct_honest = 1.0;
	} else if(model == USR_PURE){
		this->pct_cleanup = (double)((rand() % 100) + 1)/10;
		this->pct_honest = 0.0;
	} else if(model == USR_FEED){
		this->pct_cleanup = 1.0 - ((double)((rand() % 100) + 1)/10);
		this->pct_honest = 0.0;
	} else if(model == USR_PROV){
		this->pct_cleanup = (double)((rand() % 100) + 1)/10;
		this->pct_honest = 1;
	} else if(model == USR_DISG){
		this->pct_cleanup = 0.5 + ((double)((rand() % 100) + 1)/2);
		this->pct_honest = 0.5 + ((double)((rand() % 100) + 1)/2);
	} else if(model == USR_SYBL){
		this->pct_cleanup = (double)((rand() % 100) + 1)/10;
		this->pct_honest = 0.0; // irrelevant
	} else{
		this->pct_cleanup = 0.0;
		this->pct_honest = 0.0;
	}

	//setListFriends(id);
	setListInterests(id);
	setListPerfils(id);

	for(uint32_t i=0; i<g_cSizes[communityIndex]; i++)
	{
		if(i != id)
		{
			this->reputation.push_back(CreateObject<Relation>(id, i, static_cast<Relation::SIoT>(apps[communityIndex]->getSIoTScenario())));
		}
	}

}

Device::Device(Behavior model, double cleanup, double honest, bool pre_trusted, uint32_t id, uint32_t community)
{

	this->id = id;
	this->model = model;
	this->pre_trusted = pre_trusted;
	this->num_files = 0;
	this->pct_cleanup = cleanup;
	this->pct_honest = honest;
	this->communityIndex = community;

	//setListFriends(id);
	setListInterests(id);
	setListPerfils(id);

	for(uint32_t i=0; i<g_cSizes[communityIndex]; i++)
	{
		if(i != id)
		{
			this->reputation.push_back(CreateObject<Relation>(id, i, static_cast<Relation::SIoT>(apps[communityIndex]->getSIoTScenario())));
		}
	}
}


int32_t Device::BehaviorToInt(Behavior behavior)
{
	// Though inelegant, this method allows us to treat this Java
	// enumeration like one from C, permitting trace file compatibility.

	if(behavior == USR_GOOD) return 0;
	else if(behavior == USR_PURE) return 1;
	else if(behavior == USR_FEED) return 2;
	else if(behavior == USR_PROV) return 3;
	else if(behavior == USR_DISG) return 4;
	else if(behavior == USR_SYBL) return 5;
	else return 6; // (behavior == Behavior.UNKNOWN)
}


Device::Behavior Device::IntToBehavior(int32_t behavior_int){
	if(behavior_int == 0) return USR_GOOD;
	else if(behavior_int == 1) return USR_PURE;
	else if(behavior_int == 2) return USR_FEED;
	else if(behavior_int == 3) return USR_PROV;
	else if(behavior_int == 4) return USR_DISG;
	else if(behavior_int == 5) return USR_SYBL;
	else return UNKNOWN; // if(behavior_int == 6)
}


Device::Behavior Device::getModel()
{
	return (this->model);
}

bool Device::isPreTrusted()
{
	return (this->pre_trusted);
}

double Device::getCleanup()
{
	return (this->pct_cleanup);
}

double Device::getHonesty()
{
	return (this->pct_honest);
}

double Device::getNumFiles()
{
	return (this->num_files);
}

void Device::incFileCount()
{
	this->num_files++;
}

void Device::setId(uint32_t id)
{
	this->id = id;
}

uint32_t Device::getId()
{
	return this->id;
}

Ptr<Relation> Device::getReputation(uint32_t index)
{
	for(uint32_t i=0; i<g_cSizes[this->communityIndex]-1; i++){
		if(reputation[i]->getDeviceTo() == index)
			return reputation[i];
	}
	return reputation[0];
}

std::vector<Ptr<Relation>> Device::getAllReputarion()
{
	return this->reputation;
}

std::vector<uint32_t> Device::getListFriends()
{
	return this->listFriend;
}

std::vector<uint32_t> Device::getListInterests()
{
	return this->listInterests;
}

std::vector<uint32_t> Device::getListPerfil()
{
	return this->listPerfil;
}

void Device::addRelation(uint32_t from, uint32_t to)
{
	this->reputation.push_back(CreateObject<Relation>(from, to, static_cast<Relation::SIoT>(apps[this->communityIndex]->getSIoTScenario())));
}

/* test function! DON'T DO THIS AT HOME */
void Device::changeRelation(uint32_t node)
{
	reputation[node]->setRelationSIoT(Relation::SOCIAL);
}

void Device::setListFriends(std::vector<uint32_t> list)
{
	this->listFriend = list;
}

void Device::setListInterests(uint32_t id)
{
	/*switch(id)
	{
		case 0:
		{
			listInterests.reserve(3);
			uint32_t aux [] = {0,1,2};
			listInterests.insert (listInterests.begin(), aux, aux+3);
			break;
		}
		case 1:
		{
			listInterests.reserve(3);
			uint32_t aux [] = {0,1,2};
			listInterests.insert (listInterests.begin(), aux, aux+4);
			break;
		}
		case 2:
		{
			listInterests.reserve(3);
			uint32_t aux [] = {0,1,3};
			listInterests.insert (listInterests.begin(), aux, aux+3);
			break;
		}
		case 3:
		{
			listInterests.reserve(3);
			uint32_t aux [] = {0,1,2};
			listInterests.insert (listInterests.begin(), aux, aux+4);
			break;
		}
		case 4:
		{
			listInterests.reserve(3);
			uint32_t aux [] = {0,1,3};
			listInterests.insert (listInterests.begin(), aux, aux+3);
			break;
		}
		case 5:
		{
			listInterests.reserve(3);
			uint32_t aux [] = {0,1,2};
			listInterests.insert (listInterests.begin(), aux, aux+3);
			break;
		}
		case 6:
		{
			listInterests.reserve(3);
			uint32_t aux [] = {0,1,2};
			listInterests.insert (listInterests.begin(), aux, aux+4);
			break;
		}
		case 7:
		{
			listInterests.reserve(3);
			uint32_t aux [] = {0,1,2};
			listInterests.insert (listInterests.begin(), aux, aux+5);
			break;
		}
		case 8:
		{
			listInterests.reserve(3);
			uint32_t aux [] = {0,1,2};
			listInterests.insert (listInterests.begin(), aux, aux+5);
			break;
		}
		case 9:
		{
			listInterests.reserve(3);
			uint32_t aux [] = {0,1,2};
			listInterests.insert (listInterests.begin(), aux, aux+5);
			break;
		}
		case 10:
		{
			listInterests.reserve(2);
			uint32_t aux [] = {8,7};
			listInterests.insert (listInterests.begin(), aux, aux+2);
			break;
		}
		case 11:
		{
			listInterests.reserve(2);
			uint32_t aux [] = {8,7};
			listInterests.insert (listInterests.begin(), aux, aux+2);
			break;
		}
		case 12:
		{
			listInterests.reserve(2);
			uint32_t aux [] = {8,7};
			listInterests.insert (listInterests.begin(), aux, aux+2);
			break;
		}
		case 13:
		{
			listInterests.reserve(1);
			uint32_t aux [] = {9};
			listInterests.insert (listInterests.begin(), aux, aux+1);
			break;
		}
		case 14:
		{
			listInterests.reserve(1);
			uint32_t aux [] = {9};
			listInterests.insert (listInterests.begin(), aux, aux+1);
			break;
		}
		case 15:
		{
			listInterests.reserve(1);
			uint32_t aux [] = {9};
			listInterests.insert (listInterests.begin(), aux, aux+1);
			break;
		}
		case 16:
		{
			listInterests.reserve(3);
			uint32_t aux [] = {0,1,2};
			listInterests.insert (listInterests.begin(), aux, aux+3);

			break;
		}
		case 17:
		{
			listInterests.reserve(10);
			uint32_t aux [] = {0,1,2,3,4,5,6,7,8,9};
			listInterests.insert (listInterests.begin(), aux, aux+10);
			break;
		}
	}*/

	if(id % 2 == 0)
	{
		if(g_cSizes[communityIndex]/2 < id)
		{
			for(uint32_t i=0; i<5; i++)
			{
				listInterests.push_back(i);
			}
		}else
		{
			for(uint32_t i=5; i<10; i++)
			{
				listInterests.push_back(i);
			}
		}
	}else
	{
		if(g_cSizes[communityIndex]/2 < id)
		{
			for(uint32_t i=5; i<10; i++)
			{
				listInterests.push_back(i);
			}
		}else
		{
			for(uint32_t i=0; i<5; i++)
			{
				listInterests.push_back(i);
			}
		}
	}

//	for(uint32_t i=0; i<5; i++)
//	{
//		listInterests.push_back(1);
//	}
}

void Device::setListPerfils(uint32_t id)
{

	if(id % 2 == 0)
	{
		if(g_cSizes[communityIndex]/2 < id)
		{
			for(uint32_t i=0; i<3; i++)
			{
				listPerfil.push_back(i);
			}
		}else
		{
			for(uint32_t i=3; i<6; i++)
			{
				listPerfil.push_back(i);
			}
		}
	}else
	{
		if(g_cSizes[communityIndex]/2 < id)
		{
			for(uint32_t i=3; i<6; i++)
			{
				listPerfil.push_back(i);
			}
		}else
		{
			for(uint32_t i=0; i<3; i++)
			{
				listPerfil.push_back(i);
			}
		}
	}
}

#endif /* SCRATCH_DEVICE_H_ */
