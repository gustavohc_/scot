/*
 * timestamp.h
 *
 *  Created on: Nov 20, 2018
 *      Author: gustavo
 */

#ifndef SCOT_TIMESTAMP_H_
#define SCOT_TIMESTAMP_H_


class Timestamp
{
 public:
   // these are our accessors to our tag structure
   void SetTimestamp (Time time);
   Time GetTimestamp (void) const;

   void Print (std::ostream &os) const;

 private:
  Time m_timestamp;
  // end class TimestampTag
};

void Timestamp::SetTimestamp (Time time)
{
	m_timestamp = time;
}

Time Timestamp::GetTimestamp (void) const
{
	 return m_timestamp;
}

void Timestamp::Print (std::ostream &os) const
{
	os << "t=" << m_timestamp;
}

#endif /* SCOT_TIMESTAMP_H_ */
