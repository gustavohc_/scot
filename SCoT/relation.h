/*
 * relation.h
 *
 *  Created on: May 9, 2018
 *      Author: gustavo
 */
#include <iostream>

#ifndef RELATION_H_
#define RELATION_H_

class Relation : public Object
{
public:

	// ************************** PUBLIC FIELDS ******************************
	/**
	 * The Copy enumeration differentiates between the different feedback
	 * histories which are being stored at the Relation level.
	 */
	enum Copy{GLOBAL, HONEST};
	enum SIoT{OWNERSHIP, PARENTAL, LOCATION, COWORK, SOCIAL};
	// *************************** CONSTRUCTORS ******************************
	/**
	 * Construct an empty Relation object. Relations should be modified
	 * only according to the methods below, so no others are provided.
	 */
	Relation();
	Relation(uint32_t from, uint32_t to, SIoT relation);
	// ************************** PUBLIC METHODS *****************************
	/**
	 * Access method to the trust value.
	 * @return Trust value characterizing this user Relation
	 */
	double getTrust();
	/**
	 * Set the trust_value field to a new value.
	 * @param new_trust The new value for the trust_value field
	 */
	void setTrust(double new_trust);
	/**
	 * Set the history store activated for this Relation object.
	 * @param historyStyle History to be maintained, per Copy enumeration
	 */
	void setHistory(Copy historyStyle);
	/**
	 * Access method to number of positive feedbacks, per 'history' parameter.
	 * @return Global-positive feedbacks in this Relation
	 */
	int32_t getPos();
	/**
	 * Access method to number of negative feedbacks, per 'history' parameter.
	 * @return Global-negative feedbacks in this Relation
	 */
	int32_t getNeg();
	// *********************** HONEST TRUST ******************************
	/**
	 * Increment the actual positive interaction count by one
	 */
	void incHonestPos();
	/**
	 * Increment the actual negative interaction count by one
	 */
	void incHonestNeg();
	// *********************** GLOBAL TRUST ******************************
	/**
	 * Increment the global-positive feedback count by one
	 */
	void incGlobalPos();
	/**
	 * Increment the global-negative feedback count by one
	 */
	void 		incGlobalNeg();
	void 		setDeviceFrom(uint32_t from);
	void 		setDeviceTo(uint32_t to);
	void 		setSimilarity(double sim);
	uint32_t 	getDeviceFrom();
	uint32_t 	getDeviceTo();
	SIoT 		getRelationSIoT();
	void 		setRelationSIoT(SIoT rl);
	double 		getSimilarity();


private:

	// ************************** PRIVATE FIELDS *****************************
	/**
	 * Describes feedback history currently activated on this Relation
	 */
	Copy history;
	/**
	 * The number of globally broadcast positive/satisfactory feedbacks.
	 */
	int32_t global_pos;
	/**
	 * The number of globally broadcast negative/unsatisfactory feedbacks.
	 */
	int32_t global_neg;
	/**
	 * The number of truly positive/satisfactory interactions.
	 */
	int32_t honest_pos;
	/**
	 * The number of truly negative/unsatisfactory interactions.
	 */
	int32_t honest_neg;
	/**
	 * The trust value characterizing this user relationship.
	 */
	double 		trust_val;
	double 		similarity;
	uint32_t 	deviceFrom;
	uint32_t 	deviceTo;
	SIoT 		relation;


};

Relation::Relation(){} // @suppress("Class members should be properly initialized")

Relation::Relation(uint32_t from, uint32_t to, SIoT relation)
{

    this->history = GLOBAL;
    this->global_pos = 0;
    this->global_neg = 0;
    this->honest_pos = 0;
    this->honest_neg = 0;
    this->trust_val = 0.0;
    this->deviceFrom = from;
    this->deviceTo = to;
    this->relation = relation;
    this->similarity = 0.0;
    return;
}

double Relation::getTrust(){
	return (this->trust_val);
}

void Relation::setTrust(double new_trust){
	this->trust_val = new_trust;
}

void Relation::setHistory(Relation::Copy historyStyle){
	this->history = historyStyle;
}

int32_t Relation::getPos(){
	if(this->history == GLOBAL){
		return (this->global_pos);
	}else{
		return (this->honest_pos);
	}
}

int32_t Relation::getNeg(){
	if(this->history == GLOBAL){
		return (this->global_neg);
	}else{
		return (this->honest_neg);
	}
}

void Relation::incGlobalPos(){
	this->global_pos++;
}

void Relation::incGlobalNeg(){
	this->global_neg++;
}

void Relation::incHonestPos(){
	this->honest_pos++;
}

void Relation::incHonestNeg(){
	this->honest_neg++;
}

void Relation::setDeviceFrom(uint32_t from){
	this->deviceFrom = from;
}

uint32_t Relation::getDeviceFrom(){
	return this->deviceFrom;
}

void Relation::setDeviceTo(uint32_t to){
	this->deviceTo = to;
}

uint32_t Relation::getDeviceTo(){
	return this->deviceTo;
}

Relation::SIoT Relation::getRelationSIoT()
{
	return this->relation;
}

void Relation::setRelationSIoT(SIoT rl)
{
	this->relation = rl;
}

void Relation::setSimilarity(double sim)
{
	this->similarity = sim;
}

double Relation::getSimilarity()
{
	return this->similarity;
}

#endif /* SCRATCH_RELATION_H_ */
