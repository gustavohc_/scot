/*
 * validator.h
 *
 *  Created on: Jul 22, 2018
 *      Author: gustavo
 */

#ifndef SCOT_VALIDATOR_H_
#define SCOT_VALIDATOR_H_

typedef unsigned long DWORD, *PDWORD, *LPDWORD;
typedef unsigned short WORD, *PWORD, *LPWORD;
typedef int BOOL, *PBOOL, *LPBOOL;
typedef unsigned char BYTE, *PBYTE, *LPBYTE;
typedef unsigned int UINT;

class checksum : public Object
{
public:
    checksum() { clear(); }
    void clear() { sum = 0; r = 55665; c1 = 52845; c2 = 22719;}
    void add(DWORD w);
    void add(BOOL w) { add((DWORD)w); }
    void add(UINT w) { add((DWORD)w); }
    void add(WORD w);
    void add(const std::string & s);
    void add(LPBYTE b, UINT length);
    void add(BYTE b);
    DWORD get() { return sum; }
protected:
    WORD r;
    WORD c1;
    WORD c2;
    DWORD sum;
};

/****************************************************************************
*                                  checksum::add
* Inputs:
*        DWORD d: word to add
* Result: void
*
* Effect:
*        Adds the bytes of the DWORD to the checksum
****************************************************************************/

void checksum::add(DWORD value)
{
 union { DWORD value; BYTE bytes[4]; } data;
 data.value = value;
 for(UINT i = 0; i < sizeof(data.bytes); i++)
    add(data.bytes[i]);
} // checksum::add(DWORD)

/****************************************************************************
*                                 checksum::add
* Inputs:
*        WORD value:
* Result: void
*
* Effect:
*        Adds the bytes of the WORD value to the checksum
****************************************************************************/

void checksum::add(WORD value)
{
 union { DWORD value; BYTE bytes[2]; } data;
 data.value = value;
 for(UINT i = 0; i < sizeof(data.bytes); i++)
   add(data.bytes[i]);
} // checksum::add(WORD)

/****************************************************************************
*                                 checksum::add
* Inputs:
*        BYTE value:
* Result: void
*
* Effect:
*        Adds the byte to the checksum
****************************************************************************/

void checksum::add(BYTE value)
{
 BYTE cipher = (value ^ (r >> 8));
 r = (cipher + r) * c1 + c2;
 sum += cipher;
} // checksum::add(BYTE)

/****************************************************************************
*                                 checksum::add
* Inputs:
*        const CString & s: String to add
* Result: void
*
* Effect:
*        Adds each character of the string to the checksum
****************************************************************************/

void checksum::add(const std::string & s)
{
 for(int i = 0; i < (int)s.length(); i++)
    add((BYTE)s.at(i));
} // checksum::add(CString)

/****************************************************************************
*                                 checksum::add
* Inputs:
*        LPBYTE b: pointer to byte array
*        UINT length: count
* Result: void
*
* Effect:
*        Adds the bytes to the checksum
****************************************************************************/

void checksum::add(LPBYTE b, UINT length)
{
for(UINT i = 0; i < length; i++)
   add(b[i]);
} // checksum::add(LPBYTE, UINT)

#endif /* SCOT_VALIDATOR_H_ */
