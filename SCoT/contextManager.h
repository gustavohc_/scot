/*
 * contextManager.h
 *
 *  Created on: Oct 19, 2018
 *      Author: gustavo
 */

#ifndef SCOT_CONTEXTMANAGER_H_
#define SCOT_CONTEXTMANAGER_H_

class ContextManager : public Object
{
public:

	enum Context{HOME, OFFICE, SCHOOL, GYM, PARK};

	ContextManager();

	ContextManager(uint32_t community);

	double aValueContext();

private:

	map<uint32_t, Context> community_context;

	uint32_t community;
};

ContextManager::ContextManager(){}

ContextManager::ContextManager(uint32_t community)
{

	community_context.insert(std::pair<uint32_t, Context>(0, HOME));
	community_context.insert(std::pair<uint32_t, Context>(1, OFFICE));
	community_context.insert(std::pair<uint32_t, Context>(2, SCHOOL));
	community_context.insert(std::pair<uint32_t, Context>(3, GYM));
	community_context.insert(std::pair<uint32_t, Context>(4, PARK));

	this->community = community;
}

double ContextManager::aValueContext()
{
	map<uint32_t, Context>::iterator it = community_context.find(community);
	switch(it->second)
	{
		case HOME: return 1.;

		case OFFICE: return 0.7;

		case SCHOOL: return 0.5;

		case GYM: return 0.4;

		case PARK: return 0.2;

		default : return 0.5;
	}
}

#endif /* SCOT_CONTEXTMANAGER_H_ */
