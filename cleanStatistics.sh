#!/bin/bash          
  echo Cleaning up...
  rm -r ../statistics/accuracy/*
  rm -r ../statistics/boxplot/*
  rm -r ../statistics/cdf/*
  rm -r ../statistics/detectedRate/*
  rm -r ../statistics/energy/*
  rm -r ../statistics/falsesNegatives/*
  rm -r ../statistics/falsesPositives/*
  rm -r ../statistics/relations-trust/*
  rm -r ../statistics/relations-trust3d/*
  rm -r ../statistics/relations-trust3dAll/*
  rm -r ../statistics/socialTrust/*
  rm -r ../statistics/trust/*
  rm -r ../statistics/trustAttacker/*