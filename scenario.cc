// ./waf --run "wifi-simple-adhoc --distance=500"
 // ./waf --run "wifi-simple-adhoc --distance=1000"
 // ./waf --run "wifi-simple-adhoc --distance=1500"
 //
 // The source node and sink node can be changed like this:
 //
 // ./waf --run "wifi-simple-adhoc --sourceNode=20 --sinkNode=10"
 //
 // This script can also be helpful to put the Wifi layer into verbose
 // logging mode; this command will turn on all wifi logging:
 //
 // ./waf --run "wifi-simple-adhoc-grid --verbose=1"
 //
 // By default, trace file writing is off-- to enable it, try:
 // ./waf --run "wifi-simple-adhoc-grid --tracing=1"
 //
 // When you are done tracing, you will notice many pcap trace files
 // in your directory.  If you have tcpdump installed, you can try this:
 //
 // tcpdump -r wifi-simple-adhoc-grid-0-0.pcap -nn -tt

#include <fstream>
#include <cstdlib>
#include "ns3/core-module.h"
#include "ns3/internet-module.h"
#include "ns3/csma-module.h"
#include "ns3/mobility-module.h"
#include "ns3/internet-apps-module.h"
#include "ns3/ipv6-static-routing-helper.h"
#include "ns3/netanim-module.h"

#include "ns3/ipv6-routing-table-entry.h"
#include "ns3/sixlowpan-module.h"



using namespace ns3;

NS_LOG_COMPONENT_DEFINE ("TrustInIoT");

//Declarations
uint32_t m_bytesTotal;
int numNodes = 10;
int lastNode = 1;
double distance = 1;  // m
bool positionControl = true;

//Fuction to change positions - Random mobility ------------
void SetPosition (Ptr<Node> node, Vector position)
{
  Ptr<MobilityModel> mobility = node->GetObject<MobilityModel> ();
  mobility->SetPosition (position);
}

Vector GetPosition (Ptr<Node> node)
{
  Ptr<MobilityModel> mobility = node->GetObject<MobilityModel> ();
  return mobility->GetPosition ();
}

int GetRandomNode(NodeContainer nodes)
{
	srand (time(NULL));
    int random_integer;
    random_integer = (rand() % nodes.GetN());
    if (random_integer == lastNode){
    	random_integer = (rand() % nodes.GetN());
    }
    std::cout << "Set new position to : " << random_integer <<"\n";
    lastNode = random_integer;
    return random_integer;

}

void AdvancePosition (NodeContainer nodes)
{

  Ptr<Node> node = nodes.Get(GetRandomNode(nodes));

  Vector pos = GetPosition (node);
  //double mbs = ((m_bytesTotal * 8.0) / 1000000);
  m_bytesTotal = 0;
  //m_output.Add (pos.x, mbs);
  if (positionControl){
	  pos.x += 1.0;
	  pos.y += 1.5;
	  positionControl = false;
	  if (pos.x >= 210.0)
	  	{
	  	  return;
	  	}
  }else{
	  pos.x -= 0.8;
	  pos.y -= 1.1;
	  positionControl = true;
	  if (pos.x == 0.0 || pos.y == 0.0)
	  	{
	  	  return;
	  	}
  }
  SetPosition (node, pos);
  //std::cout << "x="<<pos.x << std::endl;
  Simulator::Schedule (Seconds (5.0), &AdvancePosition, nodes);
}

int main (int argc, char** argv)
{
  bool verbose = true;

  //TrustManagement trustManagement;

  CommandLine cmd;
  std::cout << "argc : " << argc <<"\n";

  cmd.AddValue ("verbose", "turn on some relevant log components", verbose);
  cmd.AddValue ("numNodes", "Number of Nodes", numNodes);
  cmd.Parse (argc, argv);

  if (verbose)
    {
      LogComponentEnable ("SixLowPanNetDevice", LOG_LEVEL_ALL);
      LogComponentEnable ("Ping6Application", LOG_LEVEL_ALL);
    }

  Packet::EnablePrinting ();
  Packet::EnableChecking ();

  NS_LOG_INFO ("Create nodes.");
  NodeContainer c;
  c.Create (numNodes);


  NS_LOG_INFO ("Create IPv6 Internet Stack");
  InternetStackHelper internetv6;
  internetv6.Install (c);

  NS_LOG_INFO ("Setting mobility to the nodes");
  MobilityHelper mobility;
  mobility.SetPositionAllocator ("ns3::GridPositionAllocator",
                                     "MinX", DoubleValue (42.0),
                                     "MinY", DoubleValue (42.0),
                                     "DeltaX", DoubleValue (distance),
                                     "DeltaY", DoubleValue (distance),
                                     "GridWidth", UintegerValue (20),
                                     "LayoutType", StringValue ("RowFirst"));
  mobility.SetMobilityModel ("ns3::ConstantPositionMobilityModel");
  mobility.Install (c);

  NS_LOG_INFO ("Create channels.");
  CsmaHelper csma;
  csma.SetChannelAttribute ("DataRate", DataRateValue (5000000));
  csma.SetChannelAttribute ("Delay", TimeValue (MilliSeconds (2)));
  csma.SetDeviceAttribute ("Mtu", UintegerValue (150));
  NetDeviceContainer d = csma.Install (c);


  SixLowPanHelper sixlowpan;
  sixlowpan.SetDeviceAttribute ("ForceEtherType", BooleanValue (true) );
  NetDeviceContainer six = sixlowpan.Install (d);

  NS_LOG_INFO ("Create networks and assign IPv6 Addresses.");
  Ipv6AddressHelper ipv6;
  ipv6.SetBase (Ipv6Address ("2018:1::"), Ipv6Prefix (64));
  Ipv6InterfaceContainer i = ipv6.Assign (six);

//  Experiment exp;
//  			exp.broadcast();

  /* Create a Ping6 application to send ICMPv6 echo request from n0 to n1 via r */
  uint32_t packetSize = 200;
  uint32_t maxPacketCount = 50;
  Time interPacketInterval = Seconds (1.);
  Ping6Helper ping6;

  /*ping6.SetLocal (i1.GetAddress (0, 1));
  ping6.SetRemote (i2.GetAddress (1, 1));*/

  ping6.SetIfIndex(i.GetInterfaceIndex(0));
  ping6.SetRemote(Ipv6Address::GetAllNodesMulticast());

  ping6.SetAttribute ("MaxPackets", UintegerValue (maxPacketCount));
  ping6.SetAttribute ("Interval", TimeValue (interPacketInterval));
  ping6.SetAttribute ("PacketSize", UintegerValue (packetSize));
  ApplicationContainer apps = ping6.Install(c.Get(0));

  apps.Start (Seconds (5.0));
  apps.Stop (Seconds (15.0));


  AsciiTraceHelper ascii;
  csma.EnableAsciiAll (ascii.CreateFileStream ("trust-iot.tr"));
  csma.EnablePcapAll (std::string ("trust-iot"), true);

  //Change nodes position
  Simulator::Schedule (Seconds (1.5), &AdvancePosition, c);

  /*Scenario animation*/
  AnimationInterface anim ("trust-iot.xml");

  Simulator::Stop (Seconds (100));
  NS_LOG_INFO ("Run Simulation.");
  Simulator::Run ();
  Simulator::Destroy ();
  NS_LOG_INFO ("Done.");
}

