/* -*-  Mode: C++; c-file-style: "gnu"; indent-tabs-mode:nil; -*- */
/*  Converts traces from crawdad to ns2 format
 *
 *  Usage of converte_traces:
 *
 *  ./waf --run "converte_traces \
 *        --traceFile=scratch/crawdad-traces-file.tr"
 *
 *  NOTE: crawdad-traces-file.tr could be an absolute or relative path. You could use the file default.ns_movements
 *        included in the same directory as the example file.
 *  NOTE 2: Number of nodes get from the trace file, since it has more than that.
 *          Note that you must know it before to be able to load it.
 *  NOTE 3: Duration must be a positive number and should match the trace file.
 *          Note that you must know it before to be able to load it.
 */


#include <iostream>
#include <fstream>
#include <sstream>
#include "ns3/core-module.h"
#include "ns3/mobility-module.h"
#include "ns3/ns2-mobility-helper.h"

using namespace ns3;

int totalNodes;

// Write a line in the new trace file
void
//WriteNewLine (std::ostream *os, std::string line)
WriteNewLine (std::string line)
{
	// write line in the new trace file
	//*os << line << std::endl;
	std::cout << line << std::endl;
}


int
countSpaces(std::string line)
{
	std::size_t found = line.find_first_of(" ");
	int spaces = 0;
	while(found != std::string::npos)
	{
		found = line.find_first_of(" ",found+1);
		spaces++;
	}
	return spaces;
}

bool
isNodeOfInterest(int nodeNumber)
{
	bool interest = false;

	/*ostermalm_001_1.tr - Top 100 in presence in mobility model
	std::vector <int> nodesI {1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,23,24,
							  26,27,31,33,35,37,41,42,44,47,50,53,54,55,56,63,64,67,69,
							  72,75,76,78,79,83,85,87,91,92,93,95,98,99,104,109,110,114,
							  115,118,119,123,127,129,130,132,136,137,138,140,141,142,
							  145,146,151,153,155,157,158,160,161,164,165,166,168,169,
							  170,172,174,177,183,185,188,191,193,195,198,202,204};
	*/

	/* ostermalm_002_1.tr - Top 100 in presence in mobility model
	std::vector <int> nodesI {1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,
							  22,23,24,25,27,29,33,35,37,42,44,45,49,50,52,54,55,66,
							  67,75,78,79,80,81,85,88,90,91,100,102,104,105,106,107,
							  111,115,117,120,122,123,125,131,132,134,135,136,139,141,
							  143,144,147,156,158,159,161,164,165,166,171,173,176,177,
							  178,180,181,183,191,195,196,202,204,205,206,208,211,216,
							  222,231,245};
	*/
	/* ostermalm_003_1.tr - Top 100 in presence in mobility model
	std::vector <int> nodesI {1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,
							  23,24,25,26,35,36,41,42,43,45,48,50,51,52,53,55,56,58,59,
							  63,66,70,74,76,77,78,81,84,90,95,101,102,103,104,105,107,
							  108,109,111,116,118,120,123,124,125,130,134,135,137,139,146,
							  147,154,156,157,158,167,171,174,175,176,178,180,186,187,191,
							  194,197,201,205,206,209,210,211,212,223,224,231};
	*/

	// ostermalm_003_1.tr - Top 100 in presence in mobility model in first 900s
		std::vector <int> nodesI {1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,
							  23,24,25,26,27,28,30,36,37,39,41,42,44,50,51,52,53,54,55,56,
							  58,60,65,66,69,73,75,77,79,80,81,83,84,85,87,88,91,95,103,106,
							  108,109,111,112,116,118,119,123,124,134,135,136,137,140,141,
							  143,146,147,148,150,153,155,157,158,159,164,170,171,173,174,
							  175,176,178,180,183,184,187,190};

	/* ostermalm_004_1.tr - Top 100 in presence in mobility model
	std::vector <int> nodesI {1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,
							  24,25,26,27,28,29,30,31,32,35,39,40,42,44,46,49,51,56,62,77,
							  83,84,89,92,101,106,107,111,120,127,129,134,137,139,142,145,
							  146,157,158,160,163,169,170,172,175,179,181,183,187,188,194,
							  204,207,210,222,223,226,231,232,233,238,241,246,249,258,263,
							  265,272,273,274,277,279,283,285,293,299,305};

	 */
	/* ostermalm_005_1.tr - Top 100 in presence in mobility model
	std::vector <int> nodesI {1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,
							  25,26,27,28,29,30,31,32,33,34,35,36,37,38,39,40,41,42,50,53,55,
							  56,58,59,60,61,70,86,90,95,98,105,111,112,114,123,124,137,139,
							  151,156,173,186,189,192,194,195,196,198,212,231,235,242,251,256,
							  266,271,275,277,279,281,283,288,294,304,308,310,316,322,326,329,
							  330,334,346,363,396};
	 */

	/* ostermalm_005_1.tr - Top 100 duration in mobility model
	std::vector <int> nodesI {39,59,163,198,242,272,310,371,410,424,438,509,532,567,584,
			  	  	  	  	    606,611,701,716,738,813,859,907,951,982,1054,1115,1120,
							    1122,1140,1193,1239,1287,1322,1361,1384,1393,1513,1534,1569,
								1585,1621,1656,1667,1715,1722,1861,1892,1908,1928,1940,1947,
								1957,1960,1991,2046,2068,2171,2294,2307,2317,2355,2464,2513,
								2558,2614,2669,2689,2712,2718,2737,2753,2773,2792,2798,2808,
								2811,2864,2945,2973,3048,3064,3078,3083,3108,3122,3137,3164,
								3165,3225,3290,3298,3317,3413,3469,3478,3490,3520,3548,3588};
   */

	for (int i = 0; i != 100; i++)
    {
    	if (nodesI[i] == nodeNumber)
    	{
    		interest = true;
    		break;
    	}
    }
	return interest;
}

int
GetNewNodeNumber(int nodeNumber)
{
	int i;
	/*ostermalm_001_1.tr - Top 100 in presence in mobility model
	std::vector <int> nodesI {1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,23,24,
							  26,27,31,33,35,37,41,42,44,47,50,53,54,55,56,63,64,67,69,
							  72,75,76,78,79,83,85,87,91,92,93,95,98,99,104,109,110,114,
							  115,118,119,123,127,129,130,132,136,137,138,140,141,142,
							  145,146,151,153,155,157,158,160,161,164,165,166,168,169,
							  170,172,174,177,183,185,188,191,193,195,198,202,204};
	 */

	/* ostermalm_002_1.tr - Top 100 in presence in mobility model
	std::vector <int> nodesI {1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,
							  22,23,24,25,27,29,33,35,37,42,44,45,49,50,52,54,55,66,
							  67,75,78,79,80,81,85,88,90,91,100,102,104,105,106,107,
							  111,115,117,120,122,123,125,131,132,134,135,136,139,141,
							  143,144,147,156,158,159,161,164,165,166,171,173,176,177,
							  178,180,181,183,191,195,196,202,204,205,206,208,211,216,
							  222,231,245};
	*/
	/* ostermalm_003_1.tr - Top 100 in presence in mobility model
	std::vector <int> nodesI {1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,
							  23,24,25,26,35,36,41,42,43,45,48,50,51,52,53,55,56,58,59,
							  63,66,70,74,76,77,78,81,84,90,95,101,102,103,104,105,107,
							  108,109,111,116,118,120,123,124,125,130,134,135,137,139,146,
							  147,154,156,157,158,167,171,174,175,176,178,180,186,187,191,
							  194,197,201,205,206,209,210,211,212,223,224,231};

	*/

	// ostermalm_003_1.tr - Top 100 in presence in mobility model in first 900s
		std::vector <int> nodesI {1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,
							  23,24,25,26,27,28,30,36,37,39,41,42,44,50,51,52,53,54,55,56,
							  58,60,65,66,69,73,75,77,79,80,81,83,84,85,87,88,91,95,103,106,
							  108,109,111,112,116,118,119,123,124,134,135,136,137,140,141,
							  143,146,147,148,150,153,155,157,158,159,164,170,171,173,174,
							  175,176,178,180,183,184,187,190};


	/* ostermalm_004_1.tr - Top 100 in presence in mobility model
	std::vector <int> nodesI {1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,
							  24,25,26,27,28,29,30,31,32,35,39,40,42,44,46,49,51,56,62,77,
							  83,84,89,92,101,106,107,111,120,127,129,134,137,139,142,145,
							  146,157,158,160,163,169,170,172,175,179,181,183,187,188,194,
							  204,207,210,222,223,226,231,232,233,238,241,246,249,258,263,
							  265,272,273,274,277,279,283,285,293,299,305};

	*/
	/* ostermalm_005_1.tr - Top 100 in presence in mobility model
	std::vector <int> nodesI {1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,
							  25,26,27,28,29,30,31,32,33,34,35,36,37,38,39,40,41,42,50,53,55,
							  56,58,59,60,61,70,86,90,95,98,105,111,112,114,123,124,137,139,
							  151,156,173,186,189,192,194,195,196,198,212,231,235,242,251,256,
							  266,271,275,277,279,281,283,288,294,304,308,310,316,322,326,329,
							  330,334,346,363,396};
	 */

	/* ostermalm_005_1.tr - Top 100 duration in mobility model
	std::vector <int> nodesI {39,59,163,198,242,272,310,371,410,424,438,509,532,567,584,
			  	  	  	  	    606,611,701,716,738,813,859,907,951,982,1054,1115,1120,
							    1122,1140,1193,1239,1287,1322,1361,1384,1393,1513,1534,1569,
								1585,1621,1656,1667,1715,1722,1861,1892,1908,1928,1940,1947,
								1957,1960,1991,2046,2068,2171,2294,2307,2317,2355,2464,2513,
								2558,2614,2669,2689,2712,2718,2737,2753,2773,2792,2798,2808,
								2811,2864,2945,2973,3048,3064,3078,3083,3108,3122,3137,3164,
								3165,3225,3290,3298,3317,3413,3469,3478,3490,3520,3548,3588};
   */

	for (i = 0; i != 100; i++)
    {
    	if (nodesI[i] == nodeNumber)
    		break;
    }
	return i;

}


std::string
ConverteLine(std::string oldLine, int spaces, bool &nodeNumber)
{
	std::string newLine = "";
	std::size_t firstSpace = 0;
	std::size_t nextSpace = 0;
	std::string nodeNum;
	int value;
	nodeNumber = true;

	std::size_t aux1 = 0;
	std::size_t aux2 = 0;


	if (spaces != 3 && spaces != 5 && spaces != 7)
	{
		std::cout << "Wrong number of spaces in old line! " << spaces << std::endl;
		nodeNumber = false;
		return newLine;
	}
	else if (spaces == 5) // Define node initial position
	{
		// 6.0 create 1 818.1 1452.5 0.0 (origin)
		// Transform in two lines
		// $node_(0) set X_ 150.0
		// $node_(0) set Y_ 93.98597018956875

//		newLine = "$node_(";
		// Search to get node number
		aux1 = oldLine.find_first_of(" ",0);
		aux1 = oldLine.find_first_of(" ",aux1+1);
		aux2 = oldLine.find_first_of(" ",aux1+1);
		nodeNum = oldLine.substr(aux1+1,(aux2 - aux1 - 1));

		// In original traces, nodes Id begins from 1, but in NS3 must be from 0
		value = atoi(nodeNum.c_str());
		if(value <= totalNodes)
		{
			newLine = "$node_(";
			// Search to get node number
			firstSpace = oldLine.find_first_of(" ",0);
			firstSpace = oldLine.find_first_of(" ",firstSpace+1);
			nextSpace = oldLine.find_first_of(" ",firstSpace+1);

//			value = GetNewNodeNumber (value);
			nodeNum = std::to_string(value-1);
			//nodeNumber = nodeNum;
			newLine.append(nodeNum);
			firstSpace = nextSpace;

			newLine.append(") set X_ ");
			// Search to get X coordinate
			nextSpace = oldLine.find_first_of(" ",firstSpace+1);
			newLine.append(oldLine.substr(firstSpace+1,(nextSpace - firstSpace - 1)));
			// Insert * to enable break this line out here when inserting
			// in new trace file
			newLine.append("-");

			// Next line for Y coordinate
			newLine.append("$node_(");
			newLine.append(nodeNum);
			newLine.append(") set Y_ ");
			firstSpace = nextSpace;
			nextSpace = oldLine.find_first_of(" ",firstSpace+1);
			newLine.append(oldLine.substr(firstSpace+1,(nextSpace - firstSpace - 1)));
		}
//		else
//			nodeNumber = false;
	}
	else if (spaces == 7) // Define node position
	{
		// 6.0 setdest 1 818.0 1453.0 0.0 0.8 6.6 (origin)
		// Convert to
		// $ns_ at 0.0 "$node_(0) setdest 150.0 110.0 50.40378694202284" (destiny)

//		newLine = "$ns_ at ";
		// Search to get timestamp
		aux1 = oldLine.find_first_of(" ",0);

//		newLine.append (" \"$node_(");
		// Search to get node number
		aux2 = oldLine.find_first_of(" ",aux1+1);
		aux1 = aux2;
		aux2 = oldLine.find_first_of(" ",aux1+1);
		nodeNum = oldLine.substr(aux1+1,(aux2 - aux1 - 1));

		// In original traces, nodes Id begins from 1, but in NS3 must be from 0
		value = atoi(nodeNum.c_str());
		if(value <= totalNodes)
		{
			newLine = "$ns_ at ";
			// Search to get timestamp
			firstSpace = oldLine.find_first_of(" ",0);
			newLine.append(oldLine.substr(0,firstSpace));

			newLine.append (" \"$node_(");
			// Search to get node number
			nextSpace = oldLine.find_first_of(" ",firstSpace+1);
			firstSpace = nextSpace;
			nextSpace = oldLine.find_first_of(" ",firstSpace+1);
//			value = GetNewNodeNumber (value);
			nodeNum = std::to_string(value-1);
			//nodeNumber = nodeNum;
			newLine.append(nodeNum);
			firstSpace = nextSpace;

			newLine.append (") setdest ");
			// Search to get X coordinate
			nextSpace = oldLine.find_first_of(" ",firstSpace+1);
			newLine.append(oldLine.substr(firstSpace+1,(nextSpace - firstSpace - 1)));
			firstSpace = nextSpace;

			newLine.append (" ");
			// Search to get Y coordinate
			nextSpace = oldLine.find_first_of(" ",firstSpace+1);
			newLine.append(oldLine.substr(firstSpace+1,(nextSpace - firstSpace - 1)));

			// Jump Z coordinate
			nextSpace = oldLine.find_first_of(" ",nextSpace+1);
			firstSpace = nextSpace;

			newLine.append (" ");
			// Search to get speed
			nextSpace = oldLine.find_first_of(" ",firstSpace+1);
			newLine.append(oldLine.substr(firstSpace+1,(nextSpace - firstSpace - 1)));

			// End of line
			newLine.append ("\"");
		}

	}else // Destroy node (3 spaces)
	{
		newLine = "Pending!!!";
		nodeNumber = false;
	}
	return newLine;
}



// Example to use ns2 traces file in ns3
int main (int argc, char *argv[])
{
  std::string traceFile;
  std::string newTraceFile;
  std::string oldLine;
  std::string newLine;
  std::string lineXY;
  std::size_t lineDivision = 0;
  bool nodeNumber = false;
  int spaces = 0;
  std::string strTemp;
  double t;
  //int    nodeNum;
  //double duration;

  // Enable logging from the ns2 helper
  LogComponentEnable ("Ns2MobilityHelper",LOG_LEVEL_DEBUG);

  // Parse command line attribute
  CommandLine cmd;
  cmd.AddValue ("traceFile", "Ns2 movement trace file", traceFile);
  cmd.AddValue ("toalNodes", "Total nodes in the simulation", totalNodes);
  cmd.Parse (argc,argv);

  newTraceFile = traceFile.substr(0, traceFile.size() - 3) + "_600s_new.txt";
  // open new trace file for output
  std::ofstream os;
  os.open (newTraceFile.c_str ());

  std::cout << "Opening trace file ..." << std::endl;
  std::ifstream file (traceFile.c_str (), std::ios::in);
  if (file.is_open ())
    {
      while (!file.eof () )
        {
          getline (file, oldLine);

          // Get only the first 900.0 sec -------
          // Comment or change limit value to get
          // more mobility time
          spaces = oldLine.find_first_of(" ", 0);
          strTemp = oldLine.substr(0,spaces);
          t = atof(strTemp.c_str());
          if (t > 600.0)
        	  break;
          // --- First 900 sec ------------------

          spaces = countSpaces(oldLine);
          newLine = ConverteLine(oldLine, spaces, nodeNumber);
          if (newLine == "")
        	  continue;
          if (spaces != 3 && spaces != 5 && spaces != 7)
        	  continue;
          if (!nodeNumber)
        	  continue;
          //std::cout << "nodeNumber.length(): " << nodeNumber.length() << "nodeNumber.substr(0,3): " << nodeNumber.substr(0,3) << std::endl;
          if (spaces == 5)
          {
        	  lineDivision = newLine.find_first_of("-",0);
        	  lineXY = newLine.substr(0,lineDivision);
        	  //std::cout << lineXY << " Node Number: " << nodeNumber << std::endl;
        	  os << lineXY << std::endl;
        	  lineXY = newLine.substr(lineDivision+1,newLine.size() - lineDivision + 1);
        	  //std::cout << lineXY << " Node Number: " << nodeNumber << std::endl;
        	  os << lineXY << std::endl;
          }


          else
          {
        	  //std::cout << newLine << " Node Number: " << nodeNumber << std::endl;
        	  os << newLine << std::endl;
          }
        }
    }
  file.close ();

  os.close (); // close log file
  return 0;
}


