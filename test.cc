/*
 * test.cc
 *
 *  Created on: May 20, 2018
 *      Author: gustavo
 */


#include <fstream>
#include <cstdlib>
#include "ns3/core-module.h"
#include "ns3/internet-module.h"
#include "ns3/csma-module.h"
#include "ns3/mobility-module.h"
#include "ns3/internet-apps-module.h"
#include "ns3/ipv6-static-routing-helper.h"
#include "ns3/netanim-module.h"
#include "ns3/li-ion-energy-source.h"
#include "ns3/energy-source-container.h"
#include "ns3/simple-device-energy-model.h"
#include "ns3/netanim-module.h"

#include "ns3/ipv6-routing-table-entry.h"
#include "ns3/sixlowpan-module.h"

//#include "SCoT/sendPacket.h"

using namespace ns3;

//void ReceivePacket (Ptr<Socket> socket)
//{
//  //NS_LOG_UNCOND ("Received one packet!");
//	std::cout << "Received one packet!" << std::endl;
//}

//Declarations
//uint32_t m_bytesTotal;
//int lastNode = 1;
//bool positionControl = true;
//
////Fuction to change positions - Random mobility ------------
//void SetPosition (Ptr<Node> node, Vector position)
//{
//  Ptr<MobilityModel> mobility = node->GetObject<MobilityModel> ();
//  mobility->SetPosition (position);
//}
//
//Vector GetPosition (Ptr<Node> node)
//{
//  Ptr<MobilityModel> mobility = node->GetObject<MobilityModel> ();
//  return mobility->GetPosition ();
//}
//
//int GetRandomNode()
//{
//	srand (time(NULL));
//    int random_integer;
//    random_integer = (rand() % container.GetN());
//    if (random_integer == lastNode){
//    	random_integer = (rand() % container.GetN());
//    }
//    std::cout << "Set new position to : " << random_integer <<"\n";
//    lastNode = random_integer;
//    return random_integer;
//
//}
//
//void AdvancePosition ()
//{
//
//  Ptr<Node> node = container.Get(GetRandomNode());
//
//  Vector pos = GetPosition (node);
//  //double mbs = ((m_bytesTotal * 8.0) / 1000000);
//  m_bytesTotal = 0;
//  //m_output.Add (pos.x, mbs);
//  if (positionControl){
//	  pos.x += 1.0;
//	  pos.y += 1.5;
//	  positionControl = false;
//	  if (pos.x >= 210.0)
//	  	{
//	  	  return;
//	  	}
//  }else{
//	  pos.x -= 0.8;
//	  pos.y -= 1.1;
//	  positionControl = true;
//	  if (pos.x == 0.0 || pos.y == 0.0)
//	  	{
//	  	  return;
//	  	}
//  }
//  SetPosition (node, pos);
//  //std::cout << "x="<<pos.x << std::endl;
//  Simulator::Schedule (Seconds (5.0), &AdvancePosition);
//}
//
//static void  PrintCellInfo (Ptr<LiIonEnergySource> es, Ptr<Node> nodebatery)
//{
//	std::string filename = "consumo_nos_cripto.txt";
//	Ptr<OutputStreamWrapper> stream = Create<OutputStreamWrapper>(filename.c_str(), std::ios::app);
//
//	//  std::cout << "[NODE INFO BATERY]" << nodebatery << std::endl;
//
//	std::cout << "[BATTERY] "  << "simulation time: " << Simulator::Now ().GetSeconds () <<  " node: " << nodebatery->GetId()<< " voltage: " << es->GetSupplyVoltage () << " remaining power: " <<
//	es->GetRemainingEnergy () / (3.6 * 3600) << " Ah" << std::endl;
//
//	if (!Simulator::IsFinished ())
//	{
//	  Simulator::Schedule (Seconds (1),
//						   &PrintCellInfo,
//						   es, nodebatery);
//	}
//
//
//	*stream->GetStream() << "[BATTERY] "  << "simulation time: " << Simulator::Now ().GetSeconds () <<  " node: " << nodebatery->GetId()<< " voltage: " << es->GetSupplyVoltage () << " remaining power: " << es->GetRemainingEnergy () / (3.6 * 3600) << " Ah" << std::endl;
//
//}

//void buildPositionScenario()
//{
//	MobilityHelper mobility;
//
//	Ptr<ListPositionAllocator> positionAlloc =
//	CreateObject<ListPositionAllocator>();
//
//	/* --- First community ------*/
//	positionAlloc->Add(Vector(42.0, 35.0, 0.));
//	positionAlloc->Add(Vector(52.0, 27.0, 0.));
//	positionAlloc->Add(Vector(57.0, 35.0, 0.));
//	positionAlloc->Add(Vector(48.0, 45.0, 0.));
//	positionAlloc->Add(Vector(58.0, 45.0, 0.));
//	positionAlloc->Add(Vector(63.0, 25.0, 0.));
//	positionAlloc->Add(Vector(67.0, 35.0, 0.));
//	positionAlloc->Add(Vector(78.0, 30.0, 0.));
//	positionAlloc->Add(Vector(75.0, 43.0, 0.));
//
//	/* --- Second community ------*/
//	positionAlloc->Add(Vector(20.0, 18.0, 0.));
//	positionAlloc->Add(Vector(20.0, 5.0, 0.));
//	positionAlloc->Add(Vector(25.0, 11.0, 0.));
//	positionAlloc->Add(Vector(15.0, 11.0, 0.));
//
//	/* --- Third community ------*/
//	positionAlloc->Add(Vector(18.0, 42.0, 0.));
//	positionAlloc->Add(Vector(10.0, 45.0, 0.));
//	positionAlloc->Add(Vector(18.0, 52.0, 0.));
//
//	/* --- Communityless nodes ------*/
//	positionAlloc->Add(Vector(26.0, 30.0, 0.));
//
//	/* ------ Sybil -----*/
//	positionAlloc->Add(Vector(30.0, 39.0, 0.));
//
//
//	mobility.SetPositionAllocator(positionAlloc);
//	mobility.Install (container);
//}

int main(int argc, char **argv) {

//	CommandLine cmd;
//	cmd.Parse (argc, argv);
//
//	/*--------- Scenario variables ---------- */
//
//	ApplicationUtil *appUtil = ApplicationUtil::getInstance();
//
//	/* applications packet parameters */
//	//double rss = -90;
//	uint32_t packetSize = 256; // bytes
//	uint32_t numPackets = 100;
////	int cont = 0;
//	/* Simulation rounds */
//	//rounds = 10;
//	/* Convert to time object */
//	Time interPacketInterval = Seconds(1.0);
//
//
//	/*--------- Environment variables ---------- */
//
//	/* --- Create the communities --- */
////	for (int i = 0; i < totalCommunities; i++)
////	{
////		NodeContainer community;
////
////		community.Create (1);
////		communities.push_back (community);
////	}
//
//	std::cout << "Creating nodes \n"<< std::endl;
//	//NodeContainer c;
//	container.Create (numNodes);
//
//	std::cout << "Create IPv6 Internet Stack \n"<< std::endl;
//	InternetStackHelper internetv6;
//	internetv6.Install (container);
//
//	std::cout << "Setting mobility to the nodes"<< std::endl;
//	buildPositionScenario();
//
//
//	std::cout << "Create channels."<< std::endl;
//	CsmaHelper csma;
//	csma.SetChannelAttribute ("DataRate", DataRateValue (5000000));
//	csma.SetChannelAttribute ("Delay", TimeValue (MilliSeconds (2)));
//	csma.SetDeviceAttribute ("Mtu", UintegerValue (150));
//	NetDeviceContainer d = csma.Install (container);
//
//
//	SixLowPanHelper sixlowpan;
//	sixlowpan.SetDeviceAttribute ("ForceEtherType", BooleanValue (true) );
//	NetDeviceContainer six = sixlowpan.Install (d);
//
//	std::cout << "Create networks and assign IPv6 Addresses."<< std::endl;
//	Ipv6AddressHelper ipv6;
//	ipv6.SetBase (Ipv6Address ("2018:1::"), Ipv6Prefix (64));
//	//Ipv6InterfaceContainer ipv6Container = ipv6.Assign (six);
//	ipv6Container = ipv6.Assign (six);
//
//	/* Conf battery */
//	Ptr<Node> nodebatery = CreateObject<Node> ();
//
//	std::cout << "[SIZE OF FIXED NODES CONTAINER]: " << container.GetN() << std::endl;
//
//	for(uint32_t  k = 0; k < container.GetN(); k++)
//	{
//		nodebatery = container.Get(k);
//
//		std::cout << "[NODE INFO]: " << nodebatery->GetId() << std::endl;
//
//		Ptr<SimpleDeviceEnergyModel> sem = CreateObject<SimpleDeviceEnergyModel> ();
//		Ptr<EnergySourceContainer> esCont = CreateObject<EnergySourceContainer> ();
//		Ptr<LiIonEnergySource> es = CreateObject<LiIonEnergySource> ();
//		esCont->Add (es);
//		es->SetNode (nodebatery);
//		sem->SetEnergySource (es);
//		es->AppendDeviceEnergyModel (sem);
//		sem->SetNode (nodebatery);
//		nodebatery->AggregateObject (esCont);
//
//		Time now = Simulator::Now ();
//
//		// discharge at 2.33 A for 1700 seconds
//		sem->SetCurrentA (2.33);
//		now += Seconds (1701);
//
//
//		// discharge at 4.66 A for 628 seconds
//		Simulator::Schedule (now,
//							 &SimpleDeviceEnergyModel::SetCurrentA,
//							 sem,
//							 4.66);
//		now += Seconds (600);
//
//		PrintCellInfo (es,nodebatery);
//
//	}
//
//	for(int nodeind = 0; nodeind < numNodes; nodeind++)
//	{
//		appUtil->putNodeInMap(container.Get(nodeind),nodeind);
//	}
//
//	tid = TypeId::LookupByName ("ns3::UdpSocketFactory");
//
//	AnnouncementPacketCount = (numNodes * numNodes) - numNodes;
//	publicKeyCounter = (numNodes * numNodes) - numNodes;
//	randomBitCounter = (numNodes * (numNodes-1)/2);
//
//	Simulator::ScheduleNow (&init);
//
//	std::cout<<"Actual Message: " << Message << "\n";
//	MessageLength = (int)strlen(Message.c_str()) ;
//	std::cout<<"Message length: "<< MessageLength <<"\n";
//
//	source = Socket::CreateSocket (container.Get (1), tid);
//	stage1StartTime.push_back(Simulator::Now());
//	totalTimeStart = Simulator::Now();
//	Simulator::ScheduleNow (&DCNET, source);
//
//	//Simulator::ScheduleNow (&StartKeyExchange);
//	//Change nodes position
//	Simulator::Schedule (Seconds (1.5), &AdvancePosition);
//
//	//Simulator::Schedule (Seconds (1), &StartSybil, container.Get(17));
//
//	Simulator::Schedule (Seconds (10), &GenerateTraffic, source, packetSize, numPackets, interPacketInterval);
//
//	Simulator::Schedule (Seconds (55), &NewNode, 16);
//
//	//Simulator::Schedule (Seconds (70), &Attacking, 0);
//
//	Simulator::Schedule (Seconds (99.7), &printTrust);
//
//	//Simulator::Schedule (Seconds (99), &DisplayMessage, source);
//
//	//Simulator::Schedule (Seconds (99.5), &ShowSybil);
//
//	Simulator::Schedule (Seconds (99.8), &DisplayMeasurements);
//
//	//AnimationInterface anim ("xmls/test.xml");
//
//	Simulator::Stop (Seconds (100));
//	std::cout << "Run Simulation."<< std::endl;
//	Simulator::Run ();
//	Simulator::Destroy ();
//	std::cout << "Done." << std::endl;


}




/* App to send packets */
	//while (cont < numNodes){

		//std::cout << "PROBLEMA AQUI" << std::endl;
		//std::cout << "TID: " << tid << std::endl;
		//Ptr<Socket> recvSink2 = Socket::CreateSocket (container.Get(3), tid);
		//std::cout << "PEGUEI O NO: " << container.Get(3)->GetId() << std::endl;
		//   recvSink2->Bind(localSocket);

//		Inet6SocketAddress localSocket = Inet6SocketAddress (Ipv6Address::GetAny (),9800);
//		std::cout << "localsocket: " << localSocket << std::endl;
//
//		std::cout << "Bind: " << recvSink2->Bind(localSocket) << std::endl;
//		recvSink2->SetRecvCallback (MakeCallback (&ReceivePacket));
//
//		Ptr<Socket> sourcepp = Socket::CreateSocket (container.Get(cont), tid);
//		Inet6SocketAddress remote = Inet6SocketAddress (Ipv6Address::GetAny (), 8080);
//		std::cout << "remote: " << remote << std::endl;
//		sourcepp->SetAllowBroadcast (true);
//		sourcepp->Connect (remote);
//
//
//		//std::cout << "Testing " << numPackets  << " packets sent with receiver rss " << rss << std::endl;
//		Simulator::ScheduleWithContext (sourcepp->GetNode ()->GetId (),
//									  Seconds (3.3), &GenerateTraffic,
//									  sourcepp, packetSize, numPackets, interPacketInterval);
//
//		cont++;
//	}
