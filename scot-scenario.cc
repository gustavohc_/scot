/*
 * scot-scenario.cc
 *
 *  Created on: Jul 31, 2018
 *      Author: gustavo
 */
#include <fstream>
#include <cstdlib>
#include "ns3/core-module.h"
#include "ns3/internet-module.h"
#include "ns3/csma-module.h"
#include "ns3/mobility-module.h"
#include "ns3/internet-apps-module.h"
#include "ns3/ipv6-static-routing-helper.h"
#include "ns3/netanim-module.h"
#include "ns3/energy-module.h"
#include "ns3/li-ion-energy-source.h"
#include "ns3/energy-source-container.h"
#include "ns3/basic-energy-source-helper.h"
#include "ns3/simple-device-energy-model.h"
#include "ns3/netanim-module.h"
#include "ns3/scot-application-helper.h"
#include "ns3/scot-packet-sink-helper.h"
#include "ns3/ns2-mobility-helper.h"

#include "ns3/ipv6-routing-table-entry.h"
#include "ns3/sixlowpan-module.h"

#include "ns3/stats-module.h"

#include "SCoT/sendPacket.h"

using namespace ns3;

//void ReceivePacket (Ptr<Socket> socket)
//{
//  //NS_LOG_UNCOND ("Received one packet!");
//	std::cout << "Received one packet!" << std::endl;
//}

class BasicEnergyDepletion
{
public:
  BasicEnergyDepletion ();
  BasicEnergyDepletion (uint32_t numNodes);
  virtual ~BasicEnergyDepletion ();

private:
  /**
   * Callback invoked when energy is drained from source.
   */
  void DepletionHandler (void);

  /**
   * \param simTimeS Simulation time, in seconds.
   * \param updateIntervalS Device model update interval, in seconds.
   * \return False if all is good.
   *
   * Runs simulation with specified simulation time and update interval.
   */
  bool DepletionTestCase (double simTimeS, double updateIntervalS);

private:
  uint32_t 	m_numOfNodes;         // number of nodes in simulation
  uint32_t 	m_callbackCount;      // counter for # of callbacks invoked
  double 	m_simTimeS;        // maximum simulation time, in seconds
  double 	m_timeStepS;       // simulation time step size, in seconds
  double 	m_updateIntervalS; // update interval of each device model

};

BasicEnergyDepletion::BasicEnergyDepletion (uint32_t numNodes)
{
  m_numOfNodes = numNodes;
  m_callbackCount = 0;
  m_simTimeS = 600.;
  m_timeStepS = 0.5;
  m_updateIntervalS = 1.5;
}

BasicEnergyDepletion::~BasicEnergyDepletion ()
{
}

void
RemainingEnergy (double oldValue, double remainingEnergy)
{
  NS_LOG_UNCOND (Simulator::Now ().GetSeconds ()
                 << "s Current remaining energy = " << remainingEnergy << "J");
}

/// Trace function for total energy consumption at node.
void
TotalEnergy (double oldValue, double totalEnergy)
{
  NS_LOG_UNCOND (Simulator::Now ().GetSeconds ()
                 << "s Total energy consumed by radio = " << totalEnergy << "J");
}

MobilityHelper buildPositionScenario(uint32_t comm)
{
	MobilityHelper mobility;

	Ptr<ListPositionAllocator> positionAlloc =
	CreateObject<ListPositionAllocator>();

	if(comm)
	{
		/* --- First community ------*/
		positionAlloc->Add(Vector(1.0, 1.0, 0.));
		positionAlloc->Add(Vector(3.0, 5.0, 0.));
		positionAlloc->Add(Vector(5.0, 8.0, 0.));
		positionAlloc->Add(Vector(6.0, 5.0, 0.));
		positionAlloc->Add(Vector(5.0, 7.0, 0.));
	}else
	{
		/* --- Second community ------*/
		positionAlloc->Add(Vector(-1.0, 2.0, 0.));
		positionAlloc->Add(Vector(-2.0, 0.0, 0.));
		positionAlloc->Add(Vector(-3.0, 1.0, 0.));
		positionAlloc->Add(Vector(-3.0, 0.0, 0.));
		positionAlloc->Add(Vector(-5.0, -1.0, 0.));
	}

	/* --- Third community ------*/
//	positionAlloc->Add(Vector(18.0, 42.0, 0.));
//	positionAlloc->Add(Vector(10.0, 45.0, 0.));
//	positionAlloc->Add(Vector(18.0, 52.0, 0.));
//	positionAlloc->Add(Vector(12.0, 50.0, 0.));
//	positionAlloc->Add(Vector(9.0, 48.0, 0.));
//	positionAlloc->Add(Vector(75.0, 43.0, 0.));

	/* --- Communityless nodes ------*/
//	positionAlloc->Add(Vector(26.0, 30.0, 0.));
//
//	/* ------ Sybil -----*/
//	positionAlloc->Add(Vector(30.0, 39.0, 0.));


	mobility.SetPositionAllocator(positionAlloc);
	return mobility;
}


void writingStatistics()
{
	NS_LOG_INFO ("Finishing simulation, writing statistics...");
	for (uint32_t i = 0; i < g_tCommunities; i++)
	{
		statistics[i]->generateCDF();
		statistics[i]->generateBoxplot();
		statistics[i]->writedectecedRate();
//		statistics[i]->writedectecedRateMultiId();
//		statistics[i]->writedectecedRateChurn();
		statistics[i]->writeFalsesNegativesRate();

		NS_LOG_INFO ("------------------------------------");
		NS_LOG_INFO ("The Mean: " << statistics[i]->Output());
		NS_LOG_INFO ("------------------------------------");

	}
}

void ReceivePacket (Ptr<Socket> socket)
{
	while (socket->Recv ())
	{
		NS_LOG_INFO ("Received one packet!");
	}
}

//------------------------------------------------------------------------
// Experiment
//------------------------------------------------------------------------

void runExperiment()
{
	NS_LOG_INFO ("Setting parameters...");
	/*--------- Scenario variables ---------- */

	/* applications packet parameters */
//	double rss = -80; // -dBm
//	/*
//	* This is a magic number used to set the transmit power, based on other
//	* configuration.
//	*/
//	double offset = 81;
	std::stringstream ss;

	simulatorCounter = (((g_tNodesInComm/g_tCommunities) * (g_tNodesInComm/g_tCommunities)) - (g_tNodesInComm/g_tCommunities)) * g_tCommunities;
	randomBitCounter = (((g_tNodesInComm/g_tCommunities) * ((g_tNodesInComm/g_tCommunities)-1)* g_tCommunities)/2);

	// Import node's mobility from the trace file
	// Necessary to use a helper from NS2
	NS_LOG_INFO ("Loading mobility trace...");
	Ns2MobilityHelper ns2 = Ns2MobilityHelper ("scratch/ostermalm_001_2_600s_new.txt");

	MessageLength = (uint32_t)strlen(Message.c_str());
	NS_LOG_INFO ("Message length: " << MessageLength);

	/*--------- Environment variables ---------- */
	NS_LOG_INFO ("Creating communities");
	cSize = g_tNodesInComm/g_tCommunities;

	/* --- Create the communities and the real nodes --- */
	for (uint32_t i = 0; i < g_tCommunities; i++)
	{
		NodeContainer community;

		NS_LOG_INFO ("Creating nodes");
		community.Create (g_tNodesInComm);
		communities.push_back (community);
	}

	for (uint32_t i=0; i < g_tCommunities; i++)
	{
		/*--------- Create App instances for each community ---------- */
		apps.push_back(CreateObject<ApplicationUtil>(i));
		/*--------- Create statistic instance for each community ---------- */
		statistics.push_back(CreateObject<Statistics>(i, experiment, strategy, input, runID));
		/*--------- The communities size control ---------- */
		g_cSizes.push_back(cSize);
		/*--------- Create AC manager instances for each community ---------- */
		acs.push_back(CreateObject<AccessControl>(cSize, i));

		//----------------------------------------------------------------------------------
		// Set 6LowPan network
		//----------------------------------------------------------------------------------
		NS_LOG_INFO ("Create channels.");
		CsmaHelper csma;
		csma.SetChannelAttribute ("DataRate", DataRateValue (5000000));
		csma.SetChannelAttribute ("Delay", TimeValue (MilliSeconds (2)));
		csma.SetDeviceAttribute ("Mtu", UintegerValue (150));
		NetDeviceContainer csmaDevice = csma.Install (communities[i]);

		SixLowPanHelper sixlowpan;
		sixlowpan.SetDeviceAttribute ("ForceEtherType", BooleanValue (true) );
		NetDeviceContainer devices = sixlowpan.Install (csmaDevice);

		//----------------------------------------------------------------------------------
		// Set energy model LiIonEnergySource
		//----------------------------------------------------------------------------------
		Config::SetDefault ("ns3::LiIonEnergySource::InternalResistance", StringValue ("0.1"));
		Config::SetDefault ("ns3::LiIonEnergySource::ExpCapacity", StringValue ("0.8"));
		//Config::SetDefault ("ns3::LiIonEnergySource::NomCapacity", StringValue ("0.9"));
		NodeContainer::Iterator node = communities[i].Begin ();
		while (node != communities[i].End ())
		 {
			Ptr<SimpleDeviceEnergyModel> sem = CreateObject<SimpleDeviceEnergyModel> ();
			Ptr<EnergySourceContainer> esCont = CreateObject<EnergySourceContainer> ();
			Ptr<LiIonEnergySource> es = CreateObject<LiIonEnergySource> ();
			esCont->Add (es);
			es->SetNode ((*node));
			sem->SetEnergySource (es);
			es->AppendDeviceEnergyModel (sem);
			sem->SetNode ((*node));
			(*node)->AggregateObject (esCont);

			Time now = Simulator::Now ();

			// discharge at 2.33 A for 1700 seconds
			sem->SetCurrentA (2.33);
			now += Seconds (301);


			// discharge at 4.66 A for 628 seconds
//			Simulator::Schedule (now,
//								&SimpleDeviceEnergyModel::SetCurrentA,
//								sem,
//								0.82);

			PrintCellInfo (es,(*node), i);

			node++;
		 }

		//----------------------------------------------------------------------------------
		// Set wifi network
		//----------------------------------------------------------------------------------
//		std::string phyMode ("DsssRate1Mbps");
//		// disable fragmentation for frames below 2200 bytes
//		Config::SetDefault ("ns3::WifiRemoteStationManager::FragmentationThreshold", StringValue ("2200"));
//		// turn off RTS/CTS for frames below 2200 bytes
//		Config::SetDefault ("ns3::WifiRemoteStationManager::RtsCtsThreshold", StringValue ("2200"));
//		// Fix non-unicast data rate to be the same as that of unicast
//		Config::SetDefault ("ns3::WifiRemoteStationManager::NonUnicastMode",
//						  StringValue (phyMode));
//
//		/** Wifi PHY **/
//		/***************************************************************************/
//		WifiHelper wifiHelper;
//		//wifiHelper.EnableLogComponents ();
//		//wifiHelper.SetStandard (WIFI_PHY_STANDARD_80211n_2_4GHZ); //WIFI_PHY_STANDARD_80211
//		wifiHelper.SetStandard (WIFI_PHY_STANDARD_80211b);
//		// The rate adaptation algorithm doesn't work with the 802.11n standard
//		// remove this line "wifiHelper.SetStandard (WIFI_PHY_STANDARD_80211n_5GHZ)" to forward tests
//		//wifiInfra.SetRemoteStationManager ("ns3::ArfWifiManager");
//		YansWifiPhyHelper wifiPhy =  YansWifiPhyHelper::Default ();
//		// set it to zero; otherwise, gain will be added
//		wifiPhy.Set ("RxGain", DoubleValue (-10));
//		wifiPhy.Set ("TxGain", DoubleValue (offset + rss));
//		wifiPhy.Set ("CcaMode1Threshold", DoubleValue (0.0));
//		// ns-3 supports RadioTap and Prism tracing extensions for 802.11b
////		wifiPhy.SetPcapDataLinkType (YansWifiPhyHelper::DLT_IEEE802_11_RADIO);
//		/***************************************************************************/
//
//		/** wifi channel **/
//		/***************************************************************************/
//		YansWifiChannelHelper wifiChannel = YansWifiChannelHelper::Default ();
//		wifiChannel.SetPropagationDelay ("ns3::ConstantSpeedPropagationDelayModel");
//		//wifiChannel.AddPropagationLoss ("ns3::FriisPropagationLossModel");
//		wifiChannel.AddPropagationLoss ("ns3::FixedRssLossModel","Rss",DoubleValue (rss));
//		wifiPhy.SetChannel (wifiChannel.Create ());
//
//		/** MAC layer **/
//		/***************************************************************************/
//		WifiMacHelper wifiMac;
//		wifiHelper.SetRemoteStationManager ("ns3::ConstantRateWifiManager",
//											"DataMode",StringValue (phyMode),
//											"ControlMode",StringValue (phyMode));
//		// Set it to ad-hoc mode
//		wifiMac.SetType ("ns3::AdhocWifiMac");
//		/** install PHY + MAC **/
//		NetDeviceContainer devices = wifiHelper.Install (wifiPhy, wifiMac, communities[i]);

		//----------------------------------------------------------------------------------
		// Set Protocols to nodes
		//----------------------------------------------------------------------------------
		NS_LOG_INFO ("Create IPv6 Internet Stack");
		InternetStackHelper internetv6;
		internetv6.Install (communities[i]);

		NS_LOG_INFO ("Create networks and assign IPv6 Addresses.");
		Ipv6AddressHelper ipv6;
		std::string addBase ("2018:");
		ss << i;
		addBase += ss.str ();
		addBase += "::";
		ipv6.SetBase (Ipv6Address (addBase.c_str()), Ipv6Prefix (64));
		Ipv6InterfaceContainer ipv6Container = ipv6.Assign (devices);
		ipv6Containers.push_back(ipv6Container);

//		InternetStackHelper internet;
//		internet.Install (communities[i]);
//
//		Ipv4AddressHelper ipv4;
//		NS_LOG_INFO ("Assign IP Addresses.");
//		std::string addBase ("10.1.");
//		ss << i;
//		addBase += ss.str ();
//		addBase += ".0";
//		ipv4.SetBase (addBase.c_str(), "255.255.255.0");
//		Ipv4InterfaceContainer ipv4Container = ipv4.Assign (devices);
//		ipv4Containers.push_back(ipv4Container);

		//
		// The new wireless nodes need a mobility model so we aggregate one
		// to each of the nodes we just finished building.
		//
		NS_LOG_INFO ("Create mobility in the nodes.");
		// Install node's mobility in all nodes
		ns2.Install();
//		MobilityHelper mobility;
//		Ptr<ListPositionAllocator> subnetAlloc =
//		CreateObject<ListPositionAllocator> ();
//		double angle = 2 * 3.141592653589 / cSize;
//		double aux = 15.;
//		for(uint32_t j=0; j < communities[i].GetN(); j++)
//		{
//			double x = 25.0 + aux * cos(j*angle);
//			double y = 25.0 - aux * sin(j*angle);
//			subnetAlloc->Add (Vector (x, y, 1.5));
//			aux += 30.;
//		}
//		//mobility.PushReferenceMobilityModel (backbone.Get (i));
//		mobility.SetPositionAllocator (subnetAlloc);
//		mobility.SetMobilityModel ("ns3::RandomDirection2dMobilityModel",
//								  "Bounds", RectangleValue (Rectangle (-10, 10, -10, 10)),
//								  "Speed", StringValue ("ns3::ConstantRandomVariable[Constant=1]"),
//								  "Pause", StringValue ("ns3::ConstantRandomVariable[Constant=0.9]"));
//		mobility.Install (communities[i]);


		/*
		* Create and install energy source and a single basic radio energy model on
		* the node using helpers.
		*/
//		// source helper
//		BasicEnergySourceHelper basicSourceHelper;
//		// set energy to 0 so that we deplete energy at the beginning of simulation
//		basicSourceHelper.Set ("BasicEnergySourceInitialEnergyJ", DoubleValue (0.0));
//		// set update interval
//		basicSourceHelper.Set ("PeriodicEnergyUpdateInterval",
//							 TimeValue (Seconds (updateIntervalS)));
//		// install source
//		EnergySourceContainer sources = basicSourceHelper.Install (communities[i]);
//
//		// device energy model helper
//		WifiRadioEnergyModelHelper radioEnergyHelper;
//		// set energy depletion callback
//		WifiRadioEnergyModel::WifiRadioEnergyDepletionCallback callback =
//		MakeCallback (&BasicEnergyDepletion::DepletionHandler, this);
//		radioEnergyHelper.SetDepletionCallback (callback);
//		// install on node
//		DeviceEnergyModelContainer deviceModels = radioEnergyHelper.Install (devices, sources);
//
//		/** connect trace sources **/
//		/***************************************************************************/
//		// all sources are connected to node 1
//		// energy source
//		Ptr<BasicEnergySource> basicSourcePtr = DynamicCast<BasicEnergySource> (sources.Get (1));
//		basicSourcePtr->TraceConnectWithoutContext ("RemainingEnergy", MakeCallback (&RemainingEnergy));
//		// device energy model
//		Ptr<DeviceEnergyModel> basicRadioModelPtr =
//		basicSourcePtr->FindDeviceEnergyModels ("ns3::WifiRadioEnergyModel").Get (0);
//		NS_ASSERT (basicRadioModelPtr != NULL);
//		basicRadioModelPtr->TraceConnectWithoutContext ("TotalEnergyConsumption", MakeCallback (&TotalEnergy));
		/***************************************************************************/

		/** Energy Model **/
//		/***************************************************************************/
//		/* energy source */
//		BasicEnergySourceHelper basicSourceHelper;
//		// configure energy source
//		basicSourceHelper.Set ("BasicEnergySourceInitialEnergyJ", DoubleValue (1.0));
//		// install source
//		EnergySourceContainer sources = basicSourceHelper.Install (communities[i]);
//		/* device energy model */
//		WifiRadioEnergyModelHelper radioEnergyHelper;
//		// configure radio energy model
//		radioEnergyHelper.Set ("TxCurrentA", DoubleValue (0.0174));
//		radioEnergyHelper.Set ("RxCurrentA", DoubleValue (0.0197));
//		// install device model
//		DeviceEnergyModelContainer deviceModels = radioEnergyHelper.Install (devices, sources);
//
//		/** connect trace sources **/
//		/***************************************************************************/
//		// all traces are connected to node 1 (Destination)
//		// energy source
//		Ptr<BasicEnergySource> basicSourcePtr = DynamicCast<BasicEnergySource> (sources.Get (1));
//		basicSourcePtr->TraceConnectWithoutContext ("RemainingEnergy", MakeCallback (&RemainingEnergy));
//		// device energy model
//		Ptr<DeviceEnergyModel> basicRadioModelPtr =
//		basicSourcePtr->FindDeviceEnergyModels ("ns3::WifiRadioEnergyModel").Get (0);
//		NS_ASSERT (basicRadioModelPtr != 0);
//		basicRadioModelPtr->TraceConnectWithoutContext ("TotalEnergyConsumption", MakeCallback (&TotalEnergy));

		//----------------------------------------------------------------------------------
		// Tracing
		//----------------------------------------------------------------------------------
//		NS_LOG_INFO ("----------------- Tx e Rx -----------------");
//		AsciiTraceHelper ascii;
//		Ptr<OutputStreamWrapper> stream = ascii.CreateFileStream ("scot-scenario.tr");
//		csma.EnableAsciiAll (stream);
//		csma.EnablePcapAll ("scot-scenario", false);
//		//wifiPhy.EnableAsciiAll (stream);
//		// Trace routing tables
//		internetv6.EnableAsciiIpv6All  (stream);

		//Config::Connect ("/NodeList/*/$ns3::UdpSocketFactory", MakeCallback (&CourseChangeCallback));

		//wifiPhy.EnablePcap ("scot-scenario", staDevices, false);
	}

	//----------------------------------------------------------------------------------
	// Put nodes in the map
	//----------------------------------------------------------------------------------
	for (uint32_t i=0; i < communities.size(); i++)
	{
		for(uint32_t nodeind=0; nodeind < communities[i].GetN(); nodeind++)
		{
			apps[i]->putNodeInMap(communities[i].Get(nodeind),nodeind);
		}
	}

	tid = TypeId::LookupByName ("ns3::UdpSocketFactory");

	//----------------------------------------------------------------------------------
	// Start key exchange between the nodes in the network
	//----------------------------------------------------------------------------------
	Simulator::ScheduleNow (&StartKeyExchange);

	//Change nodes positioncSize
	//Simulator::Schedule (Seconds (1.5), &AdvancePosition);

	//----------------------------------------------------------------------------------
	// Make callbacks to track social trust
	//----------------------------------------------------------------------------------
	Simulator::Schedule (Seconds (0.1), &MakeACallbackSocialTrust);
	Simulator::Schedule (Seconds (0.5), &MakeACallbackSocialTrust3d);

	//----------------------------------------------------------------------------------
	// Start the Sybil nodes
	//----------------------------------------------------------------------------------
	Simulator::Schedule (Seconds (1), &StartSybil);

	//----------------------------------------------------------------------------------
	// Display the informations about the simulation
	//----------------------------------------------------------------------------------
	Simulator::Schedule (Seconds (600), &DisplayMeasurements);
	//Simulator::Schedule (Seconds (99.5), &ShowSybil);
	//Simulator::Schedule (Seconds (80), &printSecrectBit);

	//AnimationInterface anim ("xmls/test.xml");


	Simulator::Stop (Seconds (600));
	NS_LOG_INFO ("Run Simulation.");
	totalTimeStart = Simulator::Now();
	stage1StartTime = Simulator::Now();
//	GtkConfigStore configstore;
//	configstore.ConfigureAttributes();
	Simulator::Run ();
	Simulator::Destroy ();
}

int main(int argc, char **argv)
{
	srand(time(NULL));

	CommandLine cmd;
	cmd.AddValue ("nodes", "total number of nodes", g_tNodes);
	cmd.AddValue ("comm", "total of communities in the simulation", g_tCommunities);
	cmd.AddValue ("attackers", "total number of attackers", g_tAttackers);
	cmd.AddValue ("attack", "the attack type", g_tActualAttack);
	cmd.AddValue ("experiment", "Identifier for experiment.", experiment);
	cmd.AddValue ("strategy", "Identifier for strategy.", strategy);
	cmd.AddValue ("run", "Identifier for run.", runID);
	cmd.Parse (argc, argv);

	LogComponentEnable ("SCoTScenario", LOG_LEVEL_INFO);
	LogComponentEnable ("SCoTApplication", LOG_LEVEL_INFO);
	LogComponentEnable ("SCoTPacketSink", LOG_LEVEL_INFO);
	LogComponentEnable ("Config", LOG_LEVEL_ALL);

	#ifndef STATS_HAS_SQLITE3
	  NS_LOG_ERROR ("sqlite support not compiled in.");
	  return -1;
	#endif

	{
		std::ostringstream sstr;
		sstr << g_tNodes;
		input = sstr.str ();
	}

	g_tNodesInComm = g_tNodes/g_tCommunities;

	cMap::createMapping();

	SeedManager::SetSeed (3);  // Changes seed from default of 1 to 3
	SeedManager::SetRun (33);  // Changes run number from default of 1 to 7

	NS_LOG_INFO("Cleaning stats: "+system("cd scratch/ && ./cleanStatistics.sh"));

	runExperiment();
	writingStatistics();


	NS_LOG_INFO ("Done.");
}



